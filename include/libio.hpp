#include "lowlevel/vpoll.hpp"

#ifndef LIBIO_ENDPOINT_HPP_
#define LIBIO_ENDPOINT_HPP_

#if __cplusplus
#define USE_ARRAY_HASH_TABLE 1
#include "libbase/logcat.hpp"
#include "libbase/table.hpp"

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <map>
#include <memory>
#include <tuple>

namespace amqt{
namespace server{
namespace endpoint{
struct Binder;
struct Acceptor;
struct Connector;

enum Type { TUnknown = 0, TBinder = 1, TAcceptor = 2, TConnector = 3};

struct Endpoint : std::enable_shared_from_this<Endpoint> {
 public:
  Endpoint(Endpoint&) = delete;
  virtual ~Endpoint() {}

 public:

 protected:
  explicit Endpoint(IOClassImpl&& ioclass) : 
    _ioclass{ioclass.internal()._vtable} {}
  Endpoint() : _ioclass{nullptr} {}

 public:
  std::string _message;

 protected:
  IOClassImpl _ioclass;
};

struct Acceptor : public Endpoint {
 public:
  using PBinder = std::shared_ptr<Binder>;
  using Sockaddr = struct sockaddr_in;

 public:
  explicit Acceptor(int port, std::size_t backlog);
  virtual ~Acceptor();

 public:
  PBinder accept_(const Sockaddr& addr);
  IOClassImpl& mount(IOService* serivce);

 public:
  bool reusePort(bool yes);
  bool reuseAddress(bool yes);
  bool nonblock(bool yes);

 public:
 #if !USE_COROUTING_METHOD
  std::function<int(IOClass* clazz, int fd)> accepting;
 #endif

 protected:
  virtual IOBuffer* __restrict_(const Sockaddr& addr);
  virtual bool __config_();
  
 private:
  std::size_t _backlog;
  Sockaddr _addr;
};

struct Connector : public Endpoint {
 public:
  using Callbacks = std::tuple<std::function<int(int fd)>, std::function<int(int fd)>>;

 public:
  explicit Connector(IOClassImpl&& ioclass);
  explicit Connector(sockaddr& addr);  
  virtual ~Connector();

 public:
  virtual IOClassImpl& mount(IOService* serivce);
  virtual bool perform() = 0;

 public:
  const IOClass& ioclass;

 public:
  std::function<int(IOClass*, int)> _in, _out;

 protected:
  struct sockaddr _addr;
  bool _read;
};

struct Binder : public Connector {
 public:
  virtual ~Binder();
  explicit Binder(Acceptor& acceptor, IOClassImpl& ioclass);

 public:
  void mount(int fd, sockaddr_in addr);

 public:
  bool reuseAddress(bool yes);
  bool nonblock(bool yes);
  bool nodelay(bool yes);  

 protected:
  bool perform();

 protected:
  Acceptor& _acceptor;
};
} // endpoint

struct IOService {
 public:
  using PEndpoint = std::shared_ptr<endpoint::Endpoint>;
  using PAcceptor = std::shared_ptr<endpoint::Acceptor>;
  using PConnector = std::shared_ptr<endpoint::Connector>;
  using PBinder = std::shared_ptr<endpoint::Binder>;

  struct Emplacement {
    IOClassImpl& ioclass;
    PEndpoint endpoint;

    Emplacement(IOClassImpl& ioclass, PEndpoint endpoint)
        : ioclass{ioclass}, endpoint{endpoint} {}
  };

 public:
  explicit IOService(std::size_t maxconn = 1000, int backlog = 100);
  ~IOService();

 private:
  IOService(IOService&) = delete;

 public:
  PEndpoint current;
  int       result;

 public:
  inline IOClass& at(PAcceptor acceptor) {
    Emplacement entry{acceptor->mount(this), acceptor};
    int         error{base::error::ENoError};

    if ((error = ioAssignTask(_fdesc, entry.ioclass.internal()._fd, endpoint::TAcceptor)))
      throw pError(error, "ioAssignTask got an unknown error");

    _endpoints.insert(std::make_pair(entry.ioclass.internal()._fd, entry));
    current = acceptor;    
    return entry.ioclass.internal();
  }

  inline IOClass& at(PConnector connector) {
    Emplacement entry{connector->mount(this), connector};
    int         error{base::error::ENoError};

    if ((error = ioAssignTask(_fdesc, entry.ioclass.internal()._fd, endpoint::TConnector)))
      throw pError(error, "ioAssignTask got an unknown error");

    _table.put(std::forward<int>(entry.ioclass.internal()._fd), connector);
    _endpoints.insert(std::make_pair(entry.ioclass.internal()._fd, entry));
    current = connector;    
    return entry.ioclass.internal();
  }

  inline void sign(int&& fd, PConnector binder) {
    const_cast<IOClass&>(binder->ioclass)._vtable = this;
    const_cast<IOClass&>(binder->ioclass).in = nullptr;
    const_cast<IOClass&>(binder->ioclass).out = nullptr;
    const_cast<IOClass&>(binder->ioclass).err = nullptr;

    _table.put(std::forward<int>(fd), binder);
  }

  inline int fpoll() { return _fdesc; }

 private:
  std::map<int, Emplacement> _endpoints;
  base::Table<int, PConnector> _table;

 private:
  int _backlog, _fdesc;

 private:
  std::function<int(int)> _in, _out, _err;
};
} // server
} // amqt
#endif

#if __cplusplus
extern "C"{
#endif
IOClass* embrace(IOClass* ioclass, struct sockaddr addr);
#if __cplusplus
}
#endif
#endif  // LIBIO_ENDPOINT_HPP_
