/* copyright 2017 by lazycat and it is owned by lazycat */

#if !defined(LIBWORKER_HPP_) && __cplusplus
#define LIBWORKER_HPP_

extern "C" {
#include "libavformat/avformat.h"
#include "libavformat/avio.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavutil/avutil.h"
#include "libavutil/imgutils.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libswscale/swscale.h"
#include "libworker.h"
}

#include "libseek.hpp"
#include "libbase/logcat.hpp"
#include "libbase/print.hpp"
#include "libbase/line.hpp"

#include <map>
#include <memory>
#include <sstream>
#include <vector>

namespace amqt {
namespace consumer {
namespace converter {

#define AVError int

struct Output : public std::enable_shared_from_this<Output> {
 public:
  using BitFilter  = std::tuple<AVBSFContext *, unsigned char>;
  using AllocIOCtx = std::function<AVIOContext*()>;

 protected:
  struct FilterGraph {
   public:
    AVFilterContext *bfsink, *bfsrc;
    AVFilterGraph   *graph;
    AVCodec         *codec;
    AVRational       frmrate, smprate;

   public:
    explicit FilterGraph(AVFilterGraph *graph, AVCodec* codec, AVRational frmrate, AVRational smprate);
    ~FilterGraph();

   public:
    inline std::tuple<AVFilterContext *, AVFilterContext *> getInOut() {
      return std::make_tuple(bfsink, bfsrc);
    }

    inline bool isBuilt(std::string &&args, std::string &&descr) {
      return args == _arguments && _description == descr;
    }

    inline void setFiltSpec(std::string&& descr) { _description = descr; }

    inline void addFiltSpec(std::string&& descr) { _description.append(descr); }

    inline std::string getFiltSpecs() { return _description; }

   private:
    std::string     _arguments, _description;
    AVCodecContext *_decodec;
  };

  struct Encode {
   private:
    using BitFilters = std::vector<BitFilter>;

   private:
    AVCodecParserContext *_parser;
    AVCodecContext       *_context;    
    AVDictionary        **_options;
    BitFilters            _bfilters;

   private:
    bool  _copied;

   public:
    explicit Encode(AVCodecContext *context): _parser{nullptr}, _context{context},  _options{nullptr}, status{-1}, count{0}{
      _copied = context == nullptr;
    }

    ~Encode() {
      if (!_parser) 
        avcodec_close(_context);
      else av_parser_close(_parser);

      for (auto &bfilter : _bfilters) 
        av_bsf_free(&std::get<0>(bfilter));
  
      avcodec_free_context(&_context);

      if (_options){
        av_dict_free(_options);
        av_free(_options);
      }
    }

   public:
    int status;
    long count;

   public:
    inline operator bool() { return _copied; }

   public:
    inline AVCodecContext *&context() { return _context; }
    inline AVCodecParserContext *&parser() { return _parser; }
    inline BitFilters &bfilters() { return _bfilters; }
    inline AVDictionary**& options(){ return _options; }
  };

  using Stream = std::tuple<AVStream*, std::shared_ptr<Encode>, std::shared_ptr<FilterGraph>>;
  using Graph = std::tuple<AVFilterContext *, AVFilterContext *>;

 public:
  explicit Output(std::string &&file, AllocIOCtx allocator = nullptr, AVOutputFormat *format = nullptr);
  explicit Output(AllocIOCtx allocator, AVOutputFormat *format);
  virtual ~Output();

 public:
  base::Error configureFiltergraph(AVCodecContext *decctx, std::size_t idstm);
  void copyEverything(AVStream *istream, std::size_t oidstream);

 public:
  base::Error write(std::size_t idstream, AVPacket *packet);
  base::Error write(std::size_t idstream, AVFrame *&frame);

 public:
  base::Error flush();
  base::Error apply();

  template <int pos>
  auto at(std::size_t id) { return id < _streams.size() ? std::get<pos>(_streams[id]) : nullptr; }

  inline bool eof(){ return _eof; }

  inline bool manual(){ return _manual; }

 public:
  std::vector<BitFilter> &getBitFilters(std::size_t id);
  std::size_t getStream(AVStream *istream, AVCodec* codec);
  std::size_t getStreamNext(AVStream *istream, std::size_t codec, std::size_t from);

 public:
  Output &setProperty(std::string&& codec, std::string &&key, std::string&& value);
  Output &setBitFilter(std::string &&codec, int type, std::string &&filter);
  Output &setBitFilter(std::size_t idstream, AVBSFContext *filter);
  Output &setQuality(std::size_t idstream, std::size_t width, std::size_t height);
  Output &setQuality(std::size_t width, std::size_t height);
  Output &setPixFormat(AVPixelFormat pix_fmt);
  Output &setPixFormat(std::size_t idstream, AVPixelFormat pix_fmt);
  Output &setChannelLayout(int layout);
  Output &setChannelLayout(std::size_t idstream, int layout);
  Output &setFrameRate(int rate, int type);
  Output &setFrameRate(std::size_t idstream, int rate);
  Output &setBitRate(int rate, int type);
  Output &setBitRate(std::size_t idstream, int rate);

 protected:
  inline bool __has_video_stream() { return _vidstream != std::string::npos; }

  inline bool __has_audio_stream() { return _aidstream != std::string::npos; }

  inline int __get_main_stream() {
    if (__has_video_stream()) return _vidstream;
    if (__has_audio_stream()) return _aidstream;
    return -1;
  }

 protected:
  virtual bool __on_before_filting_packet(std::size_t idstream, AVFrame *frame);
  virtual bool __on_after_filting_packet(std::size_t idstream, AVFrame *frame);
  virtual bool __on_before_writing_packet(AVPacket *packet);
  virtual bool __on_after_writing_packet(std::size_t idstream, double current);

 private:
  Stream __new_output_stream(std::size_t type, AVCodec* codec);
  base::Error __init_stream_copystream(AVStream *ostream, AVStream *istream);
  base::Error __reconfigure_filtergraph(Stream &stream);
  base::Error __configure_filtergraph(AVCodecContext *decctx, Stream &stream, std::string &&special);
  bool __fix_packet(AVPacket *packet);

 protected:
  AVFormatContext *_context;

 protected:
  std::vector<Stream> _streams;
  bool _applied, _autorot, _lock, _eof, _manual;
 
 private:
  std::size_t _vidstream, _aidstream;

 public:
  static bool isCopyCodec(AVStream *left, AVStream *right);
};

struct Context : public std::enable_shared_from_this<Context> {
 public:
  using POutput = std::shared_ptr<Output>;
  using OStream = std::tuple<std::string, std::size_t>;
  using Packet = std::tuple<AVPacket, AVFrame*, int>;
  using Decoder = std::tuple<AVCodecContext*, int64_t, int8_t>;

 public:
  enum DecStatus{ Finished = -1, GotPacket = 0, GotFrame = 1, DoConverting = 2 , DupData = 3};
  enum EncStatus{ PreparedFilting = 0, GotFiltFrame = 1, SentFrame = 2 };

 public:
  using Observer = std::function<bool(Packet&)>;

 public:
  Packet packet;
  
 public:
  Context(std::string &&url, AVInputFormat *format = nullptr,
          AVDictionary **options = nullptr);
  virtual ~Context();

 public:
  std::string toString(base::TypePrinter &&type);
  base::Error toTranscode(bool interrupted = false);
  int8_t& toDecoder(std::size_t idstream);

 public:  
  base::Error pull(Packet &packet);
  base::Error push(Packet &packet);
  base::Error convey(Observer condition, Observer action);
  void release(Packet& packet);

 public:
  void remove(std::string &&output);

 public:
  inline bool eof(){ return _eof; }
  bool seek(double position, int flags = 0);
  double tell();  

 public:
  std::string getAttribute(std::string &&index, base::TypePrinter type = base::Json);
  Context &setAttribute(std::string &&index, std::string &&value);
 
 public:
  inline const Seekbar& seekbar(){ return _seekbar; }

 public:
  base::Error getThumbnail(double pos, std::string &&path, std::string &&codec);
  double getDuration(std::size_t idstream = std::string::npos);
  std::string getDecodec(std::size_t idstream);
  std::size_t getWidth(std::size_t idstream);
  std::size_t getHeight(std::size_t idstream);

 public:
  std::shared_ptr<Output> &at(std::string &&ofile);

 public:
  Context &setOutput(std::string &&output);
  Context &setOutput(std::string &&output, POutput&& poutput);
  Context &setEncodec(std::string &&output, std::string &&enc, std::size_t type);
  Context &setQuality(std::string &&output, std::size_t wdth, std::size_t hght);
  std::size_t setOStream(std::string &&output, std::size_t st, std::size_t cdc);

 protected:
  inline bool __has_video_stream() { return _vidstream != std::string::npos; }

  inline bool __has_audio_stream() { return _aidstream != std::string::npos; }

  inline int __get_main_stream() {
    if (__has_video_stream()) return _vidstream;
    if (__has_audio_stream()) return _aidstream;
    return -1;
  }

 private: /* utilities functions */
  void __get_streams(std::ostream &result, base::TypePrinter type);
  void __get_stream_info(int id, std::ostream &result, base::TypePrinter type);
  void __get_format(std::ostream &result, base::TypePrinter type);
  std::string __get_stream_elm(std::string &&index, std::size_t id, std::size_t point);

 private: /* configuration */
  std::map<std::string, POutput> _outputs;
  std::vector<Decoder>           _decoders;

 private: /* input context */
  AVFormatContext* _context = nullptr;
  AVDictionary**   _options = nullptr;
  
 private: /* status information */
  std::size_t _vidstream{std::string::npos};
  std::size_t _aidstream{std::string::npos};
  Seekbar     _seekbar;
  double      _cr_conv_pos{0.0};
  bool        _eof{false}, _flush{false}, _fast{false};
};
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
#endif  // LIBWORKER_HPP_
