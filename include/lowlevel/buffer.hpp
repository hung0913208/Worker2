#ifndef LIBIO_BUFFER_HPP_
#define LIBIO_BUFFER_HPP_

#if __cplusplus
#include <memory>
#endif

typedef struct IOBuffer{
#if __cplusplus
 private:
 #endif 	
	unsigned _max, _size;
	void*    _opaque;

#if __cplusplus
 public:
  explicit IOBuffer(std::size_t size);
  virtual ~IOBuffer();

 protected:
  inline unsigned size(){ return _max; }

  inline unsigned max(){ return _size; }

 private:
  std::unique_ptr<uint8_t[]> _buffer;
#endif
} IOBuffer;

#if __cplusplus
extern "C"{
#endif
int readbuffer(IOBuffer* buffer, void* out, int size);
int writebuffer(IOBuffer* buffer, void* in, int size);
#if __cplusplus
}
#endif
#endif // LIBIO_BUFFER_HPP_