#ifndef LIBIO_VPOLL_HPP_
#define LIBIO_VPOLL_HPP_
#include <netinet/in.h>
#include <setjmp.h>

#include "buffer.hpp"
#define private_   /* private: */
#define protected_ /* protected: */
#define public_    /* public: */

#if !__cplusplus
#define bool  unsigned char 
#define true  1
#define false 0
#endif

#if __cplusplus
namespace amqt{
namespace server{
struct IOService;
} // server
} // amqt

#define IOClPriv amqt::server::IOService
#else 
#define IOClPriv void
#endif

typedef jmp_buf Injection;
typedef struct sockaddr_in Sockaddr_in;

#if __cplusplus
extern "C"{
#endif

typedef struct IOClass{
 private_
  IOBuffer* _buffer;
  IOClPriv* _vtable;
  
 private_
  Sockaddr_in _addr;
  Injection _inject;
  int _fd;

 private_
  int (*in)(void* vtable, int fd);
  int (*out)(void* vtable, int fd);
  int (*err)(void* vtable, int fd);

 public_
  void (*start)(struct IOClass*, int (*)(struct IOClass*, int));
  bool (*async)(struct IOClass*, int (*)(struct IOClass*, int));

 public_
 #if USE_COROUTING_METHOD
  bool corouting;
 #endif
} IOClass;
#if __cplusplus
}
#endif

#if __cplusplus
#include <functional>

namespace amqt{
namespace server{
struct IOClassImpl{
 private:
  IOClass _internal;

 public:
  ~IOClassImpl(){}

  explicit IOClassImpl(IOBuffer& buffer) {
    _internal._buffer = &buffer;
    _internal._vtable = nullptr;
  }

  explicit IOClassImpl(IOClPriv* vtable) {
    _internal._buffer = nullptr;
    _internal._vtable = vtable;
  }

  explicit IOClassImpl(IOClPriv* vtable, IOBuffer& buffer) noexcept{
    _internal._buffer = &buffer;
    _internal._vtable = vtable;
  }  

 public:
  IOClass& internal(){ return _internal; }

 protected:
  IOClassImpl() = delete;
  
 public:
 #if USE_COROUTING_METHOD
  std::function<int(IOClassImpl&, std::fucntion<base::Error(int fd)>)> each;
 #endif  
};
#endif
#if __cplusplus
}
#endif

#if __cplusplus
extern "C"{
#endif 
int ioCreatePoll();
void ioReleasePoll(int fd);

/* TAG: start a poll */
int ioPerformPoll(IOClass* clazz, int fd, int backlog);

/* TAG: control an io-task, it can be freely use to make infinity loops for
 * tasks with only a single thread */
int  ioAssignTask(int poll, int fd, int type);
int  ioResignTask(int poll, int fd);
int  ioReleaseTask(int poll, int fd);
void ioDeleteTask(int poll, int fd);
#if __cplusplus
} // server
} // amqt
#endif 
#endif // LIBIO_VPOLL_HPP_
