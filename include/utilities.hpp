/* copyright 2017 by lazycat and it is owned by lazycat */

#ifndef UTILITIES_HPP_
#define UTILITIES_HPP_
#include <math.h>
#include <stdint.h>
#include <string.h>

#if USE_OPENCV
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#endif

#define M_PI 3.14159265358979323846 /* pi */

#if !__cplusplus
#define FixUnused(x) (void)(x)
#define bool unsigned char
#define true 1
#define false 0
#endif

#define CONV_DB(x) (int32_t)((x) * (1 << 16))
#if __cplusplus
#define FixUnused(x) static_cast<void>(x)
extern "C" {
#endif

#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libswscale/swscale.h>

inline void av_display_rotation_set(int32_t matrix[9], double angle) {
  double radians = -angle * M_PI / 180.0f;
  double c = cos(radians);
  double s = sin(radians);

  memset(matrix, 0, 9 * sizeof(int32_t));

  matrix[0] = CONV_DB(c);
  matrix[1] = CONV_DB(-s);
  matrix[3] = CONV_DB(s);
  matrix[4] = CONV_DB(c);
  matrix[8] = 1 << 30;
}

inline struct SwsContext* get_instance_swscontext0(struct SwsContext* resizer, AVStream* istream, AVStream* ostream, int flags) {
  if (istream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO &&
      ostream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
    resizer = sws_getCachedContext(
        resizer, istream->codecpar->width, istream->codecpar->height,
        (enum AVPixelFormat)istream->codecpar->format, ostream->codecpar->width,
        ostream->codecpar->height,
        (enum AVPixelFormat)istream->codecpar->format, flags, NULL, NULL, NULL);
  return resizer;
}

inline struct SwsContext* get_instance_swscontext1(struct SwsContext* resizer, AVFrame* frame, enum AVPixelFormat format, int flags) {
  if (frame->height > 0 && frame->width > 0)
    resizer = sws_getCachedContext(resizer, frame->width, frame->height, (enum AVPixelFormat)frame->format, 
                                   frame->width, frame->height, format, 
                                   flags, NULL, NULL, NULL);
  return resizer;
}

inline struct SwsContext* get_instance_swscontext2(struct SwsContext* resizer, int size[], enum AVPixelFormat format[], int flags) {
  if (size[0] > 0 && size[1] > 0)
    resizer = sws_getCachedContext(resizer, size[0], size[1], format[0], size[0], size[1], format[1], flags, NULL, NULL, NULL);
  return resizer;
}

#if __cplusplus
}
#endif

#if __cplusplus
#include <functional>

template <typename Type>
inline Type* pmalloc(std::size_t count) {
  auto result = reinterpret_cast<Type*>(av_malloc(sizeof(Type) * count));

  memset(result, 0, sizeof(Type) * count);
  return result;
}

template <typename Type>
inline Type* pmalloc(Type* buffer, std::size_t count) {
  auto result = buffer;

  if (!result) {
    result = reinterpret_cast<Type*>(av_malloc(sizeof(Type) * count));
    memset(result, 0, sizeof(Type) * count);
  }
  return result;
}

#if USE_OPENCV
inline cv::Mat frame2mat(AVFrame&& frame){
  auto frm_buff_sz  = av_image_get_buffer_size(AV_PIX_FMT_BGR24, frame.width, frame.height, 1);
  auto frm_buff     = (uint8_t*)calloc(sizeof(uint8_t), static_cast<std::size_t>(frm_buff_sz));
  auto img_conv_ctx = get_instance_swscontext1(nullptr, &frame, AV_PIX_FMT_BGR24, SWS_BICUBIC);  
  auto buff         = av_frame_alloc();
  auto result       = cv::Mat{frame.height, frame.width, CV_8UC3};

  av_image_fill_arrays(buff->data, buff->linesize, frm_buff, AV_PIX_FMT_BGR24, frame.width, frame.height, 1);
  sws_scale(img_conv_ctx, frame.data, frame.linesize, 0, frame.height, buff->data, buff->linesize);
  bcopy(result.data, buff->data[0], buff->linesize[0]);
  av_frame_free(&buff);
  return result;
}

inline AVFrame mat2frame(cv::Mat&& mat, AVPixelFormat format){
  int size[]              = {mat.rows, mat.cols};
  AVPixelFormat formats[] = {AV_PIX_FMT_BGR24, format};
  const int stride[]      = { static_cast<int>(mat.step[0])};

  SwsContext* resizer{get_instance_swscontext2(nullptr, size, formats, SWS_BICUBIC)};
  AVFrame result;  

  sws_scale(resizer, &mat.data, stride, 0, mat.rows, result.data, result.linesize);
  return result;
}
#endif
#endif
#endif  // UTILITIES_HPP_
