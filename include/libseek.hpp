
#if !defined(LIBSEEK_HPP_) && __cplusplus
#define LIBSEEK_HPP_
extern "C" {
#include <libavformat/avformat.h>
}

#include "libbase/logcat.hpp"

#define seconds(tbase, ts) ts*av_q2d(tbase)
#define timebase(tbase, se) static_cast<long>(se/av_q2d(tbase))

#include <functional>
#include <vector>

namespace amqt {
namespace consumer {
namespace converter {
struct Seekbar {
 public:
  using Signal         = std::function<void(double)>;
  using Condition      = std::function<bool(double, int)>;
  using AVIndexEntries = std::vector<AVIndexEntry>;

 public:
  explicit Seekbar(AVFormatContext* context, Signal signal);
  explicit Seekbar(const AVRational ration);

 protected:
  inline void remove(std::size_t index) { _entries.erase(_entries.begin() + index); }
  inline void add(AVIndexEntry&& entry) { _entries.push_back(entry); }
  inline void clear() { _entries.clear(); }

 public:
  bool operator==(double postion);
  bool operator<(double postion);
  bool operator>(double postion);

 public:  
  inline std::vector<Seekbar> split(Condition condition) const{
    std::vector<Seekbar> result;
    Seekbar curr{_ration};

    if (_entries.size() == 0) throw BadAccess;
    for (auto entry : _entries) {
      if (condition(seconds(_ration, entry.timestamp), entry.flags)) {
        curr._length = seconds(_ration, entry.timestamp) - curr.begin();
        result.push_back(curr);
        curr.clear();
      }
      curr.add(std::forward<AVIndexEntry>(entry));
    }

    curr._length = seconds(_ration, _entries.back().timestamp) - curr.begin();
    result.push_back(curr);
    return result;
  }

  inline AVIndexEntry tell() const{ return _entries[_current]; }

 public:
  double find(double begin, std::function<bool(double, int)> condition);
  AVIndexEntry seek(double position, int flags);
  
 public:
  inline double begin(){ return seconds(_ration, _entries[0].timestamp); }

  inline double end(){ return seconds(_ration, _entries.back().timestamp); }

  inline double length(){ return _length; }

 public:
  inline operator bool() const { return _entries.size() > 0; }

 private:
  int __compare(AVIndexEntries::iterator entry, double sample);
  inline Seekbar(){}
  
 private:
  AVIndexEntries _entries;
  std::size_t    _current;
  double         _length;
  AVRational     _ration;
  Signal         _signal;
};
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
#endif  // LIBSEEK_HPP_
