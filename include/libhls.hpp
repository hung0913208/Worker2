#if !defined(LIBHLS_HPP_) && __cplusplus
#define LIBHLS_HPP_

#include "libmedia.hpp"

#define FF_INPUT_BUFFER_PADDING_SIZE 32

extern "C"{
typedef struct DynBuffer {
  int      pos, size, allocated_size;
  uint8_t *buffer;
  int      io_buffer_size;
  uint8_t  io_buffer[1];
} DynBuffer;
}

namespace amqt {
namespace consumer {
namespace hls {
struct Chunk: converter::Output{
 public:
  explicit Chunk(std::size_t idx, std::string &&file, double begin = -1, double end = -1): 
    Output{std::forward<std::string>(file)}, index{idx}, _begin{begin}, _end{end}, _flushed {false}, _padding{0}{}

  explicit Chunk(std::size_t idx, double begin = -1, double end = -1): 
    Output{[]() -> AVIOContext*{ AVIOContext* result{nullptr}; return avio_open_dyn_buf(&result)? nullptr: result; },
           av_guess_format("mpegts", nullptr, "video/MP2T")}, 
    index{idx}, _begin{begin}, _end{end}, _flushed {false}, _padding{0}{}

  ~Chunk(){
    if (_manual){
      unsigned char* buffer{nullptr};
      DynBuffer*   d = reinterpret_cast<DynBuffer*>(_context->pb->opaque);
      AVIOContext* s = _context->pb;

      if (!_flushed)
        avio_close_dyn_buf(_context->pb, &buffer);
      else{
        buffer       = d->buffer;
        _context->pb = nullptr;
        av_free(d);
        av_free(s);
      }
      if (!_flushed) av_free(buffer);
    }
  }

 public:
  inline uint8_t* data(){ 
    if (!_flushed){
      static const unsigned char padbuf[FF_INPUT_BUFFER_PADDING_SIZE] = {0};
      AVIOContext* s = _context->pb;

      /* don't attempt to pad fixed-size packet buffers */
      _flushed = true;
      _padding = 0;

      av_write_trailer(_context);
      if (!s->max_packet_size) {
        avio_write(s, padbuf, sizeof(padbuf));
        _padding = FF_INPUT_BUFFER_PADDING_SIZE;
      }
      
      avio_flush(s);
    }      
    return reinterpret_cast<DynBuffer*>(_context->pb->opaque)->buffer;
  }

  inline int size(){
    return reinterpret_cast<DynBuffer*>(_context->pb->opaque)->size - _padding;
  }

 private:
  bool __on_before_filting_packet(std::size_t idstream, AVFrame *frame);
  bool __on_after_writing_packet(std::size_t idstream, double current);

 public:
  std::size_t index;

 private:
  double _begin, _end;
  bool   _flushed;
  int    _padding;
};

struct M3U8 {
 private:
  using POutput = std::shared_ptr<converter::Output>;

 public:
  explicit M3U8(converter::Context& context, double length):
    _chunks{__build_chunks_(context, length)}, 
    _references{_chunks.size(), std::shared_ptr<Chunk>{nullptr}},
    _context{context}{}

 public:
  inline std::string& storage(){ return _storage; }

  inline std::string& name(){ return _name; }

  inline std::string& alias(){ return _alias; }

  inline std::size_t count(){ return _chunks.size(); }

  inline std::map<std::string, std::string>& properties(){ return _properties; }

  inline double length(){
    auto result = _chunks[0].length();

    for (auto i = count(); i > 1; --i)
      if (result < _chunks[i - 1].length())
        result = _chunks[i - 1].length();
    return result;
  }

  inline std::shared_ptr<Chunk> at(std::size_t index){ return _references[index]; }

  inline std::shared_ptr<Chunk> at(std::size_t index, std::string&& name){
    try {
      auto result = std::dynamic_pointer_cast<Chunk>(_context.at(std::forward<std::string>(name))); 

      if (result && index != std::string::npos && result->index != index) 
        throw NotFound;
      else return result;
    } catch(base::Error& error){  throw base::Error{error}; }
  }

  inline bool& autoback(){ return _auto; }

 public:
  bool ref(std::size_t index, bool memory=false);
  bool ref(std::string&& file, std::size_t index);
  void unref(std::string&& file);
  void unref(std::size_t index);

 public:
  std::string& sprint(std::string& result, std::size_t index = std::string::npos);
  bool transcode(std::size_t index = 0);

 public:
  M3U8& fprint();  
  char* sprint();
  
 private:
  std::vector<converter::Seekbar> __build_chunks_(converter::Context& input, double length);

 private:
  std::map<std::string, std::string>  _properties;
  std::vector<converter::Seekbar>     _chunks;  
  std::vector<std::shared_ptr<Chunk>> _references;  
  converter::Context&                 _context;

 private:
  std::string _name, _alias, _storage;
  bool _auto{false};
};
} // hls
} // consumer
} // amqt
#endif