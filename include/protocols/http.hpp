#if !defined(LIBIO_HTTP_HPP_) && __cplusplus
#define LIBIO_HTTP_HPP_

#include <boost/spirit/include/karma.hpp>

#include <iostream>
#include <map>

namespace qi = boost::spirit::qi;
namespace karma = boost::spirit::karma;

#define KarmaField(key, value) karma::lit(key) << ": " << karma::lit(value)

namespace amqt{
namespace protocol{
namespace http{
  using fields = std::map<std::string, std::string>;
  using boost::spirit::karma::generate;

  static std::map<std::size_t, std::string> codename;

  struct Header{
   public:
    std::string _method, _uri, _version;
    fields _fields;

   public:
    template <typename Iterator>
    void generate(Iterator& output, std::size_t code){
      static std::string Days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
      static std::string Mons[] = {"Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

      generate(output, "HTTP/" << karma::lit(_version) << ' ');
      generate(output, karma::int_(code) << ' ' << karma::lit(codename[code]) << "\r\n");

      for(auto& field: _fields){
        auto& key   = std::get<0>(field);
        auto& value = std::get<1>(field);

        if (key == "Date"){
          auto now = std::time(nullptr);
          auto tm  = std::gmtime(&now);
          auto cov = karma::lit(Days[tm->tm_wday]) << ", " 
                          << karma::int_(tm->tm_mday) << ' '
                          << karma::lit(Mons[tm->tm_mon]) << ' '
                          << karma::int_(tm->tm_year + 1900) << ' '
                          << karma::int_(tm->tm_hour) << ':'
                          << karma::int_(tm->tm_min) << ':'
                          << karma::int_(tm->tm_sec) << " GMT";
                     
          generate(output, KarmaField("Date", cov));
        } else generate(output, KarmaField(key, value));
      }
    }
   private:
  };

  template<typename Byte>
  struct Parser {
   public:
   enum Trid{True, False, Inv};
    using Step = std::function<Trid(Byte)>;
    
   public:
    Header result;

    Parser(std::vector<Step> steps): _steps{step}, _state{std::string::npos}{}

    Trid parse(Byte byte){
      if (_state != std::string::npos){
        auto result = _steps[_state](byte);

        switch(result){
        case True:
          if (++_state < _steps.size())
            return Inv;
          _state = 0;
          break;

        case False:
          _state = std::string::npos;
          break;

        default:
          break;
        }

        return result;
      } else return False;
    }

   private:
    std::vector<Step> _steps;
    std::size_t       _state;
  };
} // http
} // protocol
} // amqt
#endif