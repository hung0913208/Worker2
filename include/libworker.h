/* copyright 2017 by lazycat and it is owned by lazycat */

#ifndef LIBWORKER_LIBWORKER_H_
#define LIBWORKER_LIBWORKER_H_

#if !__cplusplus
#define bool unsigned char
#define True  1
#define False 0
#endif

#if __cplusplus
#endif
#if __cplusplus
extern "C" {
#endif

typedef struct Block{
    unsigned char *data;

    unsigned size, error;
    char    *msg;    
} Block;

enum MediaType { UNKNOWN = -1, VIDEO, AUDIO, DATA, SUBTITLE, ATTACHMENT, NB };
/* TAG: Constructing and destructing of consumer */
extern int rbitInitConsumer(const char* url);
extern void rbitSafeConsumer(int value);
extern void rbitReleaseConsumer(const char* url);
extern void rbitReleaseAllConsumer();

/* TAG: getting functions */
extern char* rbitDoTracking(const char* url, int type);
extern char* rbitGetAttribute(const char* url, const char* index);
extern char* rbitGetCodec(const char* url, int idstream);
extern int rbitGetWidth(const char* url, int idstream);
extern int rbitGetHeight(const char* url, int idstream);
extern double rbitGetDuration(const char* url);

/* TAG: setting functions */
extern int rbitSetProperty(const char* url, const char* output, const char* codec, const char* key, const char* value);
extern int rbitSetQuality(const char* url, const char* output, int height, int width);
extern int rbitSetEncode(const char* url, const char* output, const char* codec, int type);
extern int rbitSetBitFilter(const char* url, const char* output, const char* codec, int type, const char* filter);
extern int rbitSetBitrate(const char* url, const char* output, int type, int bitrate);
extern int rbitSetFramerate(const char* url, const char* output, int type, int rate);
extern int rbitSetOutput(const char* url, const char* output);

/* TAG: converting functions */
extern int rbitDoApplying(const char* url, const char* output);
extern int rbitDoTranscoding(const char* url, bool interrupt);

/* TAG: utilities */
extern void rbitRemoveOutput(const char* url, const char* output);
extern void rbitRemoveAllOutput(const char* url);
extern int rbitPrintThumbnail(const char* url, const char* path, double position);
extern int rbitSeekTime(const char* url, double seconds);
extern double rbitTellTime(const char* url);

/* TAG: hls constructor and destructors of hls context */
extern int rbitInitHLSContext(const char* url, const char* name, const char* alias, double length);
extern void rbitSafeHLSContext(int value);
extern void rbitReleaseHLSContext(const char* url);
extern void rbitReleaseAllHLSContext();

/* TAG: hls generator */
extern int rbitConcreteHLSCtx(const char* url, const char* storage);

/* TAG: hls tools */
extern char* rbitPrintM3U8(const char* url, int index);
extern Block rbitPrintChunk(const char* url, int index);
extern int   rbitHLSContextEOF(const char* url);
#if __cplusplus
}
#endif
#endif  // LIBWORKER_SWIG_WORKER_H_
