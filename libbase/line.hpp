#if !defined(LIBBASE_LINE_HPP_) && __cplusplus
#define LIBBASE_LINE_HPP_

#include <functional>
#include <vector>

namespace base{ 
template <typename T, typename DeepT=int>
struct Line{
public:
  explicit Line(T null, DeepT first, std::function<DeepT(T&&)> deep): _deep{deep}, _current{first}, _null{null} {}

public:
  T exchange(T&& value, std::function<bool(T&&)> cond) {
    if (!memcmp(&value, &_null, sizeof(T))) {
      auto ret = _buffer.size() > 0? _buffer[0]: _null;

      if (_buffer.size() > 0) _buffer.erase(_buffer.begin());
      return ret;
    } else if (cond && cond(std::forward<T>(value))) {
      _current = _deep(std::forward<T>(value));
      return value;
    } else {
      auto pos = bfind(_deep(std::forward<T>(value)));

      if (pos >= 0 && pos < static_cast<int>(_buffer.size()))
        for (auto i = pos; i < static_cast<int>(_buffer.size()); ++i)
           std::swap(_buffer[i], value);

      _buffer.push_back(value);

      if (_buffer.size() > 1  && pos != 0){
        if (cond && !cond(std::forward<T>(_buffer[0])))
          return _null;
        else{
          auto ret = _buffer[0];

          _buffer.erase(_buffer.begin());
          return ret;
        }
      } else return _null;
    }

    return _null;
  }

  T flush(){ return exchange(std::forward<T>(_null), nullptr); }

  T& null() { return _null; }

  DeepT current() { return _current; }

  DeepT begin() { return _deep(std::forward<T>(_buffer[0])); }

private:
  int comp(DeepT left, DeepT right) { return left < right? -1: (left == right? 0: 1); }

  int bfind(DeepT val) {
    auto begin = 0, end = static_cast<int>(_buffer.size()) - 1;
    auto left = -1, right = -1;

    while (begin + 1 < end) {
      if ((begin + end)/2 + 1 >= static_cast<int>(_buffer.size()))
        return comp(val, _deep(std::forward<T>(_buffer[(begin + end)/2]))) <= 0? (begin + end)/2: _buffer.size();
      else {
        left  = comp(val, _deep(std::forward<T>(_buffer[(begin + end)/2])));
        right = comp(val, _deep(std::forward<T>(_buffer[(begin + end)/2 + 1])));
      }
      
      if (right == 0 || left == 0)
        return (begin + end)/2 + (right == 0? 2 : 1);
      else if (-1 == left && right == -1)
        end = (begin + end)/2;
      else if (1 == left && right == 1)
        begin = (begin + end)/2;
      else if (-1 <= left && right <= 1)
        return (begin + end)/2 + (right < 0? left: right);
    }

    if (_buffer.size() == 1) return _deep(std::forward<T>(_buffer[0])) < val;
    else if (_buffer.size() == 2)
      return _deep(std::forward<T>(_buffer[0])) > val? 0: (_deep(std::forward<T>(_buffer[1])) > val? 1: 2);
    else if (begin + 1 == end)
      return _deep(std::forward<T>(_buffer[begin])) > val? begin: (_deep(std::forward<T>(_buffer[end])) > val? end: end + 1);
    else return (_buffer.size() == 0 || left < 0)? 0: _buffer.size();
  }

private:
  std::function<DeepT(T&&)> _deep;
  std::vector<T> _buffer;
  DeepT _current;
  T   _null;
};
}
#endif // LIBBASE_LINE_HPP_