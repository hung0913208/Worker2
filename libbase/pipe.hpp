#if !defined(LIBBASE_PIPE_HPP_) && __cplusplus
#define LIBBASE_PIPE_HPP_

#include <functional>
#include <memory>
#include <vector>

#include "grammar/grammar.hpp"
#include "logcat.hpp"

namespace base {
template <typename ByteT = char>
struct Deserialize {
 public:
  using Step = std::function<grammar::Status(std::size_t, ByteT)>;

 public:
  explicit Deserialize(std::vector<grammar::Grammar<ByteT>>& lexical): 
      _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}{
    for (auto& gram: lexical) _steps.push_back(gram.pull());
  }

  explicit Deserialize(std::vector<Step>& steps):
    _status{grammar::Inv}, _steps{steps}, _step_i{std::string::npos}, _index{0} {}

 public:
  Deserialize& operator<<(ByteT byte) {
    if (_step_i == std::string::npos && _steps.size() > 0)
      _step_i = 0;

    if (_step_i != std::string::npos) {
      auto result = grammar::Fal;

      /*   Voi truong hop state == Pal, day la trang thai yeu cua Step va de dang bi chuyen qua step moi neu ket qua do step 
       * moi tot hon step hien tai. Vi vay ta phai kiem tra truoc khi thuc hien step hien tai voi truong hop nay
       */
      if (_status == grammar::Pal && _step_i + 1 < _steps.size())
        result = _steps[_step_i + 1](0, byte);

      if (_status == grammar::Pal && result == grammar::Unf){
        _step_i += 1;
        _index   = 0;
      } else if (_status == grammar::Pal && result == grammar::Don){
        _index  = 0;

        if (++_step_i == _steps.size())
          _step_i = 0;
        else result = grammar::Inv;
      } else{
        result = _steps[_step_i](_index, byte);

        if (result == grammar::Don) { 
          /*   step tu chu dong thoat khoi qua trinh nap du lieu. Khi dieu nay xay ra, ta khong chac he thong da okey hay 
           * do chua co quyet dinh ro rang tu step tiep theo. Day la tinh trang nhap nhang. Neu khong co step tiep theo 
           * thi hien nhien ket qua la Don.
           */
          _index  = 0;

          if (++_step_i == _steps.size()) _step_i = 0;
          else result = grammar::Inv;        
        } else if (result == grammar::Fal) _step_i = std::string::npos; /* Fal chu dong, tu huy ngay lap tuc */
        else if (result == grammar::Pal || (result == grammar::Inv && _step_i + 1 < _steps.size())) {
          /* tinh trang nhap nhang, step khong chac day la ki tu dung hay sai. Tat ca phu thuoc va step tiep theo 
           * - Unf: Tiep tuc follow step nay de khang dinh chac chan hay khong chac chan ket qua trong tuong lai
           * - Pal: Follow nhung san sang tu bo step nay neu step ke tiep tra ve Unf hoac Don
           * - Fal or Inv: huy hoan toan, khong co khai niem nhap nhang trong day,
           * - Don: Okey, luu lai ki tu nay va nhay qua step tiep theo, roi vao trang thai nhap nhang.
           */
          auto dil = result == grammar::Inv;

          result = _steps[_step_i + 1](0, byte);

          if (result == grammar::Unf || (dil && result == grammar::Pal)){
            _step_i += 1;
            _index   = 0;
          } else if (result == grammar::Don){
            _index  = 0;

            if (++_step_i == _steps.size())
              _step_i = 0;
            else result = grammar::Inv;
          } else result = dil? grammar::Fal: grammar::Pal;
        } else if (result != grammar::Unf) result = grammar::Fal;
      }
      if (result < 0) ++_index;
      _status = result;
    } else _status = grammar::Fal;

    return *this;
  }

  operator int() { 
    if (_status == grammar::Inv){
      if (_step_i == std::string::npos) return grammar::Unf;
      else if (_index == 0 && _status == grammar::Inv)
        return grammar::Unf;
    }
    return _status;
  }

 private:
  grammar::Status  _status;
  std::vector<Step> _steps;
  std::size_t       _step_i, _index;
};

template <typename ByteT = char>
struct Serialize {
 public:
  using Step = std::function<grammar::Status(std::size_t, ByteT&)>;

 public:
  explicit Serialize(std::vector<grammar::Grammar<ByteT>>& lexical): 
      _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}{        
    for (auto& gram: lexical) _steps.push_back(gram.push());
  }

  explicit Serialize(std::vector<Step>&& steps):
    _status{grammar::Inv}, _steps{steps}, _step_i{std::string::npos}, _index{0} {}

 public:
  Serialize& operator >>(ByteT &byte){
     if (_step_i == std::string::npos && _steps.size() > 0)
      _step_i = 0;

    if (_step_i != std::string::npos) {
      auto result = grammar::Fal;

      /*   Voi truong hop state == Pal, day la trang thai yeu cua Step va de dang bi chuyen qua step moi neu ket qua do step 
       * moi tot hon step hien tai. Vi vay ta phai kiem tra truoc khi thuc hien step hien tai voi truong hop nay
       */
      if (_status == grammar::Pal && _step_i + 1 < _steps.size())
        result = _steps[_step_i + 1](0, byte);

      if (_status == grammar::Pal && result == grammar::Unf){
        _step_i += 1;
        _index   = 0;
      } else if (_status == grammar::Pal && result == grammar::Don){
        _index  = 0;

        if (++_step_i == _steps.size())
          _step_i = 0;
        else result = grammar::Inv;
      } else{
        result = _steps[_step_i](_index, byte);

        if (result == grammar::Don) { 
          /*   step tu chu dong thoat khoi qua trinh nap du lieu. Khi dieu nay xay ra, ta khong chac he thong da okey hay 
           * do chua co quyet dinh ro rang tu step tiep theo. Day la tinh trang nhap nhang. Neu khong co step tiep theo 
           * thi hien nhien ket qua la Don.
           */
          _index  = 0;

          if (++_step_i == _steps.size()) _step_i = 0;
          else result = grammar::Inv;        
        } else if (result == grammar::Fal) _step_i = std::string::npos; /* Fal chu dong, tu huy ngay lap tuc */
        else if (result == grammar::Pal || (result == grammar::Inv && _step_i + 1 < _steps.size())) {
          /* tinh trang nhap nhang, step khong chac day la ki tu dung hay sai. Tat ca phu thuoc va step tiep theo 
           * - Unf: Tiep tuc follow step nay de khang dinh chac chan hay khong chac chan ket qua trong tuong lai
           * - Pal: Follow nhung san sang tu bo step nay neu step ke tiep tra ve Unf hoac Don
           * - Fal or Inv: huy hoan toan, khong co khai niem nhap nhang trong day,
           * - Don: Okey, luu lai ki tu nay va nhay qua step tiep theo, roi vao trang thai nhap nhang.
           */
          auto dil = result == grammar::Inv;

          result = _steps[_step_i + 1](0, byte);

          if (result == grammar::Unf || (dil && result == grammar::Pal)){
            _step_i += 1;
            _index   = 0;
          } else if (result == grammar::Don){
            _index  = 0;

            if (++_step_i == _steps.size())
              _step_i = 0;
            else result = grammar::Inv;
          } else result = dil? grammar::Fal: grammar::Pal;
        } else if (result != grammar::Unf) result = grammar::Fal;
      }
      if (result < 0) ++_index;
      _status = result;
    } else _status = grammar::Fal;

    return *this;
  }

  operator int() { 
    if (_status == grammar::Inv){
      if (_step_i == std::string::npos) return grammar::Unf;
      else if (_index == 0 && _status == grammar::Inv)
        return grammar::Unf;
    }
    return _status; 
  }

 private:
  grammar::Status  _status;
  std::vector<Step> _steps;
  std::size_t       _step_i, _index;
};
} // base
#endif
