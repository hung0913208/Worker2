/* copyright 2017 by lazycat and it is owned by lazycat */

#include "logcat.hpp"

namespace base {
namespace debug {
static std::string *__watchdog__{nullptr};
static std::mutex   __lock__;

#ifndef DEBUG
static bool __debug__{false};
#else
static bool __debug__ = DEBUG;
#endif
}  // namespace debug
}  // namespace base

const std::string level(int code){
  if (code < 0) return "INFO";
  else if (code == 0) return "ERROR";
  else return "WARN";
}

namespace base {
#if READABLE
Error::Error(std::string&& debug, int code, int level_, std::string&& message): message{message}, debug(debug), code{code} {
  critical = level_ == 0;
  if (debug::__watchdog__)
    (*debug::__watchdog__) = "[" + level(level_) + ": " + codename() + "]: " + message + " at " + debug;

 #if TRACE
  if (code == 0) return;
  std::cout << "[" << level(level_) << ": " << codename() << "]: " << message << " at " << debug << std::endl << std::flush;
 #elif DEBUG
#if PROFILING
  if (level_ < 0){
    time_t now  = time(0);
    char   buffer [80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));

    std::cout << "[ " buffer ": " << level(level_) << ": " << codename() << "]: " 
              << message << " at " << debug << std::endl << std::flush;
  }
#endif
  if (code == 0 || !debug::__debug__ || level_ < 0) return;
  std::cout << "["<< level(level_) << ": " << codename() << "]: " 
            << message << " at " << debug << std::endl << std::flush;
 #endif
}
#else
Error::Error(std::string&&, int code, int level_, std::string&&): code{code}, critical{level_ == 0} {
  if (debug::__watchdog__) (*debug::__watchdog__) = level(level_) + ": " + codename();
 #if DEBUG
  if (code == 0) return;
  std::cout << "got a(n)" << level(level_) << ": ["  << codename() << "]: " << std::endl << std::flush;
 #else
#if PROFILING
  if (level_ < 0){
    time_t now  = time(0);
    char   buffer [80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));
    std::cout << "[ " << buffer << "]: "
              << "got a(n) " << level(level_) << ": [" << codename() << "] " << std::endl << std::flush;
  }
#endif
  if (code == 0 || !debug::__debug__ || level_ < 0) return;
  std::cout << "got a(n) " << level(level_) << ": [" << codename() << "] " << std::endl << std::flush;
 #endif
}

Error::Error(int code, int level_) : code{code}, critical{level_ == 0}{
  if (debug::__watchdog__) (*debug::__watchdog__) = level(level_) + ": " + codename();
 #if DEBUG
  if (code == 0) return;
  std::cout << "got a(n)" << level(level_) << ": ["  << codename() << "]: " << std::endl << std::flush;
 #else
#if PROFILING
  if (level_ < 0){
    time_t now  = time(0);
    char   buffer [80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));
    std::cout << "[ " << buffer << "]: "
              << "got a(n) " << level(level_) << ": [" << codename() << "] " << std::endl << std::flush;
  }
#endif
  if (code == 0 || !debug::__debug__ || level_ < 0) return;
  std::cout << "got a(n) " << level(level_) << ": [" << codename() << "] " << std::endl << std::flush;
 #endif
}
#endif

Error::~Error() {}

Error& Error::reason(std::string&& _message) {
 #if READABLE
 #if TRACE
  std::cout << " reason: " << _message << std::endl;
 #elif DEBUG
  if (code != 0 && !debug::__debug__) 
    std::cout << " reason: " << _message << std::endl;
 #endif
  message = _message;
 #else
  (void)_message;
 #endif
  return *this;
}

std::string Error::codename() {
  switch (code) {
    case base::error::ENoError:
      return "NoError";
    case base::error::EKeepContinue:
      return "KeepContinue";
    case base::error::ENoSupport:
      return "NoSupport";
    case base::error::EBadLogic:
      return "BadLogic";
    case base::error::EBadAccess:
      return "BadAccess";
    case base::error::EOutOfRange:
      return "OutOfRange";
    case base::error::ENotFound:
      return "NotFound";
    case base::error::EDrainMem:
      return "DrainMem";
    case base::error::EWatchErrno:
      return "WatchErrno";
    case base::error::EInterrupted:
      return "Interrupted";
    case base::error::EDoNothing:
      return "DoNothing";
    case base::error::EDoAgain:
      return "DoAgain";
    default:
      return std::to_string(code);
  }
}

bool Error::operator ==(Error&& dst){ return dst.code == code; }

Error::operator bool() { return code != 0 && critical; }

Error::operator int() { return code; }

std::string Error::position(const char* file, int line, const char* proc) {
  std::string result{};

  result.append(std::string{file})
        .append(":")
        .append(std::to_string(line))
        .append(" ")
        .append(proc, strlen(proc));
  return result;
}

Debug::Debug(bool mode) {
  debug::__lock__.lock();
  _rescue = debug::__debug__;
  debug::__debug__ = mode;
  debug::__lock__.unlock();
}

Debug::~Debug() {
  debug::__lock__.lock();
  debug::__debug__ = _rescue;
  debug::__lock__.unlock();
}

Watch::Watch(): _saved{debug::__watchdog__}{ debug::__watchdog__ = &this->msg; }

Watch::~Watch(){ debug::__watchdog__ = _saved; }
}  // namespace base

std::ostream& operator<<(std::ostream& stream, base::Error error) {
 #if READABLE
  stream << "[" << error.codename() << "]: " << error.message << " at " << error.debug << std::endl;
 #else
  stream << "got an error: [" << error.codename() << "]: " << std::endl;
 #endif
  return stream;
}

void writeLogW(const char* format, ...) {
  va_list vlist;

  va_start(vlist, format);
  vprintf(format, vlist);
  va_end(vlist);
}


