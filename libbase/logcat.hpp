/* copyright 2017 by lazycat and it is owned by lazycat */

#define TRACE     0
#define DEBUG     0
#define READABLE  0
#define PROFILING 0


#ifndef LIBBASE_LOGCAT_HPP_
#define LIBBASE_LOGCAT_HPP_

#define writeLog(message) printf("%s\n", message)

#if __cplusplus
#include <cstdarg>
#include <cstring>

#include <iostream>
#include <mutex>
#include <string>
#include <memory>

#if DEBUG
#include <cassert>
#endif

namespace base {
#if DEBUG
#define __ERROR_LOCATE__ base::Error::position(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#else
#define __ERROR_LOCATE__ ""
#endif
  
#if READABLE
#define pError(code, message) base::Error { __ERROR_LOCATE__, code, 0, std::forward<std::string>(message) }
#else
#define pError(code, message) base::Error { code , 0}
#endif

#if READABLE
#define warn(error) base::Error {std::forward<std::string>(error.debug), 1, error.code, std::forward<std::string>(error.message)}
#define mark(error) base::Error {std::forward<std::string>(error.debug), -1, error.code, std::forward<std::string>(error.message)}
#else
#define warn(error) base::Error {error.code, 1}
#define mark(error) base::Error {error.code, -1}
#endif

namespace error {
#endif

enum Code {
  ENoError = 0,
  EKeepContinue = 1,
  ENoSupport = 2,
  EBadLogic = 3,
  EBadAccess = 4,
  EOutOfRange = 5,
  ENotFound = 6,
  EDrainMem = 7,
  EWatchErrno = 8,
  EInterrupted = 9,
  EDoNothing = 10,
  EDoAgain = 11
};
#if __cplusplus
}

/* một số error cơ bản */
#if READABLE
#define NoError std::move(base::Error{__ERROR_LOCATE__, base::error::ENoError})
#define KeepContinue std::move(base::Error{__ERROR_LOCATE__, base::error::EKeepContinue})
#define NoSupport std::move(base::Error{__ERROR_LOCATE__, base::error::ENoSupport})
#define BadLogic std::move(base::Error{__ERROR_LOCATE__, base::error::EBadLogic})
#define BadAccess std::move(base::Error{__ERROR_LOCATE__, base::error::EBadAccess})
#define OutOfRange std::move(base::Error{__ERROR_LOCATE__, base::error::EOutOfRange})
#define NotFound std::move(base::Error{__ERROR_LOCATE__, base::error::ENotFound})
#define DrainMem std::move(base::Error{__ERROR_LOCATE__, base::error::EDrainMem})
#define WatchErrno std::move(base::Error{__ERROR_LOCATE__, base::error::EWatchErrno})
#define Interrupted std::move(base::Error{__ERROR_LOCATE__, base::error::EInterrupted})
#define DoNothing std::move(base::Error{__ERROR_LOCATE__, base::error::EDoNothing})
#define DoAgain std::move(base::Error{__ERROR_LOCATE__, base::error::EDoAgain})
#else
#define NoError std::move(base::Error{base::error::ENoError})
#define KeepContinue std::move(base::Error{base::error::EKeepContinue})
#define NoSupport std::move(base::Error{base::error::ENoSupport})
#define BadLogic std::move(base::Error{base::error::EBadLogic})
#define BadAccess std::move(base::Error{base::error::EBadAccess})
#define OutOfRange std::move(base::Error{base::error::EOutOfRange})
#define NotFound std::move(base::Error{base::error::ENotFound})
#define DrainMem std::move(base::Error{base::error::EDrainMem})
#define WatchErrno std::move(base::Error{base::error::EWatchErrno})
#define Interrupted std::move(base::Error{base::error::EInterrupted})
#define DoNothing std::move(base::Error{base::error::EDoNothing})
#define DoAgain std::move(base::Error{base::error::EDoAgain})
#endif

#if DEBUG
#define PAssert(condition) assert(condition)
#define BAssert(condition) assert(condition)
#else
#define PAssert(condition) { if ((condition)) throw BadLogic; }
#define BAssert(condition) { if ((condition)) return BadLogic; }
#endif

#define PCheck(error) { if (error.code) throw error; }
#define BCheck(error) { if (error.code) return error; }

struct Error {
#if READABLE
  std::string message;
  std::string debug;
#endif
  int  code;
  bool critical;

 public:
  explicit Error(std::string&& debug, int code = 0, int level = 0, std::string&& message = "");
#if !READABLE
  explicit Error(int code = 0, int level = 0);
#endif
  Error() = delete;
  
 public:
  Error& reason(std::string&& message);

  virtual std::string codename();
  virtual ~Error();

  operator bool();
  operator int();
  
  bool operator ==(Error&& dst);

  static std::string position(const char* file, int line, const char* proc);
};

struct Debug {
 public:
  explicit Debug(bool mode = false);
  virtual ~Debug();

 protected:
  Debug(){}
  
 private:
  bool _rescue;
};

struct Watch{
 public:
  std::string msg;

 public:
  Watch();
  ~Watch();

 private:
  std::string* _saved;
};
}  // namespase base
std::ostream& operator<<(std::ostream& stream, base::Error error);
#endif

#if __cplusplus
extern "C"{
#endif
void writeLogW(const char* format, ...);
#if __cplusplus
}
#endif
#endif  // LIBBASE_LOGCAT_HPP_
