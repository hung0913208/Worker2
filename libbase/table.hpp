#if !defined(LIBBASE_TABLE_HPP_) && __cplusplus
#define LIBBASE_TABLE_HPP_

#include <memory>
#include <functional>

namespace base{
template <typename Key, typename Entry> struct Table {
 public:
  explicit Table(std::size_t count, std::function<int(Key)> &&convert)
    : _index{std::make_unique<std::size_t[]>(count)},
      _entries{std::make_unique<Entry[]>(count)},
      _keys{std::make_unique<Key[]>(count)},
      _convert{convert},
      _max{count},
      _size{0} {
    for (auto i = count; i > 0; --i) _index[i - 1] = count;
    
  }

  Table(Table &) = delete;
  Table() = delete;

 public:
 #if USE_ARRAY_HASH_TABLE
  Entry &get(Key key) {
    auto pos = _convert(key) % _max;

    do
      if (_keys[pos] == key) return _entries[pos];
      else pos = _index[pos];
    while (pos != std::string::npos && pos != _max);
    throw NotFound;
  }

  auto put(Key &&key, Entry &value) {
    auto pos = _convert(key) % _max;
    auto par = pos;

    if (_index[pos] == _max) {
      _index[pos] = std::string::npos;
      _entries[pos] = value;
      _keys[pos] = key;
      return pos;
    }

    do
      par = _index[par];
    while (par != std::string::npos);

    for (auto i = pos; i > 0; --i) {
      if (_index[i - 1] != _max) continue;
      _index[pos] = std::string::npos;
      _index[par] = pos;
      _entries[pos] = value;
      _keys[pos] = key;
      return pos;
    }

    for (auto i = pos + 1; i < _max; ++i) {
      if (_index[i] == _max) continue;
      _index[pos] = std::string::npos;
      _index[par] = pos;
      _entries[pos] = value;
      _keys[pos] = key;
      return pos;
    }
    return std::string::npos;
  }
 #elif USE_TREE_HASH_TABLE
  Entry &get(Key key) { throw NotFound; }
  std::size_t put(Key &&key, Entry &&value) { return std::string::npos; }
 #else
 #error "can't select hashtable automatically, it need config manually"
 #endif

 private:
  std::unique_ptr<std::size_t[]> _index;
  std::unique_ptr<Entry[]> _entries;
  std::unique_ptr<Key[]> _keys;

 private:
  std::function<int(Key)> _convert;
  std::size_t _max, _size;
};
} // base

#endif // LIBBASE_TABLE_HPP_