#ifndef LIBBASE_GRAMMAR_GROUP_HPP_
#define LIBBASE_GRAMMAR_GROUP_HPP_

#include "libbase/pipe.hpp"

#include <functional>
#include <vector>

template<typename ByteT> using Lexical = std::shared_ptr<base::grammar::Grammar<ByteT>>;

namespace base{
namespace grammar{
template<typename ByteT>
struct Group: Grammar<ByteT>{ 
 public:
	explicit Group(std::vector<Lexical<ByteT>>&& target,
                 std::function<base::grammar::Status(std::size_t, base::grammar::Status)>&& in_,
                 std::function<base::grammar::Status(std::size_t, base::grammar::Status)>&& out_): Grammar<ByteT>{
    [&](std::size_t, ByteT  in)  -> Status{ 
      auto result = _deserialize << in;
      return in_(_deserialize.step(), result);
    }, 
    [&](std::size_t, ByteT& out) -> Status{
      auto result = _serialize   >> out; 
      return out_(_serialize.step(), result);}
    }, _serialize{target}, _deserialize{target}{}

 private:
  Serialize<ByteT>   _serialize;
  Deserialize<ByteT> _deserialize;
};
} // grammar
} // base
#endif