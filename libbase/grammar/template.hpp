#ifndef LIBBASE_GRAMMAR_TEMPLATE_HPP_
#define LIBBASE_GRAMMAR_TEMPLATE_HPP_

#include "operators.hpp"

namespace base {
namespace grammar {
template <typename ByteT=char>
Lexical<ByteT> make_grammar(std::vector<ByteT> samples){}

template <typename ByteT=char> 
Lexical<ByteT> make_grammar(std::basic_string<ByteT> sample){}

template <typename ByteT=char> 
Lexical<ByteT> make_grammar(ByteT sample){}

namespace pattern{
std::vector<char> regrex(std::string pattern){}

static auto letter = make_grammar<>(regrex("A-Za-z"));
static auto digit  = make_grammar<>(regrex("0-9"));
static auto number = (+make_grammar<>('-')) >> *(digit);
static auto string = make_grammar<>('\"') >> (*letter) >> make_grammar<>('\"');
} // namespace pattern
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_TEMPLATE_HPP_