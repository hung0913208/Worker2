#if !defined(LIBBASE_GRAMMAR_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_HPP_ 

#include "libbase/logcat.hpp"

#include <functional> 
#include <memory>

namespace base{
namespace grammar{
enum Status { Pal = -2, Unf = -1, Don = 1, Fal = 0, Inv = 2 };

template <typename ByteT=char>
struct Grammar: std::enable_shared_from_this<Grammar<ByteT>>{
 protected:
  using Iterator = ByteT*;

  struct Parser{ virtual bool parse(Iterator begin, Iterator end); };

 public:
  using Pull = std::function<Status(std::size_t, ByteT)>;
  using Push = std::function<Status(std::size_t, ByteT&)>;

 public:  
  template<typename Type> Grammar<ByteT>& operator[](std::function<void(Type&)> attribute);

 public:
  explicit Grammar(Pull pull, Push push): _result{Inv}, _pull{pull}, _push{push}{}
  virtual ~Grammar(){}

 public:
  inline Pull pull(){
    return [&](std::size_t idx, ByteT in) -> Status{
      if (_result != Fal && _result != Don){
        _result = _pull? _pull(idx, in): Fal;

        if (_result != Pal && _result != Unf && !_parser->parse(_begin, _end)) 
          return (_result = Fal);
        else return _result;
      } else return _result;
    };
  }

  inline Push push(){
    return [&](std::size_t idx, ByteT &out) -> Status{ 
      if (_result != Fal && _result != Don)
        return _push? _result = _push(idx, out): Fal;
      else return _result;
    };
  }

 protected:
  virtual bool support(const std::type_info&){ return false; };

 private:
  std::shared_ptr<Parser> _parser;

 private:
  Status   _result;
  Iterator _begin, _end;
  Pull     _pull;
  Push     _push;
};

template<typename Type, typename ByteT>
struct Attributor: public Grammar<ByteT>::Parser{
 protected:
  using Iterator = ByteT*;

 public:
  explicit Attributor(std::function<void(Type&)>&& attribute): _append{attribute}{
    _convert = [this](Iterator, Iterator) -> Status{ return (_append)? Fal: Don; };
  }

 public:
  bool parse(Iterator begin, Iterator end){ 
    switch(_convert(begin, end)){
     default:
     case Fal:
     case Inv:
      return false;

     case Don:
      _append(_buffer);
     case Unf:
     case Pal:     
      return true;
    }
  }

 protected:
  std::function<void(Type&)> _append;
  std::function<Status(Iterator, Iterator)> _convert;

 private:
  Type _buffer;
};

template<typename ByteT>
template<typename Type> inline Grammar<ByteT>& Grammar<ByteT>::operator[](std::function<void(Type&)> attribute){
  if (support(typeid(Type))) throw NoSupport;
  else _parser = std::make_shared<Attributor<Type, ByteT>>(attribute);
  return *this;
}
} // grammar
} // base
#endif