#ifndef LIBBASE_GRAMMAR_OPTIONAL_HPP_
#define LIBBASE_GRAMMAR_OPTIONAL_HPP_ 

#include <memory>
#include <vector>

#include "grammar.hpp"
#include "group.hpp"

template<typename ByteT> 
Lexical<ByteT> operator+(Lexical<ByteT> target){
  return std::make_shared<base::grammar::Grammar<ByteT>>(
    [target](std::size_t idx, ByteT in) -> base::grammar::Status{
      auto result = target->pull()(idx, in);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [target](std::size_t idx, ByteT& out) -> base::grammar::Status{
      auto result = target->push()(idx, out);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator+(std::vector<Lexical<ByteT>>&& target){
  return base::grammar::Group<ByteT>(target, 
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    });
}

template<typename ByteT> 
std::vector<Lexical<ByteT>>&& operator+(Lexical<ByteT> src, Lexical<ByteT> target){  
  return src >> (+target);
}
#endif // LIBBASE_GRAMMAR_OPTIONAL_HPP_