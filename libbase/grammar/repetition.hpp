#include <memory>
#include <vector>

#include "grammar.hpp"
#include "group.hpp"

namespace gram = base::grammar;

template<typename ByteT> 
Lexical<ByteT> operator*(Lexical<ByteT> target){
	return std::make_shared<base::grammar::Grammar<ByteT>>(
    [target](std::size_t idx, ByteT in) -> base::grammar::Status{
      auto result = target->pull()(idx, in);

      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [target](std::size_t idx, ByteT& out) -> base::grammar::Status{
      auto result = target->push()(idx, out);
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator*(std::vector<Lexical<ByteT>>&& targets){
	return std::make_shared<base::grammar::Group<ByteT>>(
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    });
}
