#ifndef LIBBASE_GRAMMAR_ALTERNATION_HPP_
#define LIBBASE_GRAMMAR_ALTERNATION_HPP_
#include <vector>
#include <memory>

#include "group.hpp"

namespace gram = base::grammar;

template <typename ByteT> 
Lexical<ByteT> operator|(Lexical<ByteT> left, Lexical<ByteT> right){	
	return std::make_shared<base::grammar::Grammar<ByteT>>(
  [left, right](std::size_t idx, ByteT in) -> base::grammar::Status{
  	auto lret = left->pull()(idx, in);
  	auto rret = right->pull()(idx, in);

  	return (lret == gram::Fal || lret == gram::Inv)? rret: lret;
  }, 
  [left, right](std::size_t idx, ByteT& out) -> base::grammar::Status{
  	auto ret = left->pull()(idx, out);

    if (ret == gram::Fal || ret == gram::Inv)
      return ret;
    else return right->pull()(idx, out);
  	return gram::Don;
  });
}

template <typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>>&& left, Lexical<ByteT> right){
	return right | base::grammar::Group<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(left), 
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT>
Lexical<ByteT> operator|(Lexical<ByteT> left, std::vector<Lexical<ByteT>>&& right){
	return right | left;
}

template<typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>>&& left, std::vector<Lexical<ByteT>>&& right){
	return left | base::grammar::Group<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(right), 
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}
#endif // LIBBASE_GRAMMAR_ALTERNATION_HPP_
