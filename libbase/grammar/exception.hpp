#include <memory>
#include <vector>

#include "grammar.hpp"
#include "group.hpp"

namespace gram = base::grammar;

template<typename ByteT> 
Lexical<ByteT>operator-(Lexical<ByteT>src, Lexical<ByteT>target){
  return std::make_shared<base::grammar::Grammar<ByteT>>(
    [src, target](std::size_t idx, ByteT in) -> base::grammar::Status{
      auto exclude = target->pull()(idx, in);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->pull()(idx, in);
    },
    [src, target](std::size_t idx, ByteT& out) -> base::grammar::Status{
      auto exclude = target->push(idx, out);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->push()(idx, out);
    });
}

template<typename ByteT> 
Lexical<ByteT>operator-(std::vector<Lexical<ByteT>>&& src, Lexical<ByteT>target){
  return base::grammar::Group<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(src),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    }) - target;
}

template<typename ByteT>
Lexical<ByteT>operator-(Lexical<ByteT>src, std::vector<Lexical<ByteT>>&& targets){
  return src - base::grammar::Group<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT>operator-(std::vector<Lexical<ByteT>>&& src, std::vector<Lexical<ByteT>>&& targets){
  return src - base::grammar::Group<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}
