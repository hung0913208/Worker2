#ifndef LIBWORKER_IFHLS_CPP_
#define LIBWORKER_IFHLS_CPP_
#define __STDC_CONSTANT_MACROS
#include "ifconvert.cpp"

#include "libbase/logcat.hpp"
#include "libhls.hpp"

using namespace amqt::consumer::converter;
using namespace amqt::consumer::hls;

static std::map<std::string, std::shared_ptr<M3U8>> _hls_contexts;
static std::mutex _lock_;
static bool _safe{true};

/** Init a hls context unless it exists. 
 *  @param url    the input url
 *  @param name   the name of this hls context, it's also the name pattern of chunks
 *  @param alias  the alias of chunks which redirect to another link
 *  @param length the desired length of chunks
 *  @return : > 0 if OK, othercases it is the error code which has been defined in logcat.hpp
 */
int rbitInitHLSContext(const char* url, const char* name, const char* alias, double length){
  if (name != nullptr && url != nullptr){
    auto error = rbitInitConsumer(url);

    if (!error) try {
    	std::string s_url{url};
    	
      _lock_.lock();
    	if (_hls_contexts.find(s_url) == _hls_contexts.end()){
        _hls_contexts[s_url] = std::make_shared<M3U8>(*rbitGetAllConsumers()[s_url], length);
        _hls_contexts[s_url]->name() = name;
        if (alias) _hls_contexts[s_url]->alias() = alias;
      }
      _lock_.unlock();
    	return base::error::ENoError;
    } catch(base::Error& error){ 
      _lock_.unlock();
      return error.code; 
    }
    return error;
  } else return base::error::EBadLogic;
}

/** Release an existed hls context.
 *  @param url the input url that has been used to init the hls context
 */
void rbitSafeHLSContext(int value){ 
  _lock_.lock();
  _safe = value != 0;
  _lock_.unlock();
}

/** Release an existed hls context.
 *  @param url the input url that has been used to init the hls context
 */
void rbitReleaseHLSContext(const char* url){
  if (url != nullptr){
    std::string s_url{url};

    _lock_.lock();
    _hls_contexts.erase(std::forward<std::string>(s_url));
    rbitGetAllConsumers().erase(std::forward<std::string>(s_url));
    _lock_.unlock();
  }
}

/** Release all hls context that has been cached 
 */
void rbitReleaseAllHLSContext(){
  _lock_.lock();
	for (auto& item: _hls_contexts)
		rbitReleaseConsumer(std::get<0>(item).c_str());
	_hls_contexts.clear();
  _lock_.unlock();
}

/** Generate a scrap of file m3u8
 *  @param url   the input url that has been used to init the hls context
 *  @param index if index > 0, this is the index of chunk, elsewhere it implies the header of m3u8
 *  @return null if it is fail or the scrap of file m3u8 
 */
char* rbitPrintM3U8(const char* url, int index){
  if (url) {
  	std::size_t s_idx  = index >= 0? static_cast<std::size_t>(index): std::string::npos;
    std::string s_url{url};

		try {
      if (_safe) _lock_.lock();
		  if (_hls_contexts.find(s_url) != _hls_contexts.end()){
        std::string result;

        if (index < 0){
          _hls_contexts[s_url]->properties()["PLAYLIST-TYPE"]  = "VOD";
          _hls_contexts[s_url]->properties()["VERSION"]        = "3";
          _hls_contexts[s_url]->properties()["MEDIA-SEQUENCE"] = "0";
          _hls_contexts[s_url]->properties()["TARGETDURATION"] = std::to_string(_hls_contexts[s_url]->length());
        }
        result = _hls_contexts[s_url]->sprint(result, s_idx);
        if (_safe) _lock_.unlock();
        return result.size() > 0? strdup(result.c_str()): nullptr;
      }
    } catch(base::Error &error){ 
      if (_safe) _lock_.unlock();
      return nullptr; 
    }
  }
  return nullptr;
}

void* memdup(const void* mem, size_t size) { 
  void* out = malloc(size);

  if(out != NULL)
      memcpy(out, mem, size);

  return out;
}

/** Generate a chunk
 *  @param url   the input url that has been used to init the hls context
 *  @param index this is the index of chunk
 *  @return a block which defines a chunk, including and its error report
 */
Block rbitPrintChunk(const char* url, int index){
  Block result;

  memset(&result, 0, sizeof(Block));
  if (url) {
    std::string  s_url{url};
    base::Watch  watch{};

    if (_safe) _lock_.lock();

    if (_hls_contexts.find(s_url) != _hls_contexts.end()){
      std::size_t s_idx{index >= 0? static_cast<std::size_t>(index): std::string::npos};
      
      if (!_hls_contexts[s_url]->ref(s_idx, true))
        result.error = base::error::ENotFound;
      else if (_hls_contexts[s_url]->transcode(index)){
        result.size  = _hls_contexts[s_url]->at(index)->size();
        result.data  = _hls_contexts[s_url]->at(index)->data();

        result.error = result.size? base::error::ENoError: base::error::EBadAccess;
        result.msg   = strdup (result.size? "NoError": "BadAccess");

        _hls_contexts[s_url]->unref(index);
      } else{
        result.error = base::error::EBadAccess;
        result.msg   = strdup(watch.msg.c_str());
      }
    } else {
      result.error = base::error::ENotFound;
      result.msg   = strdup("NotFound: url isn't initiated");
    }
    if (_safe) _lock_.unlock();
  } else{
    result.error = base::error::EBadLogic;
    result.msg   = strdup("BadLogic: url is None");
  }

  return result;
}

/** Generate files which use to play by hls player
 *  @param url     the input url that has been used to init the hls context
 *  @param storage where we want to contain output files
 *  @return a block which defines a chunk and 
 */
int rbitConcreteHLSCtx(const char* url, const char* storage){
	if (url && storage) {
    std::string s_url{url};
    std::size_t begin{s_url.rfind('/') + 1}, end{s_url.rfind('.')};
    std::string s_name = s_url.substr(begin, end - begin);

    if (_safe) _lock_.lock();
    if (_hls_contexts.find(s_url) != _hls_contexts.end()){
      bool result{true};

      _hls_contexts[s_url]->name()    = s_name;
      _hls_contexts[s_url]->storage() = storage;

        /* add basic properties for a VOD playlist */
      _hls_contexts[s_url]->properties()["PLAYLIST-TYPE"]  = "VOD";
      _hls_contexts[s_url]->properties()["VERSION"]        = "3";
      _hls_contexts[s_url]->properties()["MEDIA-SEQUENCE"] = "0";
      _hls_contexts[s_url]->properties()["TARGETDURATION"] = std::to_string(_hls_contexts[s_url]->length());
      _hls_contexts[s_url]->properties()["ALLOW-CACHE"]    = "YES";

      _hls_contexts[s_url]->fprint();
      for (auto i = 0; result && i < static_cast<decltype(i)>(_hls_contexts[s_url]->count()); ++i){
        result = result? _hls_contexts[s_url]->ref(i): result;
        result = result? _hls_contexts[s_url]->transcode(i): result;
				_hls_contexts[s_url]->unref(i);
			}
      if (_safe) _lock_.unlock();
			return result? base::error::ENoError: base::error::EBadLogic;
		}

    if (_safe) _lock_.unlock();
		return base::error::ENotFound;
  }
  return base::error::EBadLogic;
}

/** Check if this context has got the ending chunk
 *  @param url   the input url that has been used to init the hls context
 *  @return -1 if it is eof, 0 is not and > 0 if it got an error.
 * The result in this case is the error code which has been defined in logcat.hpp
 */
int rbitHLSContextEOF(const char* url){
  if (url){
    auto result = 0;

    if (_safe) _lock_.lock();
    if (rbitGetAllConsumers()[std::string{url}]->eof()) 
      result = -1;
    if (_safe) _lock_.unlock();
    return result;
  } else return base::error::EBadLogic;
}
#endif
