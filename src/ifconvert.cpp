/* copyright 2017 by lazycat and it is owned by lazycat */
#define __STDC_CONSTANT_MACROS

#include "libmedia.hpp"
#include "libhls.hpp"

#include <map>
#include <memory>
#include <string>
#include <utility>

using namespace amqt::consumer::converter;
using namespace amqt::consumer::hls;

std::map<std::string, std::shared_ptr<Context>>& rbitGetAllConsumers();

#ifndef LIBWORKER_IFHLS_CPP_

static std::map<std::string, std::shared_ptr<Context>> _mdi_contexts{};
static std::mutex _lock_;
static bool _register{false}, _safe{true};

std::map<std::string, std::shared_ptr<Context>>& rbitGetAllConsumers(){ return _mdi_contexts; }

/** register formats and codecs
 */
void rbitRegisterEverything() {
  if (!_register) {
    avformat_network_init();
    avcodec_register_all();
    avfilter_register_all();
    av_register_all();

    _register = true;
  }
}

/** Turn on and off safe access, it needs to be called when using api's functions with pthreads
 *  @param value  != 0 for turing on or == 0 for turning off
 */
void rbitSafeConsumer(int value){
  _lock_.lock();
  _safe = value != 0; 
  _lock_.unlock();
}

/** Create a new Consumer if it's not existed
 *  @param url  the input url
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitInitConsumer(const char* url) {
  rbitRegisterEverything();
  if (!url) return base::error::EBadLogic;

  try {
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) == _mdi_contexts.end()) {
      _mdi_contexts.emplace(s_url, std::make_shared<Context>(std::forward<std::string>(s_url)));
    }
    if (_safe) _lock_.unlock();
  } catch (base::Error& error) {
    if (_safe) _lock_.unlock();
    return error.code;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
  return base::error::ENoError;
}

/** Release the existed consumer
 *  @param url  the input url which has been used to init the consumer
 */
void rbitReleaseConsumer(const char* url) { 
  if (_safe) _lock_.lock();
  _mdi_contexts.erase(std::string{url}); 
  if (_safe) _lock_.unlock();
}

/** Release whole the existed consumers
 */
void rbitReleaseAllConsumer() { 
  if (_safe) _lock_.lock();
  _mdi_contexts.clear(); 
  if (_safe) _lock_.unlock();
}

/** Get attributes of consumer 
 *  @param url   the input url which has been used to init the consumer
 *  @param index the name of the attribute which we want to get
 *  @param type  the export type, the default value is 0, 0 --> json, 1 --> xml
 */
char* rbitGetAttribute(const char* url, const char* index, int type) {
  if (!url || !index)
    return nullptr;
  else try{
    base::TypePrinter ptype = base::print::getTypePrinter(type);
    std::string s_index{index};
    std::string s_url{url};
    char* result{nullptr};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = strdup(_mdi_contexts.at(s_url)
                      ->getAttribute(std::forward<std::string>(s_index), std::forward<base::TypePrinter>(ptype))
                      .c_str());

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return nullptr;
  }
}

/** print all informations of consumer
 *  @param url   the input url which has been used to init the consumer
 *  @param type  the export type, the default value is 0
 */
char* rbitDoTracking(const char* url, int type) {
  if (!url) return nullptr;
  else try{
    std::string s_url{url};
    char* result{nullptr};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = strdup(_mdi_contexts.at(s_url)
                        ->toString(std::forward<base::TypePrinter>(base::print::getTypePrinter(type)))
                        .c_str());

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return nullptr;
  }
}

/** Get attributes of consumer with the result's format is json
 *  @param url   the input url which has been used to init the consumer
 *  @param index the name of the attribute which we want to get
 */
char* rbitGetAttribute(const char* url, const char* index) {
  if (!url || !index) return nullptr;
  else try{
    std::string s_url{url};
    std::string s_index{index};
    char* result{nullptr};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = strdup(_mdi_contexts.at(s_url)
                        ->getAttribute(std::forward<std::string>(s_index))
                        .c_str());
    if (_safe) _lock_.unlock();
    return result;
  }catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return nullptr;
  }
}

/** Get codec of a stream in consumer
 *  @param url      the input url which has been used to init the consumer
 *  @param idstream the id of the stream 
 *  @result name of codec or null if it got an error
 */
char* rbitGetCodec(const char* url, int idstream) {
  if (!url) return nullptr;
  else try{
    std::string s_url{url};
    char* result{nullptr};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = strdup(_mdi_contexts.at(s_url)->getDecodec(idstream).c_str());

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return nullptr;
  }
}

/** Get width of a stream in consumer
 *  @param url      the input url which has been used to init the consumer
 *  @param idstream the id of the stream 
 *  @result width of the video stream or -${error code}
 */
int rbitGetWidth(const char* url, int idstream) {
  if (!url) return -base::error::EBadLogic;
  else try {
    std::string s_url{url};
    int result{0};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = _mdi_contexts.at(s_url)->getWidth(idstream);

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return 0;
  }
}

/** Get height of a stream in consumer
 *  @param url      the input url which has been used to init the consumer
 *  @param idstream the id of the stream 
 *  @result height of the video stream or -${error code}
 */
int rbitGetHeight(const char* url, int idstream) {
  if (!url)
    return -base::error::EBadLogic;
  else try {
    std::string s_url{url};
    int result{0};
     
    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = _mdi_contexts.at(s_url)->getWidth(idstream);

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return 0;
  }
}

/** Get duration of a stream in consumer
 *  @param url      the input url which has been used to init the consumer
 *  @param idstream the id of the stream 
 *  @result the duration of consumer which calculated in seconds
 */
double rbitGetDuration(const char* url) {
  if (!url)
    return base::error::EBadLogic;
  else try {
    std::string s_url{url};
    double result{0.0};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = _mdi_contexts.at(s_url)->getDuration();

    if (_safe) _lock_.unlock();
    return result;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return 0.0;
  }
}

/** Set quality of output's video streams
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param height the height of video output streams
 *  @param width  the width of video output streams
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetQuality(const char* url, const char* output, int height, int width) {
  if (!url || !output) return base::error::EBadLogic;

  try {
    std::string s_output{output};
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) == _mdi_contexts.end()){
      if (_safe) _lock_.unlock();
      return base::error::EOutOfRange;
    }

    _mdi_contexts.at(s_url)->setQuality(std::forward<std::string>(s_output), height, width);
    if (_safe) _lock_.unlock();
  } catch (base::Error& error) {
    if (_safe) _lock_.unlock();
    return error.code;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
  return base::error::ENoError;
}

/** Set encode of output's streams
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param codec  the codec name will use for these streams
 *  @param type   the type of streams(VIDEO, AUDIO, SUBTITLE)
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetEncode(const char* url, const char* output, const char* codec, int type) {
  if (!url || !output || !codec) return base::error::EBadLogic;
  try {
    std::string s_output{output};
    std::string s_codec{codec};
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) == _mdi_contexts.end()){
      if (_safe) _lock_.unlock();
      return base::error::EOutOfRange;
    }
    
    _mdi_contexts.at(s_url)->setEncodec(std::forward<std::string>(s_output), std::forward<std::string>(s_codec), type);    
    if (_safe) _lock_.unlock();
  } catch (base::Error& error) {
    if (_safe) _lock_.unlock();
    return error.code;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
  return base::error::ENoError;
}

/** Set new output for consumer
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetOutput(const char* url, const char* output) {
  if (!url || !output)
    return base::error::EBadLogic;
  else try {
    std::string s_output{output};
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      _mdi_contexts.at(s_url)->setOutput(std::forward<std::string>(s_output));
    else{
      if (_safe) _lock_.unlock();
      return base::error::ENotFound;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENoError;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
}

/** Set property of output codec
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param codec  the codec which use to set the property
 *  @param key    the key name of property
 *  @param value  the value of property
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetProperty(const char* url, const char* output, const char* codec, const char* key, const char* value){
  if (!url || !output || !codec || !key || !value)
    return base::error::EBadLogic;
  try{
    std::string s_output{output};
    std::string s_codec{codec};
    std::string s_url{url};

    std::string s_key{key};
    std::string s_value{value};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      _mdi_contexts.at(s_url)->at(std::forward<std::string>(s_output))
                         ->setProperty(std::forward<std::string>(s_codec), 
                                       std::forward<std::string>(s_key), 
                                       std::forward<std::string>(s_value));
    else{
      if (_safe) _lock_.unlock();
      return base::error::ENotFound;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENoError;
  } catch(base::Error& error){
    if (_safe) _lock_.unlock();
    return error.code;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
}

/** Set channel layof of output's audio streams
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param layout the layout type
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetChannelLayout(const char* url, const char* output, int layout) {
  if (!url || !output) return base::error::EBadLogic;
  else {
    std::string s_output{output};
    std::string s_url{url};

    try {
      if (_safe) _lock_.lock();
      if (_mdi_contexts.find(s_url) != _mdi_contexts.end()) 
        _mdi_contexts.at(s_url)->at(std::forward<std::string>(s_output))->setChannelLayout(layout);
      else{
        if (_safe) _lock_.unlock();
        return base::error::ENotFound;
      }
    } catch (base::Error& error) { 
      if (_safe) _lock_.unlock();
      return error; 
    } catch(std::out_of_range){
      if (_safe) _lock_.unlock();
      return NotFound;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENoError;
  }
}

/** Set frame rate of output's video streams
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param rate   the frame rate value
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetFramerate(const char* url, const char* output, int type, int rate) {
  if (!url || !output) return base::error::EBadLogic;
  else {
    std::string s_output{output};
    std::string s_url{url};

    try {
      if (_safe) _lock_.lock();
      if (_mdi_contexts.find(s_url) != _mdi_contexts.end()) {
        _mdi_contexts.at(s_url)
            ->at(std::forward<std::string>(s_output))
            ->setFrameRate(rate, type);

        if (_safe) _lock_.unlock();
        return base::error::ENoError;
      }

      if (_safe) _lock_.unlock();
      return base::error::ENotFound;
    } catch (base::Error& error) {
      if (_safe) _lock_.unlock();
      return error;
    } catch(std::out_of_range){
      if (_safe) _lock_.unlock();
      return NotFound;
    }
  }
}

/** Set bit filters of output streams
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 *  @param codec  the codec of streams
 *  @param type   the stream type
 *  @param filter the bitfilter name
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetBitFilter(const char* url, const char* output, const char* codec, int type, const char* filter) {
  if (!url || !output || !codec || !filter) return base::error::EBadLogic;
  else {
    std::string s_output{output};
    std::string s_url{url};
    std::string s_codec{codec};
    std::string s_filter{filter};

    try {
      if (_safe) _lock_.lock();
      if (_mdi_contexts.find(s_url) != _mdi_contexts.end()) {
        _mdi_contexts.at(s_url)
            ->at(std::forward<std::string>(s_output))
            ->setBitFilter(std::forward<std::string>(s_codec), type, std::forward<std::string>(s_filter));

        if (_safe) _lock_.unlock();
        return base::error::ENoError;
      }

      if (_safe) _lock_.unlock();
      return base::error::ENotFound;
    } catch (base::Error& error) {
      if (_safe) _lock_.unlock();
      return error;
    } catch(std::out_of_range){
      if (_safe) _lock_.unlock();
      return NotFound;
    }
  }
}

/** Set bitrate of output's streams
 *  @param url     the input url which has been used to init the consumer
 *  @param output  the output url which has been used when calling rbitSetOutput
 *  @param type    the stream type
 *  @param bitrate the bitrate value
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSetBitrate(const char* url, const char* output, int type, int bitrate) {
  if (!url || !output)
    return base::error::EBadLogic;
  else {
    std::string s_output{output};
    std::string s_url{url};

    try {
      if (_safe) _lock_.lock();
      if (_mdi_contexts.find(s_url) != _mdi_contexts.end()) {
        _mdi_contexts.at(s_url)->at(std::forward<std::string>(s_output))->setBitRate(bitrate, type);

        if (_safe) _lock_.unlock();
        return base::error::ENoError;
      }

      if (_safe) _lock_.unlock();
      return base::error::ENotFound;
    } catch (base::Error& error) {
      if (_safe) _lock_.unlock();
      return error;
    } catch(std::out_of_range){
      if (_safe) _lock_.unlock();
      return NotFound;
    }
  }
}

/** Generate a thumbnail at position
 *  @param url      the input url which has been used to init the consumer
 *  @param path     the path where it is used to contain the thumbnail
 *  @param position the time position of the frame which uses to produce a thumbnail
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitPrintThumbnail(const char* url, const char* path, double position) {
  if (!url || !path)
    return base::error::EBadLogic;
  else {
    std::string s_path{path};
    std::string s_url{url};

    try {
      if (_safe) _lock_.lock();
      if (_mdi_contexts.find(s_url) == _mdi_contexts.end())
        return base::error::ENotFound;
      else{
        auto result = _mdi_contexts.at(s_url)->getThumbnail(position, std::forward<std::string>(s_path), "mjpeg");
        if (_safe) _lock_.unlock();
        return result;
      }
    } catch (base::Error& error) {
      if (_safe) _lock_.unlock();
      return error;
    } catch(std::out_of_range){
      if (_safe) _lock_.unlock();
      return NotFound;
    }
  }
}

/** Apply changing of output's streams
 *  @param url      the input url which has been used to init the consumer
 *  @param output  the output url which has been used when calling rbitSetOutput
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitDoApplying(const char* url, const char* output) {
  if (!url || !output) return base::error::EBadLogic;
  try {
    std::string s_output{output};
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end()){
      auto result = _mdi_contexts.at(s_url)->at(std::forward<std::string>(s_output))->apply();

      if (_safe) _lock_.unlock();
      return result;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENotFound;
  } catch (base::Error& error) {
    if (_safe) _lock_.unlock();
    return error.code;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
}

/** Transcode streams from input to outputs
 *  @param url       the input url which has been used to init the consumer
 *  @param interrupt if == 1, this will do step by step and we need to set a loop to call to an end. 
 * Otherwhile, it will transcode directly.
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitDoTranscoding(const char* url, bool interrupt) {
  if (!url) return base::error::EBadLogic;
  else try {
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end()){
      auto result = _mdi_contexts.at(s_url)->toTranscode(interrupt);

      if (_safe) _lock_.unlock();
      return result;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENotFound;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
}

/** Seek to the time we want
 *  @param url     the input url which has been used to init the consumer
 *  @param seconds the time position that we desire to jump in
 *  @return the error code which has been defined in logcat.hpp
 */
int rbitSeekTime(const char* url, double seconds) {
  if (!url) return base::error::EBadLogic;
  else try {
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end()){
      if(_mdi_contexts.at(s_url)->seek(seconds)){
        if (_safe) _lock_.unlock();
        return base::error::ENoError;
      }
    }

    if (_safe) _lock_.unlock();
    return base::error::ENotFound;
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return NotFound;
  }
}


/** Get the current time position
 *  @param url     the input url which has been used to init the consumer
 *  @return the current time positon
 */
double rbitTellTime(const char* url) {
  double result{-1};

  if (url) try {
    std::string s_url{url};

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      result = _mdi_contexts.at(s_url)->tell();
    if (_safe) _lock_.unlock();
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
    return -1.0;
  }
  return result;
}


/** Remove an output
 *  @param url    the input url which has been used to init the consumer
 *  @param output the output url which has been used when calling rbitSetOutput
 */
void rbitRemoveOutput(const char* url, const char* output) {
  if (url && output) try {
    std::string s_output{output};
    std::string s_url{url};
    
    _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      _mdi_contexts.at(s_url)->remove(std::forward<std::string>(s_output));
  } catch(std::out_of_range){
    if (_safe) _lock_.unlock();
  }
}

/** Remove whole outputs of the consumer
 *  @param url the input url which has been used to init the consumer
 */
void rbitRemoveAllOutput(const char* url){
  if (url) try {
    std::string s_url{url};

    _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end())
      _mdi_contexts.at(s_url)->remove("");
    _lock_.unlock();
  } catch(std::out_of_range){
    _lock_.unlock();
  }
}

/** Generate files which use to play by hls player
 *  @param url     the input url that has been used to init the hls context
 *  @param storage where we want to contain output files
 *  @param length  the length of chunks
 *  @return a block which defines a chunk and 
 */
int rbitConcreteHLSCtx(const char* url, const char* storage, double length){
  if (url && storage) {
    std::string s_url{url};
    std::size_t begin{s_url.rfind('/') + 1}, end{s_url.rfind('.')};
    std::string s_name = s_url.substr(begin, end - begin);    

    if (_safe) _lock_.lock();
    if (_mdi_contexts.find(s_url) != _mdi_contexts.end()){
        M3U8 m3u8{*_mdi_contexts[s_url], length};
        bool result{true};

        m3u8.name()    = s_name;
        m3u8.storage() = storage;

        /* add basic properties for a VOD playlist */
        m3u8.properties()["PLAYLIST-TYPE"]  = "VOD";
        m3u8.properties()["VERSION"]        = "3";
        m3u8.properties()["MEDIA-SEQUENCE"] = "0";
        m3u8.properties()["TARGETDURATION"] = std::to_string(m3u8.length());
        
        m3u8.fprint();
        for (auto i = 0; result && i < static_cast<decltype(i)>(m3u8.count()); ++i){
            result = result? m3u8.ref(i): result;
            result = result? m3u8.transcode(i): result;
            m3u8.unref(i);
        }
        if (_safe) _lock_.unlock();
        return result? base::error::ENoError: base::error::EBadLogic;
    }

    if (_safe) _lock_.unlock();
    return base::error::ENotFound;
  }
  return base::error::EBadLogic;
}
#endif
