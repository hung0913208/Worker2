%module NWorker

 %{
 /* Includes the header in the wrapper code */
 #include "swworker.h"
 #include <iostream>
 #include <stdint.h>
 %}
#ifdef SWIGJAVA  
	%typemap(jstype) Block rbitPrintChunk "byte[]";
 	%typemap(jtype) Block rbitPrintChunk "byte[]";
	%typemap(jni) Block rbitPrintChunk "jbyteArray";

	%typemap(out) Block rbitPrintChunk {
		if ($1.size == 0) $result = $null;
		else if ($1.error) 
			JCALL2(ThrowNew, jenv, JCALL1(FindClass, jenv, "java/lang/Exception"), $1.msg);
		else if ($1.size > 0){
			$result = JCALL1(NewByteArray, jenv, $1.size);
			JCALL4(SetByteArrayRegion, jenv, $result, 0, $1.size, (const jbyte*)$1.data);
			free($1.data);
			free($1.msg);
		} else 
			JCALL2(ThrowNew, jenv, JCALL1(FindClass, jenv, "java/lang/Exception"), "Illegal access");
	}
#endif 
/* Parse the header file to generate wrappers */
%include "swworker.h"

