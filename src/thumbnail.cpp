#include "libmedia.hpp"
#include "libbase/logcat.hpp"
#include "libseek.hpp"

#include <cstdio>
#include <cstring>

#define USE_SAVE_CODEC_CONTEXT 1
namespace amqt {
namespace consumer {
namespace converter {
base::Error Context::getThumbnail(double position, std::string&& path, std::string&& codec) {
  auto save_cr_conv_pos = tell();
  auto error = NoError;
  
  try{
    position = _seekbar.find(position, [position](double pos, int flags) -> bool {
      return pos >= position && flags == AV_PKT_FLAG_KEY;
    });
  }catch(base::Error &error){ position = 0.0; }

  if (__has_video_stream() && this->seek(position, 0)){
    auto pix_codec     = avcodec_find_encoder_by_name(codec.c_str());
    auto pix_codec_ctx = pix_codec ? avcodec_alloc_context3(pix_codec) : nullptr;
    auto vstream       = _context->streams[__get_main_stream()];

    if (!pix_codec_ctx) return NoSupport;    
    else {
      pix_codec_ctx->height    = vstream->codecpar->height;
      pix_codec_ctx->width     = vstream->codecpar->width;
      pix_codec_ctx->time_base = vstream->time_base;

      /* we will use default settings, maybe these can be changed in the near future */
      pix_codec_ctx->pix_fmt   = AV_PIX_FMT_YUVJ420P;
      pix_codec_ctx->bit_rate  = 400000;      
    }

    if (avcodec_open2(pix_codec_ctx, pix_codec, nullptr) < 0) return BadAccess;
    else {
      auto save_main_decoding_flag   = toDecoder(__get_main_stream());
      auto save_converting_seekpoint = _cr_conv_pos;

#if USE_SAVE_CODEC_CONTEXT
      auto save_codec_ctx = std::get<0>(_decoders[__get_main_stream()]);
      auto codec_id  = _context->streams[__get_main_stream()]->codecpar->codec_id;
      auto codec     = avcodec_find_decoder(codec_id);
      auto codec_ctx = avcodec_alloc_context3(codec);

      if (avcodec_parameters_to_context(codec_ctx, _context->streams[__get_main_stream()]->codecpar) < 0){
        avcodec_free_context(&codec_ctx);
        return BadAccess;
      } else{
        codec_ctx->framerate = av_guess_frame_rate(_context, _context->streams[__get_main_stream()], nullptr);

        if (avcodec_open2(codec_ctx, codec, _options) < 0){
          avcodec_free_context(&codec_ctx);
          return BadAccess;
        } else std::get<0>(_decoders[__get_main_stream()]) = codec_ctx;
      }
#endif
      _cr_conv_pos = tell();
      toDecoder(__get_main_stream()) = true;
      error = convey([this, position, &error](Packet& packet) -> bool{
        auto& ipacket = std::get<0>(packet);
        auto  iframe  = std::get<1>(packet);

        /* accept this packet. Notice: we only have a packet, so we need pull */
        if (ipacket.stream_index != __get_main_stream() || std::get<2>(packet) != GotFrame)
          return false;
        
        auto a = seconds(std::get<0>(_decoders[__get_main_stream()])->time_base, iframe->pts);
        return a >= position;
      }, [&pix_codec_ctx, &path](Packet& packet) -> bool{
        if (std::get<2>(packet) == GotFrame){
          auto error = avcodec_send_frame(pix_codec_ctx, std::get<1>(packet));

          if (error >= 0){
            AVPacket opacket;
            FILE* pfile;

            av_init_packet(&opacket);

            if (avcodec_receive_packet(pix_codec_ctx, &opacket))
              return true;
            else if ((pfile = fopen(path.c_str(), "wb"))) {
              fwrite(opacket.data, 1, opacket.size, pfile);
              fclose(pfile);

              if (!avcodec_send_frame(pix_codec_ctx, nullptr))
                while(!avcodec_receive_packet(pix_codec_ctx, &opacket))
                  av_packet_unref(&opacket);
            }
          }

          if (error >= 0 || error != AVERROR(EAGAIN))
            return false;
        }
        return true;
      });


#if USE_SAVE_CODEC_CONTEXT
      avcodec_free_context(&codec_ctx);
      avcodec_free_context(&pix_codec_ctx);
      std::get<0>(_decoders[__get_main_stream()]) = save_codec_ctx;
#else 
      if (save_converting_seekpoint == _seekbar.begin())
        avcodec_flush_buffers(std::get<0>(_decoders[__get_main_stream()]));
#endif 
      toDecoder(__get_main_stream()) = save_main_decoding_flag;
      _cr_conv_pos                   = save_converting_seekpoint;
    }

    this->seek(save_cr_conv_pos);
  } else return NoSupport;
  return error;
}
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
