/* copyright 2017 by lazycat and it is owned by lazycat */

#define __STDC_CONSTANT_MACROS

#include "libmedia.hpp"
#include "utilities.hpp"

#include <sstream>
#include <string>
#include <tuple>
#include <utility>

const auto npos = std::string::npos;

namespace amqt {
namespace consumer {
namespace converter {
using Graph = std::tuple<AVFilterContext *, AVFilterContext *>;

int __select_channel_layout(AVCodec *codec) {
  if (!codec || !codec->channel_layouts)
    return AV_CH_LAYOUT_STEREO;
  else {
    auto p = codec->channel_layouts;
    auto best_ch_layout   = 0;
    auto best_nb_channels = 0;

    while (*p) {
      auto nb_channels = av_get_channel_layout_nb_channels(*p);

      if (nb_channels > best_nb_channels) {
        best_ch_layout   = *p;
        best_nb_channels = nb_channels;
      }
      p++;
    }
    return best_ch_layout;
  }
}

AVBSFContext *__new_bsf_context(std::string &&name) {
  auto filter = av_bsf_get_by_name(name.c_str());

  if (!filter)
    throw base::error::ENotFound;
  else {
    AVBSFContext *bsf_stream;

    if (av_bsf_alloc(filter, &bsf_stream) < 0) throw base::error::EDrainMem;
    return bsf_stream;
  }
}

/* ------------------------------------------------------------------------- */
Output::Output(std::string &&file, AllocIOCtx allocator, AVOutputFormat *format)
    : _applied{false}, _autorot{false}, _eof{false}, _vidstream{npos}, _aidstream{npos}{
  AVFormatContext *fmt{nullptr};

  if (avformat_alloc_output_context2(&fmt, format, nullptr, file.c_str()) < 0)
    throw NoSupport;
  else if (allocator && (fmt->pb = allocator())) _manual  = true;
  else if (avio_open(&fmt->pb, file.c_str(), AVIO_FLAG_WRITE) < 0) {
    avformat_free_context(fmt);
    throw BadAccess;
  } else _manual  = false;

  _context = fmt;  
}

Output::Output(AllocIOCtx allocator, AVOutputFormat *format)
    : _applied{false}, _autorot{false}, _eof{false}, _vidstream{npos}, _aidstream{npos}{
  AVFormatContext *fmt{nullptr};

  if (!allocator || !format) throw BadLogic;
  else if (avformat_alloc_output_context2(&fmt, format, nullptr, nullptr) < 0)
    throw NoSupport;
  else if (!(fmt->pb = allocator())){
    avformat_free_context(fmt);
    throw BadAccess;
  }
  _context = fmt;
  _manual  = true;
}

Output::~Output() {
  if (_context && _applied && !_manual) {
    av_write_trailer(_context);

    for (auto i = _context->nb_streams; i > 0; --i) {
      std::get<1>(_streams[i - 1]) = nullptr;
      std::get<2>(_streams[i - 1]) = nullptr;
    }
    avio_flush(_context->pb);
  }

  if (_context && !_manual) avio_closep(&_context->pb);
  avformat_free_context(_context);
}

void Output::copyEverything(AVStream *istream, std::size_t oidstream) {
  auto ostream = std::get<0>(_streams[oidstream]);
  auto enc_ctx = std::get<1>(_streams[oidstream])->context();  
  auto encodec = std::get<2>(_streams[oidstream])->codec;
  auto decodec = avcodec_find_decoder(istream->codecpar->codec_id);
  auto dec_ctx = decodec ? avcodec_alloc_context3(decodec) : nullptr;

  if (!dec_ctx) return;
  avcodec_parameters_to_context(dec_ctx, istream->codecpar);

  if (enc_ctx->extradata && enc_ctx->extradata_size) {
    avcodec_free_context(&dec_ctx);
    return;
  }

  if (_context->oformat->flags & AVFMT_GLOBALHEADER)
    enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

  enc_ctx->bit_rate               = enc_ctx->bit_rate ? enc_ctx->bit_rate : dec_ctx->bit_rate;
  enc_ctx->profile                = enc_ctx->profile == -99 ? dec_ctx->profile : enc_ctx->profile;
  enc_ctx->level                  = enc_ctx->level == -99 ? dec_ctx->level : enc_ctx->level;
  enc_ctx->time_base              = av_add_q(av_stream_get_codec_timebase(istream), (AVRational){0, 1});  
  enc_ctx->chroma_sample_location = dec_ctx->chroma_sample_location;

  switch (enc_ctx->codec_type) {
    case AVMEDIA_TYPE_VIDEO:
      enc_ctx->pix_fmt             = dec_ctx->pix_fmt;
      enc_ctx->width               = dec_ctx->width;
      enc_ctx->height              = dec_ctx->height;
      enc_ctx->sample_aspect_ratio = istream->sample_aspect_ratio;
      enc_ctx->has_b_frames        = dec_ctx->has_b_frames;

      if (encodec->pix_fmts)
        enc_ctx->pix_fmt = encodec->pix_fmts[0];
      else
        enc_ctx->pix_fmt = dec_ctx->pix_fmt;

      ostream->avg_frame_rate = istream->avg_frame_rate;
      ostream->time_base      = istream->time_base;
      break;

    case AVMEDIA_TYPE_AUDIO:
      enc_ctx->sample_fmt     = encodec->sample_fmts[0];
      enc_ctx->channel_layout = dec_ctx->channel_layout;
      enc_ctx->channels       = av_get_channel_layout_nb_channels(enc_ctx->channel_layout);
      enc_ctx->sample_rate    = dec_ctx->sample_rate;
      enc_ctx->time_base      = (AVRational){1, dec_ctx->sample_rate};
      break;

    case AVMEDIA_TYPE_SUBTITLE:
      enc_ctx->width  = dec_ctx->width;
      enc_ctx->height = dec_ctx->height;
      break;

    default:
      break;
  }

  avcodec_free_context(&dec_ctx);
}

base::Error Output::apply() {
  for (auto &stream : _streams) {
    if (std::get<1>(stream)->context() && !(*std::get<1>(stream))) {
      AVStream *avstream          = std::get<0>(stream);
      AVCodecContext *encodec_ctx = std::get<1>(stream)->context();
      AVCodec *encodec            = std::get<2>(stream)->codec;

      if (avcodec_open2(encodec_ctx, encodec, std::get<1>(stream)->options()) < 0)
        return BadAccess;
      else if (avcodec_parameters_from_context(avstream->codecpar, encodec_ctx) < 0)
        return BadAccess;
     #if VERSION == 2
      else if (avcodec_copy_context(avstream->codec, avstream) < 0)
        return BadAccess;
     #endif

      if (encodec_ctx->nb_coded_side_data) {
        for (auto i = 0; i < encodec_ctx->nb_coded_side_data; i++) {
          auto sd_src = &encodec_ctx->coded_side_data[i];
          auto dst_data = av_stream_new_side_data(avstream, sd_src->type, sd_src->size);

          if (!dst_data) return DrainMem;
          memcpy(dst_data, sd_src->data, sd_src->size);
        }
      }

      for (auto i = 0; i < avstream->nb_side_data; ++i) {
        auto sd = &avstream->side_data[i];
        auto dst = av_stream_new_side_data(avstream, sd->type, sd->size);

        if (!dst) return DrainMem;

        memcpy(dst, sd->data, sd->size);
        if (_autorot && sd->type == AV_PKT_DATA_DISPLAYMATRIX)
          av_display_rotation_set((int32_t *)dst, 0);
      }

      if (avstream->time_base.num <= 0 || avstream->time_base.den <= 0)
        avstream->time_base = av_add_q(avstream->time_base, AVRational{0, 1});

     #if VERSION == 2
      avstream->codec = avstream->codec;
     #endif
    }

    for (auto &bitfilter : std::get<1>(stream)->bfilters()) {
      if (avcodec_parameters_copy(std::get<0>(bitfilter)->par_in, std::get<0>(stream)->codecpar) < 0)
        return BadAccess;
      else if (av_bsf_init(std::get<0>(bitfilter)) < 0)
        return BadLogic;
    }
  }

#if DEBUG
  av_dump_format(_context, 0, nullptr, 1);
#endif

  if (avformat_write_header(_context, nullptr) < 0)
    return BadAccess;
  _applied = true;
  return NoError;
}

std::size_t Output::getStreamNext(AVStream *istream, std::size_t codec, std::size_t fr) {
  auto it  = _streams.begin() + (fr == npos ? 0 : fr);
  auto res = npos;

  if (_eof) return npos;
  for (; res == npos && it != _streams.end(); ++it) {
    auto codec_stream = std::get<1>(*it) ? nullptr : std::get<1>(*it)->context();
    auto codecpar     = std::get<0>(*it)->codecpar;

    if (_applied && (codec == npos || !codec_stream)) {
      if (codecpar->codec_type == istream->codecpar->codec_type) {
        if (codec != npos && codec != codecpar->codec_id) continue;
        return std::distance(_streams.begin(), it);
      }
    }

    if (!codec_stream) continue;
    if (codec_stream->codec_type == istream->codecpar->codec_type && codec_stream->codec_id == codec && codec != npos)
      res = std::distance(_streams.begin(), it);
  }

  return res;
}

std::size_t Output::getStream(AVStream *istream, AVCodec *codec) {
  auto res = npos;

  if (istream == nullptr) return npos;
  for (auto it = _streams.begin(); res == npos && it != _streams.end(); ++it) {
    auto codecpar = std::get<0>(*it)->codecpar;
    auto codec_stream = *std::get<1>(*it) ? nullptr : std::get<1>(*it)->context();

    if (_applied && (!codec || !codec_stream)) {
      if (codecpar->codec_type == istream->codecpar->codec_type) {
        if (codec && codec->id != codecpar->codec_id) continue;
        return std::distance(_streams.begin(), it);
      }
    }

    if (!codec_stream) continue;
    if (codec_stream->codec_type == istream->codecpar->codec_type && codec_stream->codec_id == codec->id && codec)
      res = std::distance(_streams.begin(), it);
  }

  if (res == npos && !_applied) {
    try {
      _streams.push_back(__new_output_stream(istream->codecpar->codec_type, codec));
      res = _streams.size() - 1;

      if (istream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        _aidstream = (__has_audio_stream())? _aidstream: res;
      else if (istream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        _vidstream = (__has_video_stream())? _vidstream: res;
    } catch (base::Error &error) { throw base::Error{error}; }

    if (!codec) {
      auto error = std::move(__init_stream_copystream(std::get<0>(_streams[res]), istream));
      if (error) throw error;
      if (!(std::get<1>(_streams[res])->context() = avcodec_alloc_context3(nullptr)))
        throw DrainMem;
      avcodec_parameters_to_context(std::get<1>(_streams[res])->context(), std::get<0>(_streams[res])->codecpar);
      std::get<1>(_streams[res])->parser() = av_parser_init(istream->codecpar->codec_id);
    } else {
      auto graph = avfilter_graph_alloc();

      if (!graph) throw DrainMem;
      std::get<2>(_streams[res]) = std::make_shared<Output::FilterGraph>(graph, codec, istream->avg_frame_rate, istream->time_base);
    }
  }

  return res;
}

std::vector<Output::BitFilter> &Output::getBitFilters(std::size_t id) {
  if (id >= _streams.size()) throw OutOfRange;
  return std::get<1>(_streams[id])->bfilters();
}

Output &Output::setProperty(std::string &&codec, std::string &&key, std::string&& value){
  auto encoder = avcodec_find_encoder_by_name(codec.c_str());

  if (_applied) return *this;
  if (!encoder) throw NotFound;
  for (auto& stream: _streams){
    if (std::get<2>(stream)->codec != encoder) continue;
    if (!std::get<1>(stream)->options())
      std::get<1>(stream)->options() = pmalloc<AVDictionary*>(1);

    if (std::get<1>(stream)->options())
      av_dict_set(std::get<1>(stream)->options(), key.c_str(), value.c_str(), 0);
  }
  return *this;
}

Output &Output::setBitFilter(std::string &&codec, int type, std::string &&filter) {
  auto pcodec = avcodec_find_encoder_by_name(codec.c_str());

  if (type < 0 && !pcodec) throw BadLogic;
  for (auto &stream : _streams) {
    auto codecpair = std::get<0>(stream)->codecpar;
    auto avstream  = std::get<1>(stream)->context();

    if (codec == "copy" && type == codecpair->codec_type) {
      if (!std::get<1>(stream)->context()) throw NoSupport;

      std::get<1>(stream)->bfilters().push_back(
        std::make_tuple(__new_bsf_context(std::forward<std::string>(filter)), 0));
    } else if (!avstream && codecpair->codec_id == pcodec->id) {
      if (!std::get<1>(stream)->context()) throw NoSupport;

      std::get<1>(stream)->bfilters().push_back(
        std::make_tuple(__new_bsf_context(std::forward<std::string>(filter)), 0));
    } else if (pcodec && codecpair->codec_id == pcodec->id) {
      if (!std::get<1>(stream)->context()) throw NoSupport;

      std::get<1>(stream)->bfilters().push_back(
        std::make_tuple(__new_bsf_context(std::forward<std::string>(filter)), 0));
    }
  }
  return *this;
}

Output &Output::setBitFilter(std::size_t idstream, AVBSFContext *filter) {
  if (idstream >= _streams.size()) throw OutOfRange;
  std::get<1>(_streams[idstream])->bfilters().push_back(std::make_tuple(filter, 0));
  return *this;
}

Output &Output::setChannelLayout(std::size_t idstream, int layout) {
  if (idstream >= _streams.size()) throw OutOfRange;
  if (std::get<1>(_streams[idstream])->context()->codec_type != AVMEDIA_TYPE_AUDIO)
    throw BadLogic;
  else if (std::get<1>(_streams[idstream])->context())
    return *this;

  else if (!avcodec_is_open(std::get<1>(_streams[idstream])->context())) {
    auto codec_id = std::get<1>(_streams[idstream])->context()->codec_id;
    auto codec    = layout ? nullptr : avcodec_find_encoder(codec_id);

    layout = layout ? layout : __select_channel_layout(codec);

    std::get<1>(_streams[idstream])->context()->channel_layout = layout;
    std::get<1>(_streams[idstream])->context()->channels       = av_get_channel_layout_nb_channels(layout);
  }
  return *this;
}

Output &Output::setQuality(std::size_t idstream, std::size_t width, std::size_t height) {
  if (idstream >= _streams.size()) throw OutOfRange;
  if (std::get<1>(_streams[idstream])->context()->codec_type != AVMEDIA_TYPE_VIDEO)
    throw BadLogic;
  else if (std::get<1>(_streams[idstream]))
    return *this;
  else if (!(*std::get<1>(_streams[idstream])) && !avcodec_is_open(std::get<1>(_streams[idstream])->context())) {
    AVRational aspect = av_d2q(double(width)/double(height), 100);
    base::Error error{NoError};
    std::string spec = "";
    
    spec.append(",scale=").append(std::to_string(width)).append(":").append(std::to_string(height)).append(":flags=bicubic");
    spec.append(",setdar=").append(std::to_string(aspect.num)).append("/").append(std::to_string(aspect.den));
    
    std::get<1>(_streams[idstream])->context()->width  = width;
    std::get<1>(_streams[idstream])->context()->height = height;
    std::get<1>(_streams[idstream])->context()->sample_aspect_ratio = AVRational{1, 1};

    std::get<2>(_streams[idstream])->addFiltSpec(std::forward<std::string>(spec));
  }
  return *this;
}

Output &Output::setFrameRate(std::size_t idstream, int rate) {
  if (idstream >= _streams.size())
    throw OutOfRange;
  else if (std::get<1>(_streams[idstream]))
    return *this;

  else if (!avcodec_is_open(std::get<1>(_streams[idstream])->context())) {
    std::get<1>(_streams[idstream])->context()->time_base.den = rate;
    std::get<1>(_streams[idstream])->context()->time_base.num = 1;

    if (std::get<1>(_streams[idstream])->context()->codec_type == AVMEDIA_TYPE_AUDIO)
      std::get<1>(_streams[idstream])->context()->sample_rate = rate;
  }
  return *this;
}

Output &Output::setBitRate(std::size_t idstream, int rate) {
  if (idstream >= _streams.size())
    throw OutOfRange;
  else if (std::get<1>(_streams[idstream]))
    return *this;

  else if (!avcodec_is_open(std::get<1>(_streams[idstream])->context()))
    std::get<1>(_streams[idstream])->context()->bit_rate = rate;
  return *this;
}

Output &Output::setPixFormat(std::size_t idstream, AVPixelFormat pix_fmt) {
  if (idstream >= _streams.size()) throw OutOfRange;
  if (std::get<1>(_streams[idstream])->context()->codec_type != AVMEDIA_TYPE_VIDEO)
    throw BadLogic;
  else if (std::get<1>(_streams[idstream]))
    return *this;
  else if (!avcodec_is_open(std::get<1>(_streams[idstream])->context()))
    std::get<1>(_streams[idstream])->context()->pix_fmt = pix_fmt;
  return *this;
}

Output &Output::setQuality(std::size_t width, std::size_t height) {
  for (auto &stream : _streams) {
    auto a = std::get<1>(stream);
    if (std::get<1>(stream)->context()->codec_type != AVMEDIA_TYPE_VIDEO || *std::get<1>(stream))
      continue;
    else if (!(*std::get<1>(stream)) && !avcodec_is_open(std::get<1>(stream)->context())) {
      AVRational aspect = av_d2q(double(width)/double(height), 100);
      base::Error error{NoError};
      std::string spec = "";
      
      spec.append(",scale=").append(std::to_string(width)).append(":").append(std::to_string(height)).append(":flags=bicubic");
      spec.append(",setdar=").append(std::to_string(aspect.num)).append("/").append(std::to_string(aspect.den));

      std::get<1>(stream)->context()->sample_aspect_ratio = AVRational{1, 1};
      std::get<1>(stream)->context()->width = width;
      std::get<1>(stream)->context()->height = height;

      std::get<2>(stream)->addFiltSpec(std::forward<std::string>(spec));
    }
  }
  return *this;
}

Output &Output::setPixFormat(AVPixelFormat pix_fmt) {
  for (auto &stream : _streams) {
    if (std::get<1>(stream)->context()->codec_type != AVMEDIA_TYPE_VIDEO || *std::get<1>(stream))
      continue;
    else if (!avcodec_is_open(std::get<1>(stream)->context()))
      std::get<1>(stream)->context()->pix_fmt = pix_fmt;
  }
  return *this;
}

Output &Output::setChannelLayout(int layout) {
  for (auto &stream : _streams) {
    if (std::get<1>(stream)->context()->codec_type != AVMEDIA_TYPE_AUDIO || *std::get<1>(stream))
      continue;
    else if (!avcodec_is_open(std::get<1>(stream)->context())) {
      auto codec_id = std::get<1>(stream)->context()->codec_id;
      auto codec = layout ? nullptr : avcodec_find_encoder(codec_id);

      layout = layout ? layout : __select_channel_layout(codec);

      std::get<1>(stream)->context()->channel_layout = layout;
      std::get<1>(stream)->context()->channels = av_get_channel_layout_nb_channels(layout);
    }
  }
  return *this;
}

Output &Output::setFrameRate(int rate, int type) {
  for (auto &stream : _streams) {
    if (std::get<1>(stream)->context()->codec_type != type || *std::get<1>(stream))
      continue;

    std::get<1>(stream)->context()->time_base.den = rate;
    std::get<1>(stream)->context()->time_base.num = 1;

    if (type == AVMEDIA_TYPE_AUDIO)
      std::get<1>(stream)->context()->sample_rate = rate;
  }
  return *this;
}

Output &Output::setBitRate(int rate, int type) {
  for (auto &stream : _streams) {
    if (std::get<1>(stream)->context()->codec_type != type || *std::get<1>(stream))
      continue;
    else if (!avcodec_is_open(std::get<1>(stream)->context()))
      std::get<1>(stream)->context()->bit_rate = rate;
  }

  return *this;
}

Output::Stream Output::__new_output_stream(std::size_t type, AVCodec* codec) {
  if (type == npos) throw NoSupport;
  else {
    auto stm = avformat_new_stream(_context, NULL);

    if (!stm) throw DrainMem;
    if (!codec) return std::make_tuple(stm, std::make_shared<Encode>(nullptr), nullptr);
    if (type == AVMEDIA_TYPE_VIDEO || type == AVMEDIA_TYPE_AUDIO) {
      auto avctx   = avcodec_alloc_context3(codec);

      if (!avctx) throw DrainMem;
      if (codec->pix_fmts) avctx->pix_fmt = codec->pix_fmts[0];

      avctx->sample_fmt = codec->sample_fmts ? codec->sample_fmts[0] : avctx->sample_fmt;
      avctx->codec_id   = static_cast<AVCodecID>(codec->id);
      avctx->codec_type = static_cast<AVMediaType>(type);

      if (_context->oformat->flags & AVFMT_GLOBALHEADER)
        avctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
      return std::make_tuple(stm, std::make_shared<Encode>(avctx), nullptr);
    }
    throw NoSupport;
  }
}

base::Error Output::__init_stream_copystream(AVStream *ostream, AVStream *istream) {
  /* dùng copy codec để hoàn tất việc còn lại */
  auto icdestream = avcodec_alloc_context3(nullptr);
  auto icodecpar  = avcodec_parameters_alloc();
  auto ocodecpar  = ostream->codecpar;
  auto codec_tag  = ocodecpar->codec_tag;

  AVRational sar{0, 1};

  /* check memory blocks are really allocated */
  if (!icdestream || !icodecpar) {
    avcodec_parameters_free(&icodecpar);
    avcodec_free_context(&icdestream);
    return DrainMem;
  }

  /* make a contemporary copying of input_stream's code parameters */
  icodecpar->codec_type = istream->codecpar->codec_type;
  if (avcodec_parameters_to_context(icdestream, istream->codecpar) < 0) {
    avcodec_parameters_free(&icodecpar);
    avcodec_free_context(&icdestream);
    return BadAccess;
  }
  avcodec_parameters_from_context(icodecpar, icdestream);

  /* load a new code_tag if it needs */
  if (!codec_tag) {
    unsigned int codec_tag_tmp;

    if (!_context->oformat->codec_tag ||
        av_codec_get_id(_context->oformat->codec_tag, icodecpar->codec_tag) == icodecpar->codec_id ||
        !av_codec_get_tag2(_context->oformat->codec_tag, icodecpar->codec_id, &codec_tag_tmp))
      codec_tag = icodecpar->codec_tag;
  }

  /* copy codec from input codec parameters to output codec parameters */
  if (avcodec_parameters_copy(ocodecpar, icodecpar) < 0) {
    avcodec_parameters_free(&icodecpar);
    avcodec_free_context(&icdestream);
    return BadLogic;
  }

  ocodecpar->codec_tag = codec_tag;

  if (avformat_transfer_internal_stream_timing_info(_context->oformat, ostream, istream, AVFMT_TBCF_AUTO) < 0) {
    avcodec_parameters_free(&icodecpar);
    avcodec_free_context(&icdestream);
    return BadAccess;
  }

  // copy timebase while removing common factors
  if (ostream->time_base.num <= 0 || ostream->time_base.den <= 0)
    ostream->time_base = av_add_q(av_stream_get_codec_timebase(istream), (AVRational){0, 1});

  // copy estimated duration as a hint to the muxer
  if (ostream->duration <= 0 && istream->duration > 0)
    ostream->duration = av_rescale_q(istream->duration, istream->time_base, ostream->time_base);

  // copy disposition
  ostream->disposition = istream->disposition;

  if (istream->nb_side_data) {
    for (auto i = 0; i < istream->nb_side_data; i++) {
      const AVPacketSideData *sd_src = &istream->side_data[i];
      uint8_t *dst_data = av_stream_new_side_data(ostream, sd_src->type, sd_src->size);

      if (!dst_data) return DrainMem;
      memcpy(dst_data, sd_src->data, sd_src->size);
    }
  }

  switch (ocodecpar->codec_type) {
    case AVMEDIA_TYPE_AUDIO:
      if ((ocodecpar->block_align == 1 || ocodecpar->block_align == 1152 || ocodecpar->block_align == 576) && 
          ocodecpar->codec_id == AV_CODEC_ID_MP3)
        ocodecpar->block_align = 0;

      if (ocodecpar->codec_id == AV_CODEC_ID_AC3) ocodecpar->block_align = 0;
      break;

    case AVMEDIA_TYPE_VIDEO:
      if (istream->sample_aspect_ratio.num)
        sar = istream->sample_aspect_ratio;
      else
        sar = icodecpar->sample_aspect_ratio;

      ostream->sample_aspect_ratio = ocodecpar->sample_aspect_ratio = sar;
      ostream->avg_frame_rate      = istream->avg_frame_rate;
      ostream->r_frame_rate        = istream->r_frame_rate;
      break;

    default:
      break;
  }

  ostream->time_base = istream->time_base;

  avcodec_parameters_free(&icodecpar);
  avcodec_free_context(&icdestream);
  return NoError;
}

bool Output::isCopyCodec(AVStream *left, AVStream *right) {
  if (!left || !right)
    throw BadLogic;
  else {
    auto lf_extra_data = left->codecpar->extradata;
    auto rg_extra_data = right->codecpar->extradata;
    auto ex_size       = std::max(left->codecpar->extradata_size, right->codecpar->extradata_size);
    auto lf_code_tag   = left->codecpar->codec_tag;
    auto rg_code_tag   = right->codecpar->codec_tag;
    auto res           = false;

    left->codecpar->extradata  = nullptr;
    right->codecpar->extradata = nullptr;

    left->codecpar->codec_tag  = 0;
    right->codecpar->codec_tag = 0;

    res = !memcmp(left->codecpar, right->codecpar, sizeof(AVCodecParameters));

    if (left->codecpar->extradata_size == right->codecpar->extradata_size)
      res = !memcmp(lf_extra_data, rg_extra_data, ex_size);

    left->codecpar->codec_tag  = lf_code_tag;
    right->codecpar->codec_tag = rg_code_tag;

    left->codecpar->extradata  = lf_extra_data;
    right->codecpar->extradata = rg_extra_data;
    return res;
  }
}

bool Output::__on_before_filting_packet(std::size_t, AVFrame*) { return true; }

bool Output::__on_after_filting_packet(std::size_t, AVFrame*){ return true; };

bool Output::__on_before_writing_packet(AVPacket*) { return true; }

bool Output::__on_after_writing_packet(std::size_t, double) { return true; }
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
