/* copyright 2017 by lazycat and it is owned by lazycat */

#define __STDC_CONSTANT_MACROS
#include "libmedia.hpp"
#include "logcat.hpp"
#include "utilities.hpp"

#include <unistd.h>

#define npos std::string::npos
#define getRlFilts(output, id) std::forward<std::vector<Output::BitFilter>>(output->getBitFilters(id))
#define get(output) std::get<1>(output)

using namespace base::error;

namespace amqt {
namespace consumer {
namespace converter {
using POutput = std::shared_ptr<Output>;
using OStream = std::tuple<std::string, std::size_t>;

base::Error Context::pull(Packet& packet) {
  auto error   = 0;

  if(std::get<2>(packet) == DoConverting) return NoError;

  /* state 0: finished, chua co ghi ca nen can load data vao packet */
  if (std::get<2>(packet) < 0 && !_flush) {
    av_packet_unref(&std::get<0>(packet));
    if (av_read_frame(_context, &std::get<0>(packet)) < 0)
        return DoNothing;
    else {
      /* load thanh cong, cap nhat vi tri cho seekbar */
      auto stream = _context->streams[std::get<0>(packet).stream_index];

      if (__get_main_stream() == std::get<0>(packet).stream_index){
        auto current = seconds(stream->time_base, std::get<0>(packet).dts);

        if (_seekbar == _cr_conv_pos) _cr_conv_pos  = current;
        _seekbar.seek(current, AVSEEK_FLAG_ANY);
      }
      /* lay duoc packet, chuyen trang thai va cho cho cac copying streams cap nhat het roi moi chuyen qua state tiep theo */
      std::get<2>(packet) = GotPacket;
      if (!_fast) return NoError;
    }
  }

  /*    Chi duoc goi khi da cap nhat xong packet cho cac copying streams, de dam bao tinh on dinh, packet khong bi xoa ngay
   * ma cho cho den khi lay packet ke tiep */
  if (toDecoder(std::get<0>(packet).stream_index) > 0 && _cr_conv_pos == tell()) {
    auto avctx   = std::get<0>(_decoders[std::get<0>(packet).stream_index]);
    auto decoder = std::get<0>(_decoders[std::get<0>(packet).stream_index]);
    auto istream = _context->streams[std::get<0>(packet).stream_index];    
    auto ipacket = std::get<0>(packet);

    char str_error[AV_ERROR_MAX_STRING_SIZE];

    av_packet_rescale_ts(&ipacket, istream->time_base, decoder->time_base);

    if (std::get<2>(packet) == 0) { // state 1: send packet vao decoder, ten la GotPacket
      error = avcodec_send_packet(avctx, &ipacket);

      if (error >= 0) std::get<2>(packet) = _fast? DupData: GotFrame;
      else if (error == AVERROR_EOF) 
        return NoError;
      else{
        release(packet);
        return NoError.reason(av_make_error_string(str_error, AV_ERROR_MAX_STRING_SIZE, error));
      }
    }

    if (!std::get<1>(packet) && (std::get<2>(packet) == GotFrame || std::get<2>(packet) == DupData)) {
      // state 2: receive frame tu decoder, ten la GotFrame
      if (!(std::get<1>(packet) = av_frame_alloc())) return DrainMem;
      else if ((error = avcodec_receive_frame(avctx, std::get<1>(packet))) < 0) {
        // khong nhan duoc gi ca, tu chuyen ve state 0: Finished
        av_frame_free(&std::get<1>(packet));
        std::get<2>(packet) = Finished;

        if (error != AVERROR(EAGAIN) && error != AVERROR_EOF)
          return BadAccess.reason(av_make_error_string(str_error, AV_ERROR_MAX_STRING_SIZE, error));
      }
    } 
  } else if (!_fast) std::get<2>(packet) = Finished;
  return NoError;
}

base::Error Context::push(Packet& packet) {
  auto istream = _context->streams[std::get<0>(packet).stream_index];
  auto iidx    = std::get<0>(packet).stream_index;
  auto error   = NoError;
  auto eof     = true;
  auto dup     = std::get<2>(packet) == DupData;
  auto status  = std::get<2>(packet);

  if (toDecoder(std::get<0>(packet).stream_index) < 0){
    std::get<2>(packet) = Finished;
    return NoError;
  }
  
  if (std::get<2>(packet) < GotPacket || _eof) 
    return base::Error{__ERROR_LOCATE__, base::error::EDoNothing, -1};

  for (auto& output : _outputs) {
    auto& poutput  = std::get<1>(output);
    auto  oidx     = poutput->getStreamNext(istream, npos, 0);

    for (; oidx != npos; oidx = poutput->getStreamNext(istream, npos, oidx + 1)) {
      auto ostream = poutput->at<0>(oidx);

      if (status == GotPacket || dup){ // state 1: ta moi chi lay duoc packet chua co frame nen phai ghi packet vao cac copying streams
        if (Output::isCopyCodec(istream, poutput->at<0>(oidx)) || *poutput->at<1>(oidx) == true) {
          /* do as copying packets from input to output */
          AVPacket clone, &root = std::get<0>(packet);

          av_init_packet(&clone);
          av_copy_packet(&clone, &root);
          if (root.pts != AV_NOPTS_VALUE)
            clone.pts = av_rescale_q(root.pts, istream->time_base, ostream->time_base);
          else clone.pts = AV_NOPTS_VALUE;

          if (root.dts == AV_NOPTS_VALUE)
            clone.dts = av_rescale_q(istream->cur_dts, AV_TIME_BASE_Q, ostream->time_base);
          else clone.dts = av_rescale_q(root.dts, istream->time_base, ostream->time_base);

          if (ostream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO && root.dts != AV_NOPTS_VALUE) {
            AVCodecContext *dec_ctx = std::get<0>(_decoders[iidx]);
            int duration = av_get_audio_frame_duration(dec_ctx, root.size);

            if (!duration) duration = dec_ctx->frame_size;
              clone.dts = clone.pts = av_rescale_delta(istream->time_base, root.dts,(AVRational){1, dec_ctx->sample_rate},
                                                       duration, &std::get<1>(_decoders[iidx]), ostream->time_base);
            }

          clone.duration = av_rescale_q(root.duration, istream->time_base, ostream->time_base);
          clone.flags    = root.flags;

          if (ostream->codecpar->codec_id != AV_CODEC_ID_H264 &&
              ostream->codecpar->codec_id != AV_CODEC_ID_MPEG1VIDEO &&
              ostream->codecpar->codec_id != AV_CODEC_ID_MPEG2VIDEO &&
              ostream->codecpar->codec_id != AV_CODEC_ID_VC1){
            auto ret = av_parser_change(poutput->at<1>(oidx)->parser(), poutput->at<1>(oidx)->context(), &clone.data, &clone.size, 
                                        root.data, root.size, root.flags & AV_PKT_FLAG_KEY);

            if (ret < 0) return BadAccess;
            else if (ret){
              clone.buf = av_buffer_create(clone.data, clone.size, av_buffer_default_free, NULL, 0);
              if (!clone.buf) return DrainMem;
            }
          } else{
            clone.data = root.data;
            clone.size = root.size;
          }
          av_copy_packet_side_data(&clone, &root);

          error = poutput->write(oidx, &clone);
          eof   = false;
        } else if(!dup) continue; // neu can decoder thi phai cho qua luot tiep theo 
      } 

      if (status >= GotFrame) { // state 2: ta nhan duoc frame nen ghi vao cac encoder
        if (Output::isCopyCodec(istream, poutput->at<0>(oidx))){
          if(!dup) continue;
        } else{
          if (ostream->duration <= 0 && istream->duration > 0)
            ostream->duration = av_rescale_q(istream->duration, istream->time_base, ostream->time_base);
        
          if (!poutput->configureFiltergraph(std::get<0>(_decoders[iidx]), oidx)){
            if ((status == DoConverting) ^ (poutput->at<1>(oidx)->status == Finished))
              error = poutput->write(oidx, std::get<1>(packet));
            eof = false;
          }
        }
      }

      /* giu nguyen trang thai va tiep tuc cho den khi doi trang thai moi neu dang o state 3*/
      if (std::get<2>(packet) > 0 && error.code == base::error::EKeepContinue){
        /* chuyen len state 3 neu dang o state 2 va can duy tri trang thai */
        std::get<2>(packet) = DoConverting; 
      } else if (error) return error;
      else if (std::get<2>(packet) == DoConverting)
        std::get<2>(packet) = Output::isCopyCodec(istream, poutput->at<0>(oidx)) && dup? DoConverting: GotFrame;
      
      /*  neu khong co loi, moi thu okey va da ket thuc, chuyen qua frame moi, nguoc lai huy tat ca cac frame 
       * vi chung ta khong chac nguyen nhan gay loi. Co the ngat cac stream loi va chay tiep neu muon */
    }
  }

  if (std::get<2>(packet) == GotFrame) av_frame_free(&std::get<1>(packet));
  else if (std::get<2>(packet) == GotPacket) std::get<2>(packet) = Finished;
  _eof = eof;
  return error;
}

base::Error Context::convey(Observer condition, Observer action){
  auto error = NoError;
  
  try {
    Packet packet;

    std::get<2>(packet) = Finished; // need to set this because of the random setting of system

    _fast = false;
    while (!error && !(error = pull(packet))) 
      if (condition){
        if (condition(packet)){
          if (action){ if (!action(packet)) break; } 
          else error = push(packet);
        } else if (std::get<2>(packet) == GotPacket)
          std::get<2>(packet) = toDecoder(std::get<0>(packet).stream_index) > 0? GotPacket: Finished;
        else if (std::get<2>(packet) == GotFrame)
          av_frame_free(&std::get<1>(packet));        
      } else error = push(packet);
    release(packet);
  } catch(base::Error& error) { return error; }

  return error;
}

void Context::release(Packet& packet){
  av_packet_unref(&std::get<0>(packet));
  av_frame_free(&std::get<1>(packet));

  std::get<2>(packet) = Finished;
}

base::Error Output::write(size_t idstream, AVPacket* packet) {
  /* FUTURE: do video-audio sync */
  auto ostream = _context->streams[idstream];
  auto current = seconds(ostream->time_base, packet->pts);
  auto filters = at<1>(idstream)->bfilters();
  auto ret     = 0;

  packet->stream_index = idstream; 
  if (filters.size() == 0 || packet->size == 0){}
  else if ((ret = av_bsf_send_packet(std::get<0>(filters[0]), packet)) < 0){
    if (ret != AVERROR_EOF) return BadAccess;
  } else for (auto it = filters.begin(); it != filters.end(); ++it) {
    /* get a packet from the previous filter up the chain */
    auto avfilt_next = it + 1 == filters.end()? nullptr: std::get<0>(*(it + 1));
    auto avfilt_curr = std::get<0>(*it);
    auto step_load   = std::get<1>(*it);

    if (av_bsf_receive_packet(avfilt_curr, packet) < 0) return BadLogic;
    if (!step_load && avcodec_parameters_copy(ostream->codecpar, avfilt_curr->par_out) < 0)
      return BadLogic;
    step_load = (std::get<1>(*it) |= 1);

    /* send it to the next filter down the chain or to the muxer */
    if (!avfilt_next) continue;
    if (!(step_load & 2) && avcodec_parameters_copy(avfilt_next->par_out, avfilt_curr->par_out) < 0)
      return BadLogic;
    std::get<1>(*it) |= 2;

    if ((ret = av_bsf_send_packet(avfilt_next, packet)) < 0){
      if (ret != AVERROR_EOF) return BadLogic;
      else break;
    }
  }  

  if (!_eof && _context->streams[idstream]->cur_dts < packet->dts){
    if (__on_before_writing_packet(packet) && av_interleaved_write_frame(_context, packet) < 0)
      return BadAccess;
    else if (__on_after_writing_packet(idstream, current)) return NoError;
  }
  av_packet_unref(packet);
  return NoError;
}

base::Error Output::write(std::size_t idstream, AVFrame* &ioframe){
  auto& ostream = std::get<0>(_streams[idstream]);
  auto& encoder = std::get<1>(_streams[idstream]);
  auto& filter  = std::get<2>(_streams[idstream]);
  auto  avctx   = encoder->context();

  AVFrame* oframe  = nullptr;

  if (encoder->status == Context::Finished){
    /* state 3.1 dua frame vao filter_graph */
    auto flags = AV_BUFFERSRC_FLAG_KEEP_REF | AV_BUFFERSRC_FLAG_PUSH;
    int error = 0;
    if (ioframe) ioframe->pts = av_frame_get_best_effort_timestamp(ioframe);
    if (ioframe && !__on_before_filting_packet(idstream, ioframe) && !_eof)
      return BadLogic;
    else if ((error = av_buffersrc_add_frame_flags(filter->bfsrc, ioframe, flags)) < 0){
      std::cout << (filter->bfsrc != nullptr) << std::endl << std::flush;
      std::cout << (ioframe != nullptr) << std::endl << std::flush;
      std::cout << error << std::endl << std::flush;
      return BadAccess;
    }
    encoder->status = Context::PreparedFilting;
  }

  if (encoder->status == Context::PreparedFilting){    
    /* state 3.2 lay frame tu filter_graph */ 
    if ((oframe = av_frame_alloc())) {
      int iderror = av_buffersink_get_frame_flags(filter->bfsink, oframe, 0);

      if (!ioframe && iderror < 0){
        /* flush encode, keep this state alive until everything finishes */
        av_frame_free(&oframe);
        encoder->status = Context::GotFiltFrame;
      } else if (iderror == AVERROR(EAGAIN) || iderror == AVERROR_EOF){
        encoder->status = Context::Finished;
        av_frame_free(&oframe);
        return NoError;
      } else if (iderror < 0){
        av_frame_free(&oframe);
        return BadAccess;
      } else {
        if (oframe && !__on_after_filting_packet(idstream, oframe))
          return BadLogic;
        oframe->pict_type = AV_PICTURE_TYPE_NONE;    
        encoder->status   = Context::GotFiltFrame;
      }
    } else return DrainMem;
  }


  if (encoder->status == Context::GotFiltFrame){
    /* state 3.3 send frame vao encoder */
    int iderror = 0;
    
    if (((iderror = avcodec_send_frame(avctx, oframe)) >= 0)){
      av_frame_free(&oframe);
      encoder->status = Context::SentFrame;
    } else if (iderror != AVERROR(EAGAIN))
      return BadAccess;
    else{      
      ioframe = oframe;
      return base::Error{__ERROR_LOCATE__, base::error::EKeepContinue, -1};
    }
  }

  if (encoder->status == Context::SentFrame){
    /* state 3.4 receive frame tu encoder */
    base::Error error{NoError};
    AVPacket packet;
    int ret;

    av_init_packet(&packet);

    if ((ret = avcodec_receive_packet(avctx, &packet))){
      /* khong nhan duoc packet nao ca, tien hanh doc tiep du lieu tu filter_graph */ 
      encoder->status = Context::PreparedFilting;
      _eof = _eof || (!ioframe && ret == AVERROR_EOF);
      return ioframe || ret != AVERROR_EOF? base::Error{__ERROR_LOCATE__, base::error::EKeepContinue, -1}
                                          : NoError;
    } else{
      /* nhan duoc packet, tien hanh ghi du lieu vao stream */ 
      av_packet_rescale_ts(&packet, encoder->context()->time_base, ostream->time_base);
      packet.dts = (packet.pts < packet.dts)? AV_NOPTS_VALUE: packet.dts;
      return (error = std::forward<base::Error>(write(idstream, &packet)))? error: 
                                  base::Error{__ERROR_LOCATE__, base::error::EKeepContinue, -1};
    }
  }
  return BadLogic;
}

base::Error Output::flush(){
  auto nilframe = (AVFrame*)nullptr;
  auto error    = NoError;

  if (_eof) return error;
  for (auto i = _streams.size(); i > 0; --i){
    if (*std::get<1>(_streams[i - 1])) 
      continue;
    else if(std::get<2>(_streams[i - 1])->bfsrc == nullptr)
      return BadAccess;

    error = write(i - 1, nilframe);
    if (error && error.code != base::error::EKeepContinue)
      return error;
  }
  return error;
}
}  // namespace converter
}  // namespace consumer
}  // namespace amqt 
