
#include "libmedia.hpp"
#include "libbase/logcat.hpp"


namespace amqt {
namespace consumer {
namespace converter {
base::Error Context::toTranscode(bool interrupted) {
  auto error = NoError;
  auto keep_ = false;

  _fast = true;
  do {
    keep_ = false;

    if (_eof || pull(packet).code == base::error::EDoNothing) {
      /* flushing encoders converting frames */
      for (auto& output : _outputs) {
        /* for the first calling, we need to makesure that every output has inited its filtergraph */
        for (std::size_t iidx = 0; !_eof && iidx < _decoders.size(); ++iidx) {
          auto& poutput = std::get<1>(output);
          auto  oidx    = poutput->getStreamNext(_context->streams[iidx], std::string::npos, 0);

          for (; oidx != std::string::npos; ) {
            /*    if output stream isn't been copying, we must call configureFiltergraph. This action doesn't 
             * take much time because it has been design to init if it is the first time */
            if (!Output::isCopyCodec(_context->streams[iidx], poutput->at<0>(oidx)) && *poutput->at<1>(oidx) == false) {
              if ((error = std::move(poutput->configureFiltergraph(std::get<0>(_decoders[iidx]), oidx))))
                return error;
            }
            oidx = poutput->getStreamNext(_context->streams[iidx], std::string::npos, oidx + 1);
          }
        }

        /*  flush null frame to release encoders. Because we need to finish every encoder. Therefore, variable keep_
         * has been sign true if flushing tell that they need one more steps.
         */
        error = std::move(std::get<1>(output)->flush());
        keep_ = keep_? keep_: error.code == base::error::EKeepContinue;
      }
      _eof = true;
    } else if (!(error = std::move(push(packet))))
      keep_ = true;
    else if (error.code == base::error::EDoNothing || error.code == base::error::EKeepContinue)
      keep_ = true;

    if (interrupted && keep_) return base::Error{__ERROR_LOCATE__, base::error::EKeepContinue, -1};
  } while (keep_);

  if (_eof && !keep_) {
    /* the input has been transcoded completedly. Now, we need to turn back to the first position */
  }
  release(packet);
  return error;
}
} // converter
} // consumer
} // amqt