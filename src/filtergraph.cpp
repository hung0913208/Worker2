/* copyright 2017 by lazycat and it is owned by lazycat */
#include "libmedia.hpp"  

#define get(stream) std::get<2>(stream).get()

namespace amqt {
namespace consumer {
namespace converter {

/** configue a new filter graph if it hasn't created
 *  @param decctx   the input stream's codec context
 *  @param idstream the id of output stream
 *  @return an error object
 */
base::Error Output::configureFiltergraph(AVCodecContext* decctx, std::size_t idstream) {
  if (!decctx) return NoError;
  return __configure_filtergraph(decctx, _streams[idstream], std::get<2>(_streams[idstream])->getFiltSpecs());
}

base::Error __configure_output_filter(AVFilterContext* sink, std::string&& key, uint8_t* data, std::size_t size) {
  if (av_opt_set_bin(sink, key.c_str(), data, size, AV_OPT_SEARCH_CHILDREN) < 0)
      return BadLogic;
  return NoError;
}

base::Error Output::__configure_filtergraph(AVCodecContext* decctx, Output::Stream& stream, std::string&& spec) {    
  auto enc_ctx = std::get<1>(stream)->context();
  auto outputs = (AVFilterInOut*)nullptr; 
  auto inputs  = (AVFilterInOut*)nullptr;
  auto graph   = get(stream)->graph;
  auto result  = NoError;

  char args[512];

  if (!graph) {
    if (!(graph = avfilter_graph_alloc())) return DrainMem;
    else get(stream)->graph = graph;

  } else if(get(stream)->bfsrc && get(stream)->bfsink)
    return NoError;
  
  if (!(outputs = avfilter_inout_alloc()))
    return DrainMem;
  if (!(inputs = avfilter_inout_alloc()))
      return DrainMem;

  if (std::get<1>(stream)->context()->codec_type == AVMEDIA_TYPE_VIDEO) {
    auto bfsrc  = avfilter_get_by_name("buffer");
    auto bfsink = avfilter_get_by_name("buffersink");
    auto avctx  = std::get<1>(stream)->context();

    std::string format;

    snprintf(args, sizeof(args),                                                
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d:frame_rate=%d/%d",  
             decctx->width, decctx->height, decctx->pix_fmt, 
             get(stream)->smprate.num, get(stream)->smprate.den, 
             decctx->sample_aspect_ratio.num, decctx->sample_aspect_ratio.den,
             get(stream)->frmrate.num, get(stream)->frmrate.den);
    
    if (get(stream)->isBuilt(args, std::forward<std::string>(spec))) {
      avfilter_inout_free(&inputs);
      avfilter_inout_free(&outputs);
      return NoError;
    }

    if (avfilter_graph_create_filter(&get(stream)->bfsrc, bfsrc, "in", args, nullptr, graph) < 0)
      result = DrainMem;
    else if (avfilter_graph_create_filter(&get(stream)->bfsink, bfsink, "out", nullptr, nullptr, graph) < 0)
      result = DrainMem;

    result = !result ? __configure_output_filter(get(stream)->bfsink, "pix_fmts",
                                                reinterpret_cast<uint8_t*>(&avctx->pix_fmt), 
                                                sizeof(avctx->pix_fmt))
                      : result;
    if (spec.length() > 0){
      for (auto p = get(stream)->codec->pix_fmts; *p != AV_PIX_FMT_NONE; p++) {
        format.append(av_get_pix_fmt_name(*p));

        if (p[1] != AV_PIX_FMT_NONE)
          format.append("|");
      }

      if (format.length() > 0 )
        spec.append(",").append("format=").append(format);
    }
  } else if (std::get<1>(stream)->context()->codec_type == AVMEDIA_TYPE_AUDIO) {
    auto bfsrc  = avfilter_get_by_name("abuffer");
    auto bfsink = avfilter_get_by_name("abuffersink");

    
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%lx",     
             decctx->time_base.num, decctx->time_base.den, decctx->sample_rate, 
             av_get_sample_fmt_name(decctx->sample_fmt), decctx->channel_layout);


    if (get(stream)->isBuilt(args, std::forward<std::string>(spec))) {
      avfilter_inout_free(&inputs);
      avfilter_inout_free(&outputs);
      return NoError;
    }

    if (avfilter_graph_create_filter(&get(stream)->bfsrc, bfsrc, "in", args, nullptr, graph) < 0)
      result = DrainMem;
    else if (avfilter_graph_create_filter(&get(stream)->bfsink, bfsink, "out", nullptr, nullptr, graph) < 0)
      result = DrainMem;

    if (av_opt_set_bin(&get(stream)->bfsink, "sample_fmts", (uint8_t*)&enc_ctx->sample_fmt,
                       sizeof(enc_ctx->sample_fmt), AV_OPT_SEARCH_CHILDREN) < 0) {
    }
    if (av_opt_set_bin(&get(stream)->bfsink, "channel_layouts", (uint8_t*)&enc_ctx->channel_layout,
                       sizeof(enc_ctx->channel_layout), AV_OPT_SEARCH_CHILDREN) < 0) {
    }
    if(av_opt_set_bin(&get(stream)->bfsink, "sample_rates", (uint8_t*)&enc_ctx->sample_rate,
                      sizeof(enc_ctx->sample_rate), AV_OPT_SEARCH_CHILDREN) < 0){}
  }

  outputs->name       = av_strdup("in");
  outputs->filter_ctx = get(stream)->bfsrc;
  outputs->pad_idx    = 0;
  outputs->next       = NULL;

  inputs->name        = av_strdup("out");
  inputs->filter_ctx  = get(stream)->bfsink;
  inputs->pad_idx     = 0;
  inputs->next        = NULL;

  if (!result) {
    if (avfilter_graph_parse_ptr(graph, spec.c_str(), &inputs, &outputs, nullptr) < 0)
      result = BadLogic;
    else if (avfilter_graph_config(graph, NULL) < 0) result = BadLogic;
  } else {
    get(stream)->bfsink = nullptr;
    get(stream)->bfsrc  = nullptr;
  }
  avfilter_inout_free(&inputs);
  avfilter_inout_free(&outputs);
  return result;
}

Output::FilterGraph::FilterGraph(AVFilterGraph* graph, AVCodec* codec, AVRational frmrate, AVRational smprate): 
    bfsink{nullptr}, bfsrc{nullptr}, graph{graph}, codec(codec), frmrate(frmrate), smprate(smprate) {
      if (codec){
        switch(codec->type){
        case AVMEDIA_TYPE_AUDIO:
          _description = "anull";
          break;

        case AVMEDIA_TYPE_VIDEO:
          _description = "null";
          break;

        default:
          break;
        }
      }
    }

Output::FilterGraph::~FilterGraph() {
  avfilter_graph_free(&graph);
}
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
