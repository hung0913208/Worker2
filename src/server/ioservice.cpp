#include "libio.hpp"

namespace amqt {
namespace server {
IOService::IOService(std::size_t maxconn, int backlog): 
    result{base::error::ENoError}, _table{maxconn, [](int key) -> int { return key; }}, _backlog{backlog}, _fdesc{ioCreatePoll()} {
  _in = [this](int fd) -> int {
    auto error = 0;

    result = base::error::ENoError;

    if (_endpoints.count(fd) <= 0) {
      /* perform like an binder */
      auto& endpoint = _table.get(fd);
      auto binder    = std::dynamic_pointer_cast<endpoint::Binder>(endpoint);

      current = endpoint;

      if (binder->_in)
        error = binder->_in(&const_cast<IOClass&>(binder->ioclass), fd);
      /* notice: by default if we return EKeepContinue, it would call
       * ioResignTask and cause a broken loop. Therefore we need to reset
       * this task manually and we don't need to make a error code in this
       * case */
      if (!error && result){ error = result; result = base::error::ENoError; }
      if (error == base::error::EDoAgain) return base::error::EDoAgain;
      if (error == base::error::EKeepContinue) return base::error::EKeepContinue;
      else
        return (error)? pError(error, binder->_message).code: base::error::ENoError;
    } else { /* perform like an acceptor */
      auto& emplacement = _endpoints.at(fd);      
      auto& acceptor    = (current = emplacement.endpoint);
      auto& ioclass     = emplacement.ioclass;

      while (true) {
        socklen_t len    = 0;
        int       socket = 0;

        if ((socket = accept(fd, (struct sockaddr*)&ioclass.internal()._addr, &len)) < 0) {
          /* an acceptor can receive several accepting requests, thus, we need
           * to finish them completly. The error EAGAIN or EWOULDBLOCK indicate
           * that there are no more requests now */
          ioclass.internal()._fd = fd;
          if (errno == EAGAIN || errno == EWOULDBLOCK) break;
          error = base::error::EWatchErrno;
        }
        
        try {
          std::swap(ioclass.internal()._fd, socket);
          
	       #if USE_COROUTING_METHOD
		      /* do corouting at here */
	       #else
          if (!error && std::dynamic_pointer_cast<endpoint::Acceptor>(acceptor)->accepting){
            error = std::dynamic_pointer_cast<endpoint::Acceptor>(acceptor)->accepting(&ioclass.internal(), socket);
          } else ioclass.internal().start(&ioclass.internal(), nullptr);  
 	       #endif
        } catch(base::Error &error){ 
          std::swap(ioclass.internal()._fd, socket);
          ioDeleteTask(_fdesc, socket);
          return error.code;
        }

        if (!error && result){ error = result; result = base::error::ENoError; }
        if (error) return pError(error, "Can't assign a new task");
      }
      return base::error::EDoAgain;
    }
  };

  _out = [this](int fd) -> int {
    auto error = 0;

    result = base::error::ENoError;
    
    if (_endpoints.count(fd) <= 0) {
      /* perform like an binder */
      auto& endpoint = _table.get(fd);
      auto binder    = std::dynamic_pointer_cast<endpoint::Binder>(endpoint);

      /*  By default, this task allways be destroyed by ioserver to reverse 
       * resource. Therefore, we need to resign the task everytime
       */
      current = endpoint;

      if (binder->_out)
        error = binder->_out(&const_cast<IOClass&>(binder->ioclass), fd);
      if (!error && result){ error = result; result = base::error::ENoError; }
      if (error == base::error::EKeepContinue) return error;
      return (error) ? pError(error, "").code : base::error::ENoError;
    }

    try {
      /* - Perform like an connector
       * - It can be perform like a io-corouting, which means each connector
       * can
       * perform simultaleously by separating its job into several pieces.
       */
      auto connector = std::dynamic_pointer_cast<endpoint::Connector>(current = _table.get(fd));

      if (!connector->perform()) return base::error::ENoError;
      /* in direction, connector receive as much as they can */
      if (connector->_in) {
        if ((error = connector->_in(&const_cast<IOClass&>(connector->ioclass), fd)))
        	return error;
        else connector->_in = nullptr;
        if (connector->_out) return base::error::EKeepContinue;
      }

      /* out direction, connector send as much as they can */
      if (connector->_out) {
        if ((error = connector->_out(&const_cast<IOClass&>(connector->ioclass), fd)))
          return error;
        else connector->_out = nullptr;
        if (connector->_in) return base::error::EKeepContinue;
      }

      return base::error::ENoError;
    } catch (base::Error& error) { return error.code; }
  };

  _err = [this](int) -> int { return base::error::ENoError; };
}

IOService::~IOService() {
  IOClassImpl clmain{this};

  clmain.internal().in = [](void* vtable, int fd) -> int {
    return static_cast<IOService*>(vtable)->_in(fd);
  };
  clmain.internal().out = [](void* vtable, int fd) -> int {
    return static_cast<IOService*>(vtable)->_out(fd);
  };
  clmain.internal().err = [](void* vtable, int fd) -> int {
    return static_cast<IOService*>(vtable)->_err(fd);
  };

  ioPerformPoll(&clmain.internal(), _fdesc, _backlog);
  ioReleasePoll(_fdesc);
}
} // server
} // amqt

IOClass* embrace(IOClass* ioclass, struct sockaddr addr){
  
}
