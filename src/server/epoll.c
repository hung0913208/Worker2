/* copyright 2017 by lazycat and it is owned by lazycat */
#if !__linux__
#error "can't support this unknown os system by using the epoll server platform"
#endif

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include <sys/epoll.h>
#include <unistd.h>

#include "libbase/logcat.hpp"
#include "lowlevel/vpoll.hpp"

void ioDeleteTask(int epfd, int fd);

int ioCreatePoll() { return epoll_create1(0); }

void ioReleasePoll(int fd) { close(fd); }

int ioPerformPoll(IOClass* clazz, int epfd, int evsize) {
  struct epoll_event* events = malloc(evsize * sizeof(struct epoll_event));

  if (!events) return EDrainMem;
  if (!clazz || !clazz->in || !clazz->out) {
    free(events);
    return EDoNothing;
  }

  /* a corouting state machine */
  for (int nfds = epoll_wait(epfd, events, evsize, -1) - 1; nfds >= 0; --nfds) {
    int socket = events[nfds].data.fd, error = ENoError;
    do {
      if (events[nfds].events & EPOLLERR) {
        /* on error task, only perform system's fixing tasks */
        if (!clazz->err || clazz->err(clazz->_vtable, socket) != ENoError)
          ioDeleteTask(epfd, socket);
        break;
      }
      if (events[nfds].events & EPOLLIN) {
        /* on revceiving tasks, pull data and perform the tasks */
        error = clazz->in? clazz->in(clazz->_vtable, socket): ENoError;

        if (error != EKeepContinue && error != ENoError && error != EDoAgain)
          ioDeleteTask(epfd, socket);
        else if (error == EKeepContinue)
          ioResignTask(epfd, socket);
        else if (error == ENoError)
          ioReleaseTask(epfd, socket);

        events[nfds].events &= ~EPOLLIN;
      }

      if (events[nfds].events & EPOLLOUT) {
        /* on sending tasks, perform the tasks and push data */
        error = clazz->out? clazz->out(clazz->_vtable, socket): ENoError;

        if (error < 0) ioDeleteTask(epfd, socket);
        if (error != EKeepContinue && error != ENoError)
          ioDeleteTask(epfd, socket);
        else if (error == EKeepContinue)
          ioResignTask(epfd, socket);
        else
          ioReleaseTask(epfd, socket);

        events[nfds].events &= ~EPOLLOUT;
        if (events[nfds].events & EPOLLIN) continue;
      }
    } while (0);
    if (nfds == 0) nfds = epoll_wait(epfd, events, evsize, -1);
  }

  free(events);
  switch (errno) {
    case EBADF:
    case EINVAL:
      return EBadLogic;

    case EFAULT:
      return EBadAccess;

    case EINTR:
      return EInterrupted;

    default:
      return EWatchErrno;
  }
}

int ioAssignTask(int poll, int fd, int type) {
  struct epoll_event event;
  memset(&event, 0, sizeof(struct epoll_event));

  (void)type;

  event.events = EPOLLIN | EPOLLET;
  event.data.fd = fd;

  if (epoll_ctl(poll, EPOLL_CTL_ADD, fd, &event) < 0) {
    close(fd);
    return EBadAccess;
  }
  return ENoError;
}

int ioResignTask(int poll, int fd) {
  struct epoll_event event;

  memset(&event, 0, sizeof(struct epoll_event));
  event.data.fd = fd;
  event.events = EPOLLOUT | EPOLLET;

  if (epoll_ctl(poll, EPOLL_CTL_MOD, fd, &event) < 0) return EBadAccess;
  return ENoError;
}

int ioReleaseTask(int poll, int fd) {
  struct epoll_event event;

  memset(&event, 0, sizeof(struct epoll_event));
  event.data.fd = fd;
  event.events = EPOLLIN | EPOLLET;

  if (epoll_ctl(poll, EPOLL_CTL_MOD, fd, &event) < 0)
    return EBadAccess;
  else
    return ENoError;
}

/* ------------------------------------------------------------------------ */
void ioDeleteTask(int epfd, int fd) {
  struct epoll_event event;

  event.data.fd = fd;
  event.events = EPOLLIN | EPOLLET;

  epoll_ctl(epfd, EPOLL_CTL_DEL, fd, &event);
  close(fd);
}