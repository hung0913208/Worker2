#include "libio.hpp"

#include <netinet/tcp.h>
#include <fcntl.h>

namespace amqt {
namespace server {
namespace endpoint {
Binder::Binder(Acceptor& acceptor, IOClassImpl& ioclass)
      : Connector{std::forward<IOClassImpl>(ioclass)}, _acceptor{acceptor} {}

Binder::~Binder() {}

bool Binder::reuseAddress(bool yes) {
  if (setsockopt(_ioclass.internal()._fd, SOL_SOCKET, SO_REUSEADDR, (const void *) &yes, sizeof(int)))
    return false;
  else return true;
}

void Binder::mount(int fd, sockaddr_in addr){
  _ioclass.internal()._addr = addr;
  _ioclass.internal()._fd   = fd;
  _ioclass.internal().start = [](IOClass* ioclass, int (*perform)(struct IOClass*, int)){
    auto poll = static_cast<IOService*>(ioclass->_vtable);
    auto binder = std::dynamic_pointer_cast<Binder>(poll->current);

    /* this callback will indicate a function which will be peform on server serveral times */
    if (perform) 
      poll->result = base::error::EKeepContinue;
    binder->_out = perform;
  };
  
  _ioclass.internal().async = [](IOClass* ioclass, int (*perform)(struct IOClass*, int)) -> bool {
   #if !USE_COROUTING_METHOD
    auto poll = static_cast<IOService*>(ioclass->_vtable);
    auto binder = std::dynamic_pointer_cast<Binder>(poll->current);

    if (binder->_in && perform == *binder->_out.target<int(*)(struct IOClass*, int)>())
      poll->result = base::error::EDoNothing;
    else {
      poll->result = base::error::EKeepContinue;
      binder->_out = perform;
    }
    return false;
   #endif
  };
}

bool Binder::nonblock(bool yes) {
  int flags = 0;

  if ((flags = fcntl (_ioclass.internal()._fd, F_GETFL, 0)) == -1)
    return false;
  else if(yes){
    if (fcntl (_ioclass.internal()._fd, F_SETFL, flags | O_NONBLOCK) == -1)
      return false;
  } else if (fcntl (_ioclass.internal()._fd, F_SETFL, flags & ~O_NONBLOCK) == -1)
    return false;
  return true;
}

bool Binder::nodelay(bool yes) {    
  if (setsockopt(_ioclass.internal()._fd, IPPROTO_TCP, TCP_NODELAY, (const void *) &yes, sizeof(int)))
    return false;
  else return true;

}

bool Binder::perform() { return true; }
}
}
}