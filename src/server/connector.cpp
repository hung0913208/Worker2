#include "libbase/logcat.hpp"
#include "libio.hpp"

namespace amqt {
namespace server{
namespace endpoint{
Connector::Connector(struct sockaddr& addr): 
  Endpoint{}, ioclass{_ioclass.internal()}{
    _addr = addr;
    _read = true;
  }

Connector::Connector(IOClassImpl&& ioclass): 
  Endpoint{std::forward<IOClassImpl>(ioclass)}, ioclass{_ioclass.internal()}{
    _read = true;
  }

Connector::~Connector(){}

IOClassImpl& Connector::mount(IOService* serivce){
  auto poll = static_cast<IOService*>(_ioclass.internal()._vtable);
  auto fd   = socket(AF_INET, SOCK_STREAM, 0);

  if (fd >= 0 && connect(fd, &_addr, sizeof(_addr)) < 0)
    ioDeleteTask(poll->fpoll(), fd);  
  else{
  	_ioclass.internal()._vtable = serivce;
  	_ioclass.internal()._fd = fd;
  }

  _ioclass.internal().start = [](IOClass* ioclass, int (*perform)(IOClass*, int)){

  };
  _ioclass.internal().async = [](IOClass* ioclass, int (*perform)(IOClass*, int)) -> bool{
  	#if !USE_COROUTING_METHOD
  	  return false;
  	#endif
  };
  return _ioclass;
}
} // endpoint 
} // server
} // amqt