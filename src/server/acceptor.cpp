#include "libio.hpp"

#include <arpa/inet.h>
#include <fcntl.h>

namespace amqt {
namespace server{
namespace endpoint{
Acceptor::Acceptor(int port, std::size_t backlog): 
    Endpoint{}, _backlog{backlog}{
  _addr.sin_family = AF_INET;
  _addr.sin_addr.s_addr = INADDR_ANY;
  _addr.sin_port = htons(port);
}

Acceptor::~Acceptor(){}

IOClassImpl& Acceptor::mount(IOService* service){
  auto fd = socket(AF_INET, SOCK_STREAM, 0);
  auto st_addr = reinterpret_cast<struct sockaddr*>(&_addr);
  auto poll = static_cast<IOService*>(_ioclass.internal()._vtable);

  _ioclass.internal().start = [](IOClass*, int (*)(IOClass*, int)) {
    throw BadAccess;
  };
  _ioclass.internal().async = [](IOClass*, int (*)(IOClass*, int)) -> bool{ 
    throw BadAccess;
  };

  if (fd >= 0) _ioclass.internal()._fd = fd;
  else throw WatchErrno;

  if (__config_()) ioDeleteTask(poll->fpoll(), fd);
  else if (bind(fd, st_addr, sizeof(_addr)) < 0)
    ioDeleteTask(poll->fpoll(), fd);
  else if(listen(fd, _backlog) < 0)
    ioDeleteTask(poll->fpoll(), fd);
  else {    
    _ioclass.internal()._vtable = service;
    _ioclass.internal()._fd = fd;
    _ioclass.internal().start = [](IOClass* ioclass, int (*in)(IOClass*, int)) {
      try {
        auto poll     = static_cast<IOService*>(ioclass->_vtable);
        auto acceptor = std::dynamic_pointer_cast<Acceptor>(poll->current);
        auto error    = 0;

        /* IOClass allways point to an acceptor. Therefore, the client's socket and 
         * client's address have been save temporarely in acceptor IOClass for this 
         * current performing */

        /* Because it is a new task, assigning is an only method we can do right-now */
        if (!(error = ioAssignTask(poll->fpoll(), ioclass->_fd, endpoint::TBinder))){
          auto binder = acceptor->accept_(ioclass->_addr);

          binder->mount(ioclass->_fd, ioclass->_addr);
          binder->_in = in;
                    
          poll->sign(std::forward<int>(ioclass->_fd), binder);
        } else ioDeleteTask(poll->fpoll(), ioclass->_fd);

       #if USE_COROUTING_METHOD
        /* this part is only activated if we use corouting */
       #endif
      } catch (base::Error &error) { throw base::Error{error}; }
    };

    _ioclass.internal().async = [](IOClass* ioclass, int (*async)(IOClass*, int)) -> bool{
     #if !USE_COROUTING_METHOD
      auto poll     = static_cast<IOService*>(ioclass->_vtable);
      auto acceptor = std::dynamic_pointer_cast<Acceptor>(poll->current);

      if (acceptor) acceptor->accepting = async;
      return false;
     #endif
    };
  }
  return _ioclass;
}

Acceptor::PBinder Acceptor::accept_(const struct sockaddr_in& addr){
  /* build a new binder and add basic information of the binder (socket and address) */

  try {
    auto inhr_buffer = __restrict_(addr);
    
    if (inhr_buffer){
      IOClassImpl ioclass{_ioclass.internal()._vtable, *inhr_buffer};
      
      return std::make_shared<endpoint::Binder>(*this, ioclass);
    } else {
      IOBuffer std_buffer{4096};
      IOClassImpl ioclass{_ioclass.internal()._vtable, std_buffer};

      return std::make_shared<endpoint::Binder>(*this, ioclass);
    }
  } catch(base::Error& error){ return nullptr; }  
}

IOBuffer* Acceptor::__restrict_(const struct sockaddr_in&){ return nullptr; }

bool Acceptor::reusePort(bool yes){
  auto flags = 0;

  if ((flags = fcntl (_ioclass.internal()._fd, F_GETFL, 0)) == -1)
    return false;    
  else if (setsockopt (_ioclass.internal()._fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&yes, sizeof(yes)) < 0)
    return false;
  return true;
}

bool Acceptor::reuseAddress(bool yes){
  auto flags = 0;

  if ((flags = fcntl (_ioclass.internal()._fd, F_GETFL, 0)) == -1)
    return false;
  else if (setsockopt (_ioclass.internal()._fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes)) < 0)
    return false;
  return true;
}

bool Acceptor::nonblock(bool yes){
  auto flags = 0;

  if ((flags = fcntl (_ioclass.internal()._fd, F_GETFL, 0)) == -1)
    return false;
  if (yes) {
    if (fcntl (_ioclass.internal()._fd, F_SETFL, flags | O_NONBLOCK) == -1)
      return false;
  } else {
    if (fcntl (_ioclass.internal()._fd, F_SETFL, flags & ~O_NONBLOCK) == -1)
      return false;
  }
  return true;
}

bool Acceptor::__config_(){
  auto ret = nonblock(true);
  ret = ret? reuseAddress(true): ret;
  ret = ret? reusePort(true): ret;
  return ret;
}
} // endpoint
} // server
} //amqt