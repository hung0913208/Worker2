from worker import Worker 
from tracer import Statistic
from requests import post
from json import dumps
from sys import argv

class DemoTracer(Statistic):
    """docstring for DemoTracer"""
    def __init__(self, name):
        super(DemoTracer, self).__init__(name, 5)
        self.buffer = []
    
    def end(self, record, idd, spended):
        spended = 1000000*spended
        record[1] = record[1] + 1 if not record[1] is None else 1               # total request
        record[2] = record[2] + spended if not record[2] is None else spended   # total tmp proc
        record[3] = spended                                                     # last tmp proc
        record[4] = (1000000.0*record[1])/record[2]                             # proc rate
        record[5] = 0                                                           # request rate
        if idd >= len(self.buffer):
            self.buffer.append([record[0], str(record[1]), str(0), str(record[2]), str(record[3]), str(record[4]), str(record[5])])
        else:
            self.buffer[idd][1] = str(record[1])
            self.buffer[idd][3] = str(record[2])
            self.buffer[idd][4] = str(record[3])
            self.buffer[idd][5] = str(record[4])
        self.update()

        return record

    def update(self):
        # name, total request, pending request, total tmp proc, last tmp proc, proc rate, request rate
        try:
            r = post('http://118.102.6.118:8083/worker', data=dumps(self.buffer))
            if r.status_code != 200:
                print('[x] Tracing Broken')
        except Exception as error:
            #print(error)
            pass

if __name__ == '__main__':

        # huong dan su dung cau hinh voi worker co ban
        # ffqueues: 2 queue cho worker biet nhan du lieu o dau va update len dau(member 1: queue request, member 2: queue response)
        # tracking: cac ham hoac class cu the de track neu bien flag digging == false, mac dinh khong tracking
        # max_workers: neu duoc set thi se quy dinh so luong thread toi da ma threadpool co the ho tro
        # server: server cua rabbitmq
        # port: port cua rabbitmq phuc vu viec lang nghe va tra loi cac message, mac dinh khong thiet dat la port mac dinh cua rabbitmq
        # qualities: cac qualities cho phep ho tro hien tai
        # account: account dung de dang nhap (member1: user name, member2: password), mac dinh neu khong thiet dat la ('guest', 'guest')
        # ffmpeg: duong dan cua ffmpeg, mac dinh neu khong thiet dat la /usr/bin/ffmpeg
        # ffprobe: duong dan cua ffprobe, mac dinh neu khong thiet dat la /usr/bin/ffprobe
        # interface: neu server da cai goi netifaces thi co the set flag nay len de he thong tu tim dia chi ip cua may, nguoc lai phai gan http_server
        # debug: in ra man hinh cac thong tin qua trinh hoat dong
        # http_server: dia chi ip noi worker dung de xuat ket qua sau khi convert xong
        # http_port: port cua http server cho cac may khac truy cap vao va lay file media da duoc convert
        # statistic: class dung de tong hop va up thong tin trong qua trinh tracking cac function
        # logpath: duong dan noi ghi log, neu khong co thi mac dinh se ghi ra man hinh neu ban che do debug
        # digging: flag dung de ghi thong tin tat ca cac ham, khi bat flag nay len thi trace se khong quan tam toi tracking 
        # render: neu da cai goi pycallgraph thi co the dung bien nay de cau hinh duong dan xuat ra chi tiet profiler cua worker duoi dang hinh anh cu the
    parameters = {
        'ffqueues': ['WorkerQueueTest', 'MetaQueueTest'],
        'tracking': ['worker::calibrate', 'worker::answer', 'converter::Converter.*'],
        'server': '118.102.6.118',#'10.199.220.43',
        'port': 9233,
        'qualities': [240, 360, 480],
        'no_thread': False,
        'max_workers': 200, 
        #'account': ('test', 'test'),
        'storage': '/zserver/download', #'/home/lazycat/Downloads', #'/zserver/download',
        #'interface': 'eno1',
        'debug': True,
        'alias': '/zserver/download',
        'http_server': '118.102.6.118',
        'http_port': 8081,
        'digging': False,
        'broadcasting': False,
        'statistic': DemoTracer("worker01"),
        #'logpath': '/zserver/worker/logs',
        #'render': {'output_file': 'test.png', 'output_type': 'png'}
    }
    Worker("worker01", **parameters).start()
