from sys import settrace
from time import time

from pycallgraph import PyCallGraph
from pycallgraph.config import Config
from pycallgraph.output import GraphvizOutput

from inspect import getargvalues, getmodule


class Statistic(object):
    """docstring for Statistic"""
    def __init__(self, name, range=0):
        super(Statistic, self).__init__()
        self.permanent = {}
        self.report = []
        self.range = range
        self.name = name

    def done(self):
        pass

    def begin(self, name, begin):
        pass

    def end(self, record, idd, spended):
        return record

    def __merge__(self, name, spended):
        if name not in self.permanent:
            idd = len(self.report)
            self.permanent[name] = {'id': idd}
            if self.range > 0:
                record = [None] * (self.range + 1)
                record[0] = name
            else: record = [name]
            self.report.append(record)
        else: idd = self.permanent[name]['id']

        temp = self.end(self.report[idd], idd, spended)
        
        if isinstance(temp, list):
            self.report[idd] = temp

    def reset(self):
        pass


class Tracer(object):
    def __init__(self, statistic, observers=None, digging=False, render=None, module='__main__'):
        super(Tracer, self).__init__()
        self.statistic = statistic
        self.observers = observers
        self.module = module
        self.digging = digging
        self.queue = []
        
        if not render is None:
            self.render = PyCallGraph(output=GraphvizOutput(**render))
        else: self.render = None

    def __tracer__(self, frame, event, arg):
        clazz = Tracer.__dump_class_instance__(frame)
        obj = clazz.__class__.__module__ if clazz else None
        name = self.dump(frame, self.digging)

        if self.statistic and name:
            if name and event is 'return':
                self.statistic.__merge__(name, time() - self.queue.pop(-1))
            elif name and event is 'call':
                begin = time()
                self.queue.append(begin)
                self.statistic.begin(name, begin)

        if self.render and clazz and clazz.__name__ != 'Tracer':
            self.render.tracer.tracer(frame, event, arg)
        return self.__tracer__

    def __enter__(self):
        self.start()

    def __exit__(self, type, value, traceback):
        self.done()
    
    def start(self):
        if self.render:
            self.render.start()
        settrace(self.__tracer__)

    def stop(self):
        if self.render:
            self.render.stop()
        if self.statistic:
            self.statistic.reset()
        settrace(None)

    def done(self):
        if self.render:
            self.render.done()
        self.stop()
        self.generate()

    def generate(self):
        self.stack = []

    def dump(self, frame, digging=False):
        module = Tracer.__dump_module_instance__(frame)
        clazz = Tracer.__dump_class_instance__(frame)
        function = frame.f_code.co_name

        clazz_name = clazz.__name__ if clazz else None
        module_name = module.__name__ if module and module.__name__ != self.module else None

        if module and module.__name__ != self.module:
            if clazz:
                result = '%s::%s.%s' % (module.__name__, clazz.__name__, function)
            else: result = '%s::%s' % (module.__name__, function)
        elif clazz:
            result = '%s.%s' % (clazz.__name__, function)
        else: result = '%s' % function
        

        if digging is False and self.observers and result not in self.observers:
            clazz_pattern = None
            module_pattern = None
            if module_name and clazz_name:
                clazz_pattern = '%s::%s.*' % (module_name, clazz_name)
                module_pattern = '%s::*' % module_name
            elif module_name:
                module_pattern = '%s::*' % module_name
                clazz_pattern = None
            elif clazz_name:
                clazz_pattern = '%s.*' % clazz_name
                module_pattern = None
            else:
                module_pattern = None
                clazz_pattern = None

            if module_pattern and module_pattern in self.observers:
                return result
            elif clazz_pattern and clazz_pattern in self.observers:
                return result            
            else:
                if module_pattern:
                    for observer in self.observers:
                        if module_pattern[0:-3].startswith(observer[0:-2]):
                            return result
                return None
        return result

    @staticmethod
    def __dump_module_instance__(frame):
        return getmodule(frame.f_code)

    @staticmethod
    def __dump_class_instance__(frame):
        args, _, _, value_dict = getargvalues(frame)
        if len(args) and args[0] == 'self':
            return getattr(value_dict.get('self', None), '__class__')
        return None

    @staticmethod
    def __dump_value_dict_instance__(frame):
        args, _, _, value_dict = getargvalues(frame)
        return args, value_dict
