#!/usr/bin/python3
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor
from types import MethodType

from functools import wraps
from threading import stack_size
from time import time

def pool(funct):
    class Pool(object):
        def __init__(self):
            super(Pool, self).__init__()
            self.real_pool = ThreadPoolExecutor()
            self.max_workers = None
            self.direct = True
            self.trace = None
            self.database = {}

        def boot(self, function, args, kwargs):
            restrict = self.get(function, 'restrict')
            if self.direct: return self.run(function, args, kwargs)
            try:
                if restrict is None or not restrict():
                    return self.real_pool.submit(self.run, function, args, kwargs)
            except Exception:
                if isinstance(restrict, bool) and restrict:
                    return self.get(function, 'pool').submit(self.run, function, args, kwargs)

            return self.run(function, args, kwargs)

        def run(self, function, args, kwargs):
            start = time()
            result = function(*args, **kwargs)
            end = time()
            
            return result

        @staticmethod
        def __get_funct_name__(function):
            return '.'.join((function.__module__, function.__qualname__))

        def register(self, function, session):
            self.append(function)

        def append(self, function):
            fname = Pool.__get_funct_name__(function)
            if not fname in self.database:
                self.database[fname] = {
                    'restrict': None, 
                    'pool': ThreadPoolExecutor(self.max_workers)}

        def thread(self, function, value):
            fname = Pool.__get_funct_name__(function)
            self.database[fname]['restrict'] = value

        def get(self, function, key):
            fname = Pool.__get_funct_name__(function)
            if key in self.database[fname].keys():
                return self.database[fname][key]
            else:
                return None

    if '__single_ton__' not in pool.__dict__:
        pool.__single_ton__ = Pool()

    class Session(object):
        """docstring for Session"""
        def __init__(self, own, function):
            super(Session, self).__init__()
            self.__own__ = own 

            wraps(function)(self)
            self.__own__.register(function, self)            

        def __call__(self, *args, **kwargs):
            return self.__own__.boot(self.__wrapped__, args, kwargs)

        def __get__(self, instance, cls):
            if instance is None:
                return self
            else:
                return MethodType(self, instance)
    return Session(pool.__single_ton__, funct)

def fracture(funct, condition):
    if '__single_ton__' not in pool.__dict__:
        raise AssertionError('pool need to be called first')
    pool.__single_ton__.thread(funct, condition)


def get(func, key):
    if '__single_ton__' not in pool.__dict__:
        raise AssertionError('pool need to be called first')
    return pool.__single_ton__.get(func, key)

def setparam(key, value):
    if '__single_ton__' not in pool.__dict__:
        raise AssertionError('pool need to be called first')
    setattr(pool.__single_ton__, key, value)
