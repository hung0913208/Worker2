from json import dumps
from pika import PlainCredentials, BlockingConnection, ConnectionParameters
from pika import BasicProperties
from pika.connection import Parameters

from tracer import Tracer
from converter import Converter
from pool import pool, fracture, setparam

try:
    from netifaces import interfaces, ifaddresses, AF_INET

    def address(interface=None):
        if interface is None:
            return None
        for ifaceName in interfaces():
            addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(
                AF_INET, [{'addr': 'No IP addr'}])]
            if interface is None and addresses not in ('127.0.0.1', '0.0.0.0'):
                return addresses[0]
            elif interface == ifaceName:
                return addresses[0]
        return None
except ImportError:
    def address(interface=None):
        return None


class Worker(object):
    """docstring for Worker"""
    def __init__(self, name, **kwargs):
        super(Worker, self).__init__()
        digging = kwargs.get('digging', False)
        tracking = kwargs.get('tracking', None)
        render = kwargs.get('render', None)
        ffqueues = kwargs.get('ffqueues', ['WorkerQueue', 'MetaQueue'])
        server = kwargs.get('server', '127.0.0.1')
        port = kwargs.get('port', Parameters.DEFAULT_PORT)
        account = kwargs.get('account', ('guest', 'guest'))        
        http_port = kwargs.get('http_port', 8080)
        statistic = kwargs.get('statistic', None)
        interface = kwargs.get('interface', None)

        self.response = {}
        self.name = name

        try:
            if 'max_workers' in kwargs:
                setparam('max_workers', int(kwargs['max_workers']))
            if 'no_thread' in kwargs:
                setparam('direct', int(kwargs['no_thread']))
        except AssertionError:
            pass

        if 'http_server' in kwargs:
            alias = ''.join(('http://', kwargs['http_server'], ':', str(http_port)))
            del kwargs['http_server']
        else:
            alias = None
        
        if 'alias' in kwargs:
            alias = alias if alias else 'http://localhost:' + str(http_port)
            alias = ''.join((alias, kwargs['alias']))
            del kwargs['alias']
        if not 'store' in kwargs:
            kwargs['store'] = None
            
        accounts = PlainCredentials(account[0], account[1])
        params = ConnectionParameters(host=server, port=port, credentials=accounts)
        self.converter = Converter(name=name, **kwargs)
        self.channel = BlockingConnection(params).channel()

        try:
            if self.converter.callback() is None:
                fracture(Converter.convert, False)
        except AssertionError:
            pass
        
        @pool
        def stack_answer(width, response):
            if self.response != None:
                if isinstance(width, list):
                    for w in width: Worker.calibrate(kwargs['store'], alias, http_port, interface, response, str(w))
                else:
                    Worker.calibrate(kwargs['store'], alias, http_port, interface, response, width)
                self.channel.basic_publish(
                    exchange='', routing_key=ffqueues[1],
                    body=dumps(response).encode('utf8'),
                    properties=BasicProperties(delivery_mode=2))

        def callback(ch, method, _, request):            
            if self.probe(request) == 'trace':
                pass
            elif tracking is None and digging is False and render is None:
                print(' [x] Received 1 request')
                vstreams, jsrequest, mtracking = self.converter.prepare(request)
                root, response = self.converter.build(vstreams, jsrequest, mtracking)

                if not jsrequest is None:
                    Worker.calibrate(kwargs['store'], alias, http_port, interface, response, 'thumbnail')
                    Worker.quick_answer(self.channel, ffqueues[1], kwargs, jsrequest['source'], response)

                    response = self.converter.convert(vstreams, jsrequest, root, response)
                    if self.converter.callback() is None:
                        Worker.calibrate(kwargs['store'], alias, http_port, interface, response)
                        Worker.full_answer(self.channel, ffqueues[1], kwargs, response)
                    if response: print(" [x] Done")
            else:
                with Tracer(statistic, tracking, digging=digging, render=render):
                    vstreams, jsrequest, mtracking = self.converter.prepare(request)
                    root, response = self.converter.build(vstreams, jsrequest, mtracking)

                    if not jsrequest is None:
                        Worker.calibrate(kwargs['store'], alias, http_port, interface, response, 'thumbnail')
                        Worker.quick_answer(self.channel, ffqueues[1], kwargs, jsrequest['source'], response)

                        response = self.converter.convert(vstreams, jsrequest, root, response)
                        if self.converter.callback() is None:
                            Worker.calibrate(kwargs['store'], alias, http_port, interface, response)
                            Worker.full_answer(self.channel, ffqueues[1], kwargs, response)
            ch.basic_ack(delivery_tag=method.delivery_tag)

        self.converter.callback(stack_answer)
        self.build(ffqueues, callback)
    
    def start(self):
        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

    def build(self, ffqueues, callback):
        self.channel.queue_declare(queue=ffqueues[0], durable=True)
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(callback, queue=ffqueues[0])

    def probe(self, message):
        return 'convert'

    @staticmethod
    def calibrate(store, alias, port, interface, response, fix=None):
        if store or response is None: pass
        elif alias:
            if fix in response['uri'] and fix != 'source':
                    value = response['uri'][fix]
                    response['uri'][fix] = ''.join((alias, '/', value))
            else:
                for key, value in response['uri'].items():
                    if key != 'source':
                        response['uri'][key] = ''.join((alias, '/', value))
                if response['thumbnail_uri'].startswith(alias) == False:
                    response['thumbnail_uri'] = ''.join((alias, '/', response['thumbnail_uri']))
        else:
            host = address(interface)
            path = ''.join(('http://', host, ':', str(port))) if host else ''
            fixed = ''.join((path, '/'))

            if fix in response['uri'] and fix != 'source':
                response['uri'][fix] = ''.join((fixed, response['uri'][fix]))
            else:
                for key, value in response['uri'].items():
                    if key != 'source':
                        response['uri'][key] = ''.join((fixed, value))
                if response['thumbnail_uri'].startswith(fixed) == False:
                    response['thumbnail_uri'] = ''.join((fixed, response['thumbnail_uri']))

    @staticmethod
    @pool
    def full_answer(channel, queue, kwargs, response):
        if not response is None:
            channel.basic_publish(
                exchange='', routing_key=queue,
                body=dumps(response).encode('utf8'),
                properties=BasicProperties(delivery_mode=2))

    @staticmethod
    @pool
    def quick_answer(channel, queue, kwargs, source, response):
        if not response is None:
            response['uri']['source'] = source
            channel.basic_publish(
                exchange='', routing_key=queue,
                body=dumps(response).encode('utf8'),
                properties=BasicProperties(delivery_mode=2))
