

resolutions = {
    '1080p': {'16:9': ['1920', '1080'], '4:3': ['1440', '1080'], '3:2':['1620', '1080']},
    '720p': {'16:9': ['1280', '720'], '4:3': ['960', '768'], '3:2': ['1080', '270']}, 
    '480p': {'16:9': ['858', '480']},
    '360p': {'16:9': ['480', '360']}, 
    '240p': {'16:9': ['352', '240']}    
}

def resolution(index, ration='16:9'):
    if index in resolutions.keys() and ration in resolutions[index]:
        return resolutions[index][ration]
