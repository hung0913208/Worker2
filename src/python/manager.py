import  BaseHTTPServer
import SocketServer

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self):
        pass

port = 8080
httpd = SocketServer.TCPServer(("", port), Handler())

httpd.serve_forever()