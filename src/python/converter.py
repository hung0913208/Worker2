#!/usr/bin/python3
from time import time, gmtime, strftime
from os import stat
from json import dumps, loads, decoder
from os.path import split, splitext
from subprocess import Popen, PIPE
from resolutions import resolution

from pool import pool, get

import logging
import swig_worker

class Converter(object):
    """docstring for Converter
    __init__
    Args:
        qualities: the qualities that has been apdated
    """

    def __init__(self, qualities, name=None, logpath=None, **kwargs):
        super(Converter, self).__init__()
        if name and logpath:
            hdlr = logging.FileHandler('%s/%s.txt' % (logpath, name))
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)

            self.logger = logging.getLogger(name)
            self.logger.addHandler(hdlr)
            self.logger.setLevel(logging.WARNING)
        else: self.logger = None
        self.qualities = qualities
        self.extend = kwargs
        self.response = None

        self.debug = kwargs.get('debug', False)
        self.tracer = kwargs.get('tracer', None)
        self.ffmpeg = kwargs.get('ffmpeg', '/usr/bin/ffmpeg')
        self.ffprobe = kwargs.get('ffprobe', '/usr/bin/ffprobe')
        self.store = kwargs.get('storage', None)
        self.broadcasting = kwargs.get('broadcasting', False)

    def callback(self, callback=None):
        if not callback is None:
            self.response = callback
        return self.response

    def prepare(self, request):
        jsrequest = self.__parse_request__(request)
        # kiem tra va load qualities, source, id

        if self.__is_okey__('__get_input__', jsrequest=jsrequest) is False:
            return None, None, None
        else:
            qualities, src, _ = self.__get_input__(jsrequest)

        # kiem tra va thuc hien tracking
        if self.__is_okey__('__track__', qualities=qualities) is False:
            return None, None, None
        else: tracking = self.__track__(src)

        # kiem tra va thuc hien load cac thanh phan co ban cua video stream
        if 'vstreams' in jsrequest:
            vstreams = jsrequest['vstreams']
        elif self.__is_okey__('__select_vstream__', tracking=tracking) is False:
            return None, None, None
        else: vstreams = [stream for stream in tracking['streams'] if stream['codec_type'] == 'video']

        jsrequest['convert'] = Converter.__get_output_format__(jsrequest)
        if jsrequest['convert']['acodec'] == self.__get_audio_codec__(tracking):
            jsrequest['convert']['acodec'] = 'copy'
        return vstreams, jsrequest, tracking

    def build(self, vstreams, jsrequest, tracking):
        if jsrequest and vstreams and tracking:
            oformat = Converter.__get_output_format__(jsrequest)
            src = jsrequest['source']
            idd = jsrequest['video_id']

            if self.__is_okey__('__select_vstream__', vstreams=vstreams) is False:
                return None
            else:
                src_file = split(src)[-1]
                self.__logcat__(logging.DEBUG, "[-] [idd: %s, src: %s] -> [format: %s, vcodec: %s acodec: %s]"
                                % (idd, src_file, oformat['ext'][1:], oformat['vcodec'], oformat['acodec']))
                vstream, timebase = Converter.__select_vstream__(vstreams)
                othumbnail = self.__get_othumbnail__(jsrequest, vstream, timebase)
                root, response = self.__build_new_resp__(jsrequest, tracking, oformat)
                response['thumbnail_uri'] = self.__build_thumnail__(src, src_file, root, othumbnail)
            return root, response
        return None, None

    @staticmethod
    def adapt(qualities, vstreams):
        return (approv for approv in qualities if approv < vstreams[0]['width'])

    @pool
    def convert(self, vstreams, jsrequest, root, response):
        qualities, src, idd = self.__get_input__(jsrequest)
        widths = filter(lambda approv: approv < vstreams[0]['width'], qualities)
        oformat = Converter.__get_output_format__(jsrequest)        
        ofile_prefix = splitext(split(src)[-1])[0]
        omovies = []

        # mac dinh dung video stream dau tien de truyen di
        for wdth in widths:
            dest = ''.join((ofile_prefix, str(wdth), oformat['ext']))
            omovie = ''.join((root, '/', dest))

            self.__build_cmd_conv__(src, oformat, resolution(str(wdth) + 'p'), omovie)

            response['uri'][str(wdth)] = dest
            omovies.append(omovie)

        if self.response:
            Converter.__convert__(self.logger, self.extend, src, widths, omovies, response, self.response)

            if self.broadcasting:
                self.build_broadcasting(vstreams, jsrequest, root, response)

            self.__logcat__(logging.DEBUG, "[o] [Done] [idd %s]" % idd)
            return response
        else: return None

    @pool
    def build_broadcasting(self, vstreams, jsrequest, root, response):
        qualities, src, _ = self.__get_input__(jsrequest)
        max_width = vstreams[0]['width']
        widths = filter(lambda approv: approv < max_width, qualities)
        ofile_prefix = splitext(split(src)[-1])[0]
        oformat = Converter.__get_output_format__(jsrequest)

        # mac dinh dung video stream dau tien de truyen di
        for wdth in widths:
            res = resolution(str(wdth) + 'p')
            dest = ''.join((ofile_prefix, str(wdth)))
            omovie = ''.join((root, '/', dest + oformat['ext']))
            ohls = ''.join((root, '/', dest))

            if self.__broadcast__(omovie, ohls):
                response['uri']['hls_' + str(wdth)] = dest + '.m3u8'
                if self.response:                    
                    self.response('hls_' + str(wdth), response)

    @pool
    def __broadcast__(self, source, output):
        if 'debug' in self.extend and self.extend['debug']: 
            start = time()
        try:
            command = [self.ffmpeg, '-y', '-v', 'error', '-hide_banner', '-i', source, '-codec:', 
                       'copy', '-start_number', '0', '-hls_time', '10', 
                       '-hls_list_size', '0', '-f', 'hls', output + '.m3u8']
            _, error = Popen(command, stdout=PIPE, stderr=PIPE).communicate()
            if 'debug' in self.extend and self.extend['debug']: 
                self.__logcat__(logging.INFO, '[o] [%s -> %sp(hls) spent %fs' % 
                            (split(source)[-1], output + '.m3u8', time() - start))
            return True
        except OSError as error:
            self.__logcat__(logging.ERROR, '[x] [%s catch an error: %s]' % (self.ffmpeg, str(error)))
        return False

    def __parse_request__(self, request):
        try:
            if isinstance(request, bytes) or isinstance(request, str):
                return loads(request.decode('utf8') if isinstance(request, bytes) else request)
            else:
                self.__logcat__(logging.ERROR, '[x] [Syntax error]')
                return None
        except decoder.JSONDecodeError:
            return None
        except Exception as err:
            raise err

    def __get_input__(self, jsrequest):
        qualities = jsrequest.get('qualities', self.qualities)
        src = jsrequest['source']
        idd = jsrequest['video_id']
        
        if swig_worker.rbitInitConsumer(src):
            return None, None, 0
        return qualities, src, idd

    def __get_video_codec__(self, tracking):
        vstreams = [stream for stream in tracking['streams'] if stream['codec_type'] == 'video']
        if len(vstreams) == 0:
            return None
        else: return vstreams[0]['codec_name']

    def __get_audio_codec__(self, tracking):
        astreams = [stream for stream in tracking['streams'] if stream['codec_type'] == 'audio']
        if len(astreams) == 0:
            return None
        else: return astreams[0]['codec_name']

    @staticmethod
    def __get_output_format__(jsrequest):
        if 'convert' in jsrequest:
            oformat = jsrequest['convert']
            if 'ext' not in oformat:
                return None
        else:
            oformat = {'ext': '.mp4', 'vcodec': 'libx264', 'acodec': 'copy'}
        return oformat

    @staticmethod
    def __select_vstream__(vstreams):
        vstream = vstreams[0]
        timebase = vstream['time_base']
        real_pos = vstream['time_base'].index('/') + 1
        timebase = float(timebase[real_pos: len(timebase)])
        return vstream, timebase

    def __get_othumbnail__(self, jsrequest, vstream, timebase):
        def_intervene = vstream['duration_ts']/2.0 if vstream['duration_ts'] > 0.2 else 0.1

        if 'thumnail' in jsrequest:
            othumbnail = jsrequest['thumnail']

            if not 'position' in self.extend:
                if 'portion' in self.extend:
                    othumbnail['position'] = vstream['duration_ts']/self.extend['portion']
                else:
                    othumbnail['position'] = vstream['duration_ts']/2.0 if vstream['duration_ts'] > 0.2 else 0.1
        else:
            othumbnail = {'position': 0.0}
        return othumbnail

    def __build_new_resp__(self, jsrequest, tracking, oformat):
        root = self.store if self.store else '/tmp'
        response = {
            'video_id': jsrequest['video_id'],
            'video_length': tracking['format']['duration'],
            'video_format': oformat['ext'][1:],
            'user_id': jsrequest.get('user_id', 1),
            'uri': {}
        }
        return root, response

    def __build_thumnail__(self, src, src_file, root, config):
        othumbnail = ''.join((root, '/', splitext(src_file)[0], '.jpeg'))
        if swig_worker.rbitPrintThumbnail(src, othumbnail, config['position']):
            self.__logcat__(logging.ERROR, '[x] [Can\'t produce a new thumbnail because of serveral unknowning reasons]')
            return ''
        else:
            return ''.join((splitext(src_file)[0], '.jpeg'))

    def __is_okey__(self, name, **kwargs):
        if name == '__get_input__':
            if not ('video_id' in kwargs['jsrequest'] and 'source' in kwargs['jsrequest']):
                self.__logcat__(logging.ERROR, '[x] [Syntax error, missing video_id or source]')
                return False
        elif name == '__track__':
            if not isinstance(kwargs['qualities'], list):
                self.__logcat__(logging.ERROR, '[x] [Syntax error, qualities would be prefered to be a list]')
                return False

        if name == '__select_vstream__' and 'tracking' in kwargs:
            tracking = kwargs['tracking']
            # thong bao rang video khong ho tro dinh dang
            if tracking is None or 'format' not in tracking or 'streams' not in tracking:
                self.__logcat__(logging.ERROR, '[x] [Corrupted source or unsupported formation]')
                return False

        if name == '__select_vstream__' and 'vstreams' in kwargs:
            if kwargs['vstreams'] is None:
                self.__logcat__(logging.WARN, '[x] [Can\'t found any video stream or this is an audio source]')
                return False
        return True

    def __track__(self, source, fprint='json'):
        return loads(swig_worker.rbitDoTracking(source, 0))

    def __build_cmd_conv__(self, imovie, formation, res, omovie):
        if swig_worker.rbitSetOutput(imovie, omovie):
            return False
        elif swig_worker.rbitSetEncode(imovie, omovie, formation['acodec'], swig_worker.AUDIO):
            return False
        elif swig_worker.rbitSetEncode(imovie, omovie, formation['vcodec'], swig_worker.VIDEO):
            return False
        elif swig_worker.rbitSetQuality(imovie, omovie, int(res[0]), int(res[1])):
            return False
        elif swig_worker.rbitDoApplying(imovie, omovie):
            return False
        return True

    @staticmethod
    @pool
    def __convert__(logger, extend, imovie, widths, omovies, response, callback):
        if 'debug' in extend and extend['debug']: 
            debugable = True
            start = time()
        else: debugable = False

        if swig_worker.rbitDoTranscoding(imovie, 0):
            return False
        swig_worker.rbitReleaseConsumer(imovie)
        return True

        for omovie in omovies:
            if stat(omovie).st_size == 0:
                Converter.__static_logcat__(logger, logging.ERROR, '[x] [%s -> %s got unknown errors and corrupted]' % (split(imovie)[-1], omovie), debugable)
            return False
        else:
            if 'debug' in extend and extend['debug']:
                Converter.__static_logcat__(logger, logging.INFO, '[o] [%s -> %sp spent %fs' % (split(imovie)[-1], str(omovies), time() - start), debugable)

        callback(widths, response)
        return True

    @staticmethod
    def __static_logcat__(logger, level, message, debugable):
        nlevel = None
        if logger:
            if level == logging.ERROR:
                logger.error(message)
            if level == logging.DEBUG:
                logger.debug(message)
            if level == logging.INFO:
                logger.info(message)        
        if level == logging.ERROR:
            nlevel = 'ERROR'
        elif debugable:
            if level == logging.DEBUG:
                nlevel = 'DEBUG'
            elif level == logging.INFO:
                nlevel = 'INFO'
        if not nlevel is None:
            print(strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime()), nlevel, message)

    def __logcat__(self, level, message):
        nlevel = None
        if self.logger:
            if level == logging.ERROR:
                self.logger.error(message)
            if level == logging.DEBUG:
                self.logger.debug(message)
            if level == logging.INFO:
                self.logger.info(message)        
        if level == logging.ERROR:
            nlevel = 'ERROR'
        elif self.debug:
            if level == logging.DEBUG:
                nlevel = 'DEBUG'
            elif level == logging.INFO:
                nlevel = 'INFO'
        if not nlevel is None:
            print(strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime()), nlevel, message)
