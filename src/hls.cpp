#include "libhls.hpp"

#include <fstream>

namespace amqt {
namespace consumer {
namespace hls {
bool Chunk::__on_before_filting_packet(std::size_t idstream, AVFrame *frame) {
  if (static_cast<int>(idstream) == __get_main_stream()) {
    auto current = seconds(at<1>(idstream)->context()->time_base, frame->pts);
    return (_begin <= current) && !(_eof = (current >= _end));
  }
  return true;
}

bool Chunk::__on_after_writing_packet(std::size_t idstream, double current) {
  if (static_cast<int>(idstream) == __get_main_stream()) {
    if ((_eof = (current >= _end)) && _manual) {
      static const unsigned char padbuf[FF_INPUT_BUFFER_PADDING_SIZE] = {0};

      if (!_context->pb->max_packet_size) 
        avio_write(_context->pb, padbuf, sizeof(padbuf));
      avio_flush(_context->pb);
    }
    return (_begin <= current) && (_end == std::string::npos || !_eof);
  }
  return true;
}

/** Make a reference to chunk index
 *  @param index  the index of chunk
 *  @param memory use ramdisk if == true, otherwhile it will save in hard disk
 *  @return true if okey, false if it is fail
 */
bool M3U8::ref(std::size_t index, bool memory) {
  std::string name;

  if (index >= _chunks.size()) return false;
  if (_storage.size() > 0)
    name.append(_storage).append("/");

  name.append(_name).append("-")
      .append(std::to_string(index))
      .append(".ts");

  if (memory) {
    auto begin = _chunks[index].begin();
    auto end   = (index + 1 < _chunks.size()) ? _chunks[index + 1].begin() : _context.getDuration();
    auto chunk = std::make_shared<Chunk>(index, begin, end);

    _references[index] = chunk;
    _context.setOutput(std::forward<std::string>(name), std::forward<POutput>(chunk));
    _context.setEncodec(std::forward<std::string>(name), "copy", AVMEDIA_TYPE_VIDEO);
    _context.setEncodec(std::forward<std::string>(name), "copy", AVMEDIA_TYPE_AUDIO);
    _context.at(std::forward<std::string>(name))->apply();
    return true;
  } else return ref(std::forward<std::string>(name), index);
}


/** Make a reference to file which associated with chunk
 *  @param file  the absoluted file path 
 *  @param index the index of chunk
 *  @return true if okey, false if it is fail
 */
bool M3U8::ref(std::string&& file, std::size_t index) {
  if (index < _chunks.size()) {
    auto begin = _chunks[index].begin();
    auto end   = (index + 1 < _chunks.size()) ? _chunks[index + 1].begin() : _context.getDuration();
    auto chunk = std::make_shared<Chunk>(index, std::forward<std::string>(file), begin, end);

    _context.setOutput(std::forward<std::string>(file), std::forward<POutput>(chunk));
    _context.setEncodec(std::forward<std::string>(file), "copy", AVMEDIA_TYPE_VIDEO);
    _context.setEncodec(std::forward<std::string>(file), "copy", AVMEDIA_TYPE_AUDIO);
    _context.at(std::forward<std::string>(file))->apply();

    return true;
  } else return false;
}

/** Do transcode from consumer to a chunk and save to outputs which has been linked before
 *  @param index the index of chunk
 *  @return true if okey, false if it is fail
 */
bool M3U8::transcode(std::size_t index) {
  if (index < _chunks.size()) {
    auto save   = _context.tell();
    auto result = _context.seek(_chunks[index].begin(), AVSEEK_FLAG_ANY);

    if (result)
      result = !_context.toTranscode(false);
    if (result && (_auto || index + 1 != _chunks.size()))
      result = _context.seek(save, AVSEEK_FLAG_ANY);
    return result;
  }
  return false;
}

/** Release the output that has been linked before
 *  @param file the absoluted file path that has been use before
 */
void M3U8::unref(std::string&& file) { _context.remove(std::forward<std::string>(file)); }


/** Release the output that has been linked before using index 
 *  @param index the chunk id
 */
void M3U8::unref(std::size_t index) {
  std::string name;

  if (_storage.size() > 0)
    name.append(_storage).append("/");

  name.append(_name).append("-")
      .append(std::to_string(index))
      .append(".ts");
  
  _references[index] = nullptr;
  return unref(std::forward<std::string>(name));
}

/** Print a scap of m3u8 playlist which associated with the index
 *  @param result the buffer that has been use before
 *  @param index  the id of chunk if index >= 0 of the header if index == std::string::npos
 *  @return result after printing 
 */
std::string& M3U8::sprint(std::string& result, std::size_t index) {
  result.clear();
  
  if (index == std::string::npos) {
    result = "#EXTM3U\n";

    for (auto& property : _properties) {
      auto& key   = std::get<0>(property);
      auto& value = std::get<1>(property);

      result.append("#EXT-X-")
            .append(key).append(":")
            .append(value)
            .append("\n");
    }
  } else if (index < _chunks.size()){
    result.append("#EXTINF:")
          .append(std::to_string(_chunks[index].length()))
          .append("\n");
    if (_alias.size() > 0) result.append(_alias).append("/");
    result.append(_name).append("-")
          .append(std::to_string(index))
          .append(".ts\n");
  }
  if (index + 1 == _chunks.size() && _properties.at("PLAYLIST-TYPE") == "VOD")
    result.append("#EXT-X-ENDLIST\n");
  return result;
}

/** Print a file m3u8
 */
M3U8& M3U8::fprint() {
  std::ofstream playlist{_storage + "/" + _name + ".m3u8", std::ofstream::binary};
  std::string buffer;

  for (auto i = -1; i < static_cast<int>(_chunks.size()); ++i)
    playlist << sprint(buffer, i);
  if (_properties["PLAYLIST-TYPE"] == "VOD")
    playlist << "#EXT-X-ENDLIST\n";
  return *this;
}

std::vector<converter::Seekbar> M3U8::__build_chunks_(converter::Context& input, double length) {
  auto chunk = 0.0;

  return input.seekbar().split([&chunk, length](double pos, int flags)->bool{
    auto fix = (pos - fmod(pos, length));

    if (chunk != fix && flags == AV_PKT_FLAG_KEY) {
      chunk = fix;
      return true;
    }
    return false;
  });
}
}
}
}