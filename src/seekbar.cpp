#include "libseek.hpp"
#include "libmedia.hpp"
#include "utilities.hpp"
#include "logcat.hpp"

namespace amqt {
namespace consumer {
namespace converter {

using namespace std;

int Seekbar::__compare(decltype(_entries)::iterator entry, double sample) {
   auto ts_pos = timebase(_ration, sample);

  if (entry->timestamp == ts_pos) return 0;
  if (entry->timestamp < ts_pos && ts_pos < (entry + 1)->timestamp)
    return 0;
  else if (entry == _entries.begin()) return -1;
  else if (ts_pos <= (entry - 1)->timestamp)
    return -1;

  if (ts_pos <= (entry + 1 == _entries.end())) return 1;
  else if (entry->timestamp > ts_pos && ts_pos > (entry + 1)->timestamp)
    return 0;    
  else return 1;
}

Seekbar::Seekbar(const AVRational ration): _current{0}, _ration(ration), _signal{nullptr}{}

Seekbar::Seekbar(AVFormatContext* context, Seekbar::Signal signal)
    : _current{0}, _signal{signal}{

  if (context){
    auto idstream = av_find_default_stream_index(context);
  
    if (idstream >= 0){
      auto stream = context->streams[idstream];

      _ration = stream->time_base;    
      if (stream->nb_index_entries > 1){
        _entries.resize(stream->nb_index_entries);

        for (auto i = stream->nb_index_entries; i > 0; --i)
          _entries[i - 1] = stream->index_entries[i - 1];
      } 
    } else throw NoSupport;
  }
}

double Seekbar::find(double begin, std::function<bool(double, int)> condition) {
  auto current = _current;
  auto result  = -1.0;

  try {
    seek(begin, AVSEEK_FLAG_ANY);

    for (;_current < _entries.size(); ++_current){
      auto& entry = _entries[_current];

      if (condition(seconds(_ration, entry.timestamp), entry.flags)){
        result = seconds(_ration, entry.timestamp);
        break;
      }
    }
    _current = current;
  } catch (base::Error& error){ throw base::Error{error}; }

  if (result < 0) throw NotFound;
  else return result;
}

AVIndexEntry Seekbar::seek(double position, int) {
  size_t begin = 0, end = _entries.size() - 1;

  if (_entries.size() == 0) throw BadAccess;
  if (__compare(_entries.end() - 1, position) >= 0)
    return _entries[_current = _entries.size() - 1];
  if (*this == position) return _entries[_current];;
  if (*this < position)
    end = _current;
  else
    begin = _current;

  while (begin < end - 1) {
    auto ret = __compare(_entries.begin() + (begin + end) / 2, position);

    if (!ret) {
      _current = (begin + end) / 2;
      break;
    } else if (ret < 0)
      end = (begin + end) / 2;
    else
      begin = (begin + end) / 2;
  }

  return _entries[_current = (begin + end) / 2];
}

bool Seekbar::operator==(double position) {
  auto ts_pos = timebase(_ration, position);
  auto scale  = _ration.den/10000;

  if (_entries.size() == 0) return false;
  if (_entries[_current].timestamp == ts_pos) return true;
  if (_current < _entries.size()){
    if (_entries[_current].timestamp < ts_pos + scale && ts_pos + scale < _entries[_current + 1].timestamp)
      return true;
  } else return _entries[_current].timestamp < ts_pos;
  if (_current > 0){
    if (_entries[_current].timestamp > ts_pos - scale && ts_pos - scale > _entries[_current - 1].timestamp)
      return true;
  } else return _entries[_current].timestamp > ts_pos;
  return false;
}

bool Seekbar::operator<(double position) {
  auto ts_pos = timebase(_ration, position);
  auto scale  = _ration.den/10000;

  if (_entries.size() == 0) return false;
  if (*this == position) return false;
  if (_current > 0)
    return _entries[_current - 1].timestamp >= ts_pos;
  else return _entries[_current].timestamp > ts_pos + scale;
}

bool Seekbar::operator>(double position) {
  auto ts_pos = timebase(_ration, position);
  auto scale  = _ration.den/10000;

  if (_entries.size() == 0) return false;
  if (*this == position) return false;
  if (_current < _entries.size())
    return _entries[_current + 1].timestamp < ts_pos - scale;
  else return true;
}
}  // namespace converter
}  // namespace consumer
}  // namespace amqt