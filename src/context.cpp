/* copyright 2017 by lazycat and it is owned by lazycat */

#define __STDC_CONSTANT_MACROS
#include "libmedia.hpp"
#include "utilities.hpp"

#include <sstream>
#include <string>
#include <utility>

auto timebase = AV_TIME_BASE_Q;

namespace amqt {
namespace consumer {
namespace converter {
#define __close_everything(reason)                 \
  {                                                \
    for (auto &decoder : _decoders)                \
      avcodec_free_context(&std::get<0>(decoder)); \
    avformat_close_input(&_context);               \
    throw reason;                                  \
  }

const char *avtype2string(enum AVMediaType media_type) {
  switch (media_type) {
  case AVMEDIA_TYPE_VIDEO:
    return "video";

  case AVMEDIA_TYPE_AUDIO:
    return "audio";

  case AVMEDIA_TYPE_DATA:
    return "data";

  case AVMEDIA_TYPE_SUBTITLE:
    return "subtitle";

  case AVMEDIA_TYPE_ATTACHMENT:
    return "attachment";

  default:
    return "unknown";
  }
}
/* -------------------------------------------------------------------------- */

Context::Context(std::string &&url, AVInputFormat *format, AVDictionary **options)
  : _context{nullptr}, _options{options},
    _vidstream{std::string::npos},
    _aidstream{std::string::npos},
    _seekbar{nullptr, nullptr}{
  AVError error  = avformat_open_input(&_context, url.c_str(), format, options);
  AVPacket avpkt;

  memset(&avpkt, 0, sizeof(AVPacket));
#if TRACE
#if DEBUG
  av_log_set_level(AV_LOG_TRACE);
#else
  av_log_set_level(AV_LOG_DEBUG);
#endif
#elif DEBUG
  av_log_set_level(AV_LOG_TRACE);
#else
  av_log_set_level(AV_LOG_PANIC);
#endif
  
  packet = std::make_tuple(avpkt, nullptr, Finished);

  if (error < 0) throw NoSupport;
  if (avformat_find_stream_info(_context, nullptr) < 0)
    __close_everything(BadAccess);

  if (_context->nb_streams == 0)
    __close_everything(NoError.reason("There are nothing to do"));

#if !USE_SINGLE_THREAD
  if (!options) {
    _options = pmalloc(_options, 1);
    av_dict_set(_options, "threads", "0", 0);
  }
#endif

  for (decltype(_context->nb_streams) i{0}; i < _context->nb_streams; ++i) {
    auto codec_id = _context->streams[i]->codecpar->codec_id;

    auto codec = avcodec_find_decoder(codec_id);
    auto codec_ctx = avcodec_alloc_context3(codec);

    if (avcodec_parameters_to_context(codec_ctx, _context->streams[i]->codecpar) < 0)
      __close_everything(BadAccess);

    if (codec_ctx->codec_type == AVMEDIA_TYPE_VIDEO || codec_ctx->codec_type == AVMEDIA_TYPE_AUDIO) {
      if (codec_ctx->codec_type == AVMEDIA_TYPE_VIDEO && _vidstream == std::string::npos) {
        codec_ctx->framerate = av_guess_frame_rate(_context, _context->streams[i], nullptr);

        _vidstream = i;
      } else if (_aidstream == std::string::npos)
        _aidstream = i;

      /* Open decoder */
      if (avcodec_open2(codec_ctx, codec, options) < 0) __close_everything(BadAccess);
    }
    _decoders.push_back(std::make_tuple(codec_ctx, 0, -1));
  }
  _seekbar = Seekbar{_context, nullptr};
  _cr_conv_pos = tell();
}

Context::~Context() {
  for (auto &decoder : _decoders) {
    avcodec_close(std::get<0>(decoder));
    avcodec_free_context(&std::get<0>(decoder));
  }
  avio_closep(&_context->pb);
  avformat_close_input(&_context);
  
  if (_options){
    av_dict_free(_options);
    av_free(_options);
  }
}

int8_t& Context::toDecoder(std::size_t idstream) {
  if (idstream < _decoders.size())
    return std::get<2>(_decoders[idstream]);
  throw OutOfRange;
}

std::string Context::toString(base::TypePrinter &&type) {
  base::Printer printer{std::forward<base::TypePrinter>(type)};

  base::Printer format{std::forward<base::TypePrinter>(type)};
  base::Printer streams{std::forward<base::TypePrinter>(type)};

  /* build the detailed streams' info */
  for (auto i = _context->nb_streams; i > 0; --i) {
    base::Printer info_stream{std::forward<base::TypePrinter>(type)};

    auto stream = _context->streams[i - 1];
    auto par = stream->codecpar;
    auto codec_desc = avcodec_descriptor_get(par->codec_id);

    /* load stream info into a buffer */
    if (par->codec_type == AVMEDIA_TYPE_VIDEO)
      info_stream << fwmap(int, "width", par->width)
                  << fwmap(int, "height", par->height);

    info_stream << fwmap(int64_t, "duration_ts", stream->duration)
                << fwmap(std::string, "codec_type", avtype2string(par->codec_type))
                << fwmap(std::string, "codec_name", codec_desc ? codec_desc->name : "unknown")
                << fwmap(std::string, "time_base", std::to_string(stream->time_base.num) + "/" +
                         std::to_string(stream->time_base.den));

    /* generate a new block which aims to save the streams' info */
    streams << fwblock(base::Printer, info_stream);
  }
  /* build the context's info */

  format << fwmap(int, "duration", seconds(timebase, _context->duration));

  /* load everything to root printer and export */
  printer << fwmap(base::Printer, "streams", streams)
          << fwmap(base::Printer, "format", format);

  return printer.toString();
}

std::string Context::getAttribute(std::string &&index, base::TypePrinter type) {
  std::ostringstream result{};

  if (!index.compare(0, sizeof("format"), "format")) {
    if (index.size() == sizeof("format"))
      __get_format(result, type);
    else if (!index.compare(sizeof("format."), sizeof("duration"), "duration"))
      return std::to_string(_context->duration);
  }

  if (!index.compare(0, sizeof("streams"), "streams")) {
    if (index.size() == sizeof("streams"))
      __get_streams(result, type);
    else if (index[sizeof("streams")] == '[') {
      auto last = index.find(']');
      auto block = index.substr(sizeof("streams"), last - sizeof("streams"));
      auto idstream = std::stoi(block);

      if ((idstream) >= static_cast<decltype(idstream)>(_context->nb_streams))
        return std::forward<std::string>("");
      else if (last + 1 == index.size())
        __get_stream_info(idstream, result, type);
      else if (last + 2 < index.size())
        return __get_stream_elm(std::forward<std::string>(index), idstream, last);
    }
  }
  return result.str();
}

std::string Context::__get_stream_elm(std::string &&index, std::size_t id, std::size_t point) {
  auto elm = index.substr(point + 2);
  auto stream = _context->streams[id];
  auto par = stream->codecpar;

  if (elm == "width")
    return std::forward<std::string>(std::to_string(par->width));
  else if (elm == "height")
    return std::forward<std::string>(std::to_string(par->height));
  else if (elm == "duration_ts")
    return std::forward<std::string>(std::to_string(stream->duration));
  else if (elm == "codec_type")
    return std::forward<std::string>(std::string{avtype2string(par->codec_type)});
  else if (elm == "time_base")
    return std::forward<std::string>(std::to_string(stream->time_base.num) + "/" +
                                     std::to_string(stream->time_base.den));
  else return std::forward<std::string>("");
}

void Context::__get_streams(std::ostream &result, base::TypePrinter type) {
  base::Printer streams{result, std::forward<base::TypePrinter>(type)};

  /* build the detailed streams' info */
  for (auto i = _context->nb_streams; i > 0; --i) {
    base::Printer info_stream{std::forward<base::TypePrinter>(type)};

    auto stream = _context->streams[i - 1];
    auto par = stream->codecpar;
    auto codec_desc = avcodec_descriptor_get(par->codec_id);

    /* load stream info into a buffer */
    if (par->codec_type == AVMEDIA_TYPE_VIDEO)
      info_stream << fwmap(int, "width", 0) << fwmap(int, "height", 0);

    info_stream << fwmap(int64_t, "duration_ts", stream->duration)
                << fwmap(std::string, "codec_type", avtype2string(par->codec_type))
                << fwmap(std::string, "codec_name", codec_desc ? codec_desc->name : "unknown")
                << fwmap(std::string, "time_base", std::to_string(stream->time_base.num) + "/" +
                         std::to_string(stream->time_base.den));

    /* generate a new block which aims to save the streams' info */
    streams << fwblock(base::Printer, info_stream);
  }
}

void Context::__get_stream_info(int id, std::ostream &result, base::TypePrinter type) {
  base::Printer info_stream{result, std::forward<base::TypePrinter>(type)};

  auto stream = _context->streams[id - 1];
  auto par = stream->codecpar;
  auto codec_desc = avcodec_descriptor_get(par->codec_id);

  /* load stream info into a buffer */
  if (par->codec_type == AVMEDIA_TYPE_VIDEO)
    info_stream << fwmap(int, "width", par->width)
                << fwmap(int, "height", par->height);

  info_stream << fwmap(int64_t, "duration_ts", stream->duration)
              << fwmap(std::string, "codec_type",
                       avtype2string(par->codec_type))
              << fwmap(std::string, "codec_name",
                       codec_desc ? codec_desc->name : "unknown")
              << fwmap(std::string, "time_base", std::to_string(stream->time_base.num) + "/" +
                       std::to_string(stream->time_base.den));
}

void Context::__get_format(std::ostream &result, base::TypePrinter type) {
  base::Printer printer{result, std::forward<base::TypePrinter>(type)};
  printer << fwmap(double, "duration", _context->duration);
}

std::string Context::getDecodec(std::size_t idstream) {
  auto stream = _context->streams[idstream];
  auto par = stream->codecpar;
  auto codec_desc = avcodec_descriptor_get(par->codec_id);

  return codec_desc ? codec_desc->name : "unknown";
}

std::size_t Context::getWidth(std::size_t idstream) {
  auto stream = _context->streams[idstream];
  auto par = stream->codecpar;

  if (par->codec_type == AVMEDIA_TYPE_VIDEO)
    return par->width;
  else return 0;
}

std::size_t Context::getHeight(std::size_t idstream) {
  auto stream = _context->streams[idstream];
  auto par = stream->codecpar;

  if (par->codec_type == AVMEDIA_TYPE_VIDEO)
    return par->height;
  else return 0;
}

double Context::getDuration(std::size_t idx) {
  if (idx == std::string::npos)
    return _context->duration;
  else if (idx < _context->nb_streams)
    return seconds(_context->streams[idx]->time_base, _context->streams[idx]->duration);
  return -1;
}

std::shared_ptr<Output> &Context::at(std::string &&output) {
  if (_outputs.find(output) == _outputs.end())
    throw NotFound;
  return _outputs.at(output);
}

Context &Context::setAttribute(std::string &&, std::string &&) { return *this; }

Context &Context::setEncodec(std::string &&output, std::string &&encode, std::size_t type) {
  AVCodec *encodec = nullptr;

  if (_outputs.find(output) == _outputs.end())
    throw NotFound;
  else if (encode != "copy" && !(encodec = avcodec_find_encoder_by_name(encode.c_str())))
    throw NoSupport;
  /* autobuild a new stream if the codec is found a fact that it hasn't been
   * inited. Otherwide, it just gets the index of the approviated stream */
  try {
    auto& output_ptr = _outputs.at(output);
    int iidstm{ -1};

    if (type == std::string::npos && !encodec) throw BadLogic;
    if (type == std::string::npos || (encodec && encodec->type != static_cast<int>(type)))
      type = encodec->type;

    if ((iidstm = av_find_best_stream(_context, static_cast<AVMediaType>(type), -1, -1, nullptr, 0)) >= 0) {
      /* now, we have both ostream and istream, the next step is to fill the
       * codec context with the default values that has stored inside the codec
       * parameters of istream */

      /* by default, we usually copy everything (or at least, approviated
       * information of input stream) to output stream */
      auto codec     = encodec? static_cast<std::size_t>(encodec->id): std::string::npos;
      auto oidstream = output_ptr->getStream(_context->streams[iidstm], encodec);

      if ((codec != std::string::npos))
        output_ptr->copyEverything(_context->streams[iidstm], oidstream);
        
      if (std::get<2>(_decoders[iidstm]) <= 0)
        std::get<2>(_decoders[iidstm]) = codec != std::string::npos;
      return *this;
    }
  } catch (base::Error &error) { throw base::Error{error}; }
  throw NotFound;
}

Context &Context::setQuality(std::string &&foutput, std::size_t width, std::size_t height) {
  if (_outputs.find(foutput) != _outputs.end())
    _outputs[foutput]->setQuality(width, height);
  else throw NotFound;
  return *this;
}

std::size_t Context::setOStream(std::string &&output, std::size_t istream, std::size_t codec) {
  if (istream >= _context->nb_streams) throw OutOfRange;
  try {
    if (_outputs.find(output) != _outputs.end())
      return _outputs[output]->getStreamNext(_context->streams[istream], codec, 0);
    return std::string::npos;
  } catch (base::Error &error) { throw std::forward<base::Error>(error); }
}

Context &Context::setOutput(std::string &&output) {
  if (_outputs.find(output) == _outputs.end())
    _outputs[output] = std::make_shared<Output>(std::forward<std::string>(output));
  return *this;
}

Context &Context::setOutput(std::string &&output, POutput&& poutput){
  if (_outputs.find(output) == _outputs.end())
    _outputs[output] = poutput;
  return *this;
}

bool Context::seek(double pos, int flags) {
  auto  ts_pos = timebase(AV_TIME_BASE_Q, (pos >= 0.0? pos: _seekbar.begin()));

  if (pos == _cr_conv_pos && _seekbar == pos) return true;
  if (flags != AVSEEK_FLAG_ANY && pos < _cr_conv_pos) flags |= AVSEEK_FLAG_BACKWARD;

  if (avformat_seek_file(_context, -1, INT64_MIN, ts_pos, ts_pos, flags) < 0) return false;
  else if (_seekbar.begin() == pos){
    for (auto& decoder: _decoders) avcodec_flush_buffers(std::get<0>(decoder));  
  }

  _eof = !(pos < tell()) && _eof;
  _seekbar.seek(pos, flags);
  return true;
}

double Context::tell() {
  return seconds(_context->streams[__get_main_stream()]->time_base, _seekbar.tell().timestamp);
}

void Context::remove(std::string &&output) {
  if (output.size()) _outputs.erase(output);
  else _outputs.clear();
}
}  // namespace converter
}  // namespace consumer
}  // namespace amqt
