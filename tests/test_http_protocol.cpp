#include "libbase/pipe.hpp"
#include "libbase/grammar/all.hpp"

#include <iostream>
#include <cstring>
#include <map>

enum Method {Unknown = 0, Get, Post, Respone};

namespace grammar = base::grammar;

void abc(){
  Lexical<> src;
  Lexical<> target;

  auto a = src >> target;
}

int main() {
  auto str =
    "GET /abc HTTP/1.1\r\n"
    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n"
    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n\r\ndancac";
  auto method  = Unknown;
  auto version = -1;

  std::map<std::string, std::string> parameters;
  std::string key, value;  

  base::Deserialize<>::Step step_method = [&method](std::size_t i, char c) -> grammar::Status{
    static const auto post = "POST";
    static const auto get  = "GET";

    if (method == Unknown) {
      method = (c == 'G') ? Get : ((c == 'P') ? Post : Unknown);
      if (method == Unknown) return grammar::Fal;
    } else if (method == Post) {
      if (post[i] != c) return grammar::Fal;
      else if (i + 1 == 4)
        return grammar::Don;
    } else if (method == Get) {
      if (get[i] != c) return grammar::Fal;
      else if (i + 1 == 3)
        return grammar::Don;
    } else return grammar::Fal;
    return grammar::Unf;
  };

  base::Deserialize<>::Step step_blank = [](std::size_t, char c) -> grammar::Status { 
    return c == ' '? grammar::Unf: grammar::Inv;
  };

  base::Deserialize<>::Step step_url = [&version](std::size_t i, char c) -> grammar::Status { 
    if (i == 0) 
      return c == '/'? grammar::Unf: grammar::Fal;
    else return grammar::Pal;
  };

  base::Deserialize<>::Step step_version = [&version](std::size_t i, char c) -> grammar::Status { 
    const auto prefix = "HTTP/1.";

    if (i < 7) 
      return prefix[i] != c? grammar::Fal: grammar::Unf;
    version = c - '0';
    return grammar::Don;
  };

  base::Deserialize<>::Step step_newline = [&parameters, &key, &value](std::size_t i, char c) -> grammar::Status{
    const auto prefix = "\r\n";

    if (i == 1 && key.length() && value.length()){
      parameters[key] = value;
      key.erase();
      value.erase();
    }
    
    if (i < 2)
      return (prefix[i] != c)? grammar::Fal: ((i == 1) ? grammar::Don: grammar::Unf);
    return grammar::Fal;
  };

  base::Deserialize<>::Step step_key = [&key](std::size_t i, char c) -> grammar::Status{
    if (c != ':') key += c;
    return (c != ':')? grammar::Unf: ((i > 0)? grammar::Don: grammar::Fal);
  };

  base::Deserialize<>::Step step_val = [&value](std::size_t, char c) -> grammar::Status{
    value += c;
    return grammar::Pal;
  };

  std::vector<base::Deserialize<>::Step> steps0 = {step_method, step_blank, step_url, step_blank, step_version, step_newline};  
  std::vector<base::Deserialize<>::Step> steps1 = {step_key, step_blank, step_val, step_newline, step_newline};
  base::Deserialize<> http_header{steps0};
  base::Deserialize<> http_parameter{steps1};

  for (std::size_t i = 0; i < strlen(str); i++){
    if (http_header < 0) http_header << str[i];
    else if (http_header == grammar::Don && http_parameter < 0)
      http_parameter << str[i];

    if (http_parameter == grammar::Don)
      break;
    else if (http_parameter == grammar::Fal) 
      http_parameter << str[i];
  }
  std::cout << "Bye... :-) \n\n";
}
