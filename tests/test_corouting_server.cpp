/*
#include <iostream>
#include <csetjmp>
 
 
[[noreturn]] void a(std::jmp_buf& jump_buffer, int count) 
{
    std::cout << "a(" << count << ") called\n";
    std::longjmp(jump_buffer, count+1);  // setjump() will return count+1
}
 
int main(){
  std::jmp_buf jump_buffer;
  int count = 0; // modified locals in setjmp scope must be volatile
  if (setjmp(jump_buffer) != 9) { // equality against constant expression in an if
      a(jump_buffer, ++count);  // This will cause setjmp() to exit
  }
}*/

#include <libbase/logcat.hpp>

#include <libworker.h>

#include <libmedia.hpp>
#include <libhls.hpp>
#include <libio.hpp>

namespace converter = amqt::consumer::converter;
namespace hls = amqt::consumer::hls;


int onSend(IOClass* clazz, int fd){
    do {      
      /* xu ly va ghi du lieu tu buffer vao fd */
    } while(clazz->async(clazz, onSend));
    return base::error::ENoError;
}

int onReceive(IOClass* clazz, int fd){
    int error = base::error::ENoError;
    do {
        /* doc va xu ly du lieu vao buffer */
         
        if ((error = onSend(clazz, fd))) 
          return error;
    } while(clazz->async(clazz, onReceive));
    return base::error::ENoError;
}

int onAccepting(IOClass* clazz, int){
    do
      clazz->start(clazz, onReceive);
    while (clazz->async(clazz, onAccepting));
    return base::error::ENoError;
}

int main(){
  namespace server = amqt::server;
  namespace endpoint = server::endpoint;

  server::IOService service{10000};
  base::Debug debug{true};

  auto& ioclass = service.at(std::make_shared<endpoint::Acceptor>(8080, 10000));
  ioclass.async(&ioclass, onAccepting);
}