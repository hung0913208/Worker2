#include "libworker.h"

int main(int argc, char const *argv[]){
    if (rbitInitConsumer(argv[1])) return -1;
    else if (rbitSetOutput(argv[1], argv[3])) return -1;
    else if (rbitSetEncode(argv[1], argv[3], argv[2], VIDEO)) return -1;
    else if (rbitDoApplying(argv[1], argv[3])) return -1;
    else if (rbitDoTranscoding(argv[1], 0)) return -1;
    else {
        rbitReleaseAllConsumer();
        return 0;
    }
}