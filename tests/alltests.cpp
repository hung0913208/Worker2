/* copyright 2017 by lazycat and it is owned by lazycat */

#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <libbase/logcat.hpp>

#include <libworker.h>

#include <libmedia.hpp>
#include <libhls.hpp>
#include <libio.hpp>

namespace converter = amqt::consumer::converter;
namespace hls = amqt::consumer::hls;

auto iurl = "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4";
auto ourl0 = "/tmp/otest0.mp4";
auto ourl1 = "/tmp/otest1.mp4";
auto ourl2 = "/tmp/otest2.mp4";
auto ourl3 = "/tmp/otest3.mp4";

BOOST_AUTO_TEST_CASE(line_up){
  base::Line<int> l{-1, -1, [](int&& val) -> int { return val; }}; 
  int val = 0;

  for (int i = 0; i < 10; i++)
    l.exchange(rand()%10, [&l](int&& val) -> bool { return val - 1 == l.current();} );
  
  while ((val = l.flush()) != l.null()){ std::cout << val; }
}

BOOST_AUTO_TEST_CASE(grammar){
  //Deserialize decoder();
}

BOOST_AUTO_TEST_CASE(hls_context_test) {
  BOOST_REQUIRE_MESSAGE(rbitInitHLSContext(iurl, "test", nullptr, 5.0) == 0, "Can't finish initing a hls context");
  rbitPrintChunk(iurl, 0);
  rbitPrintChunk(iurl, 0);
  rbitReleaseHLSContext(iurl);
}

BOOST_AUTO_TEST_CASE(tracking_test) {
  BOOST_REQUIRE_MESSAGE(rbitInitConsumer(iurl) == 0, "Can't finish opening a media file");

  base::Debug debug{true};

  auto tracking = rbitDoTracking(iurl, 0);
  BOOST_REQUIRE_MESSAGE(strlen(tracking) > 0, "Can't produce a tracking message");
  BOOST_TEST_MESSAGE("Tracking: " << std::string{tracking});
  free(tracking);
}

BOOST_AUTO_TEST_CASE(thumbnail_test) {
  BOOST_REQUIRE_MESSAGE(rbitInitConsumer(iurl) == 0, "Can't finish opening a media file");
  BOOST_REQUIRE_MESSAGE(rbitPrintThumbnail(iurl, "/tmp/test.jpeg", 1200) == 0, "Can't finish opening a media file");
}

BOOST_AUTO_TEST_CASE(generate_chunks){
  converter::Context context{iurl};

  context.setOutput(ourl2, std::forward<std::shared_ptr<converter::Output>>(std::make_shared<hls::Chunk>(std::string::npos, ourl2, 0, 10.0)));
  context.setEncodec(ourl2, "h264_qsv", AVMEDIA_TYPE_VIDEO);
  context.setEncodec(ourl2, "copy", AVMEDIA_TYPE_AUDIO);
  context.setQuality(ourl2, 320, 240);
  context.at(ourl2)->apply();
  context.setOutput(ourl3, std::forward<std::shared_ptr<converter::Output>>(std::make_shared<hls::Chunk>(std::string::npos, ourl3,  0, 10.0)));
  context.setEncodec(ourl3, "copy", AVMEDIA_TYPE_VIDEO);
  context.setEncodec(ourl3, "copy", AVMEDIA_TYPE_AUDIO);
  context.setQuality(ourl3, 320, 240);
  context.at(ourl3)->apply();
  context.toTranscode();
}

BOOST_AUTO_TEST_CASE(converting_test) {
  BOOST_REQUIRE_MESSAGE(rbitInitConsumer(iurl) == 0, "Can't finish opening a media file");
  BOOST_REQUIRE_MESSAGE(rbitSetOutput(iurl, ourl0) == 0, "Can't init a new output");  
  BOOST_REQUIRE_MESSAGE(rbitSetEncode(iurl, ourl0, "libx264", VIDEO) == 0, "Can't init a new video stream");
  BOOST_REQUIRE_MESSAGE(rbitSetProperty(iurl, ourl0, "libx264", "thread", "auto") == 0, "Can't set property preset to codec h264_qsv");
  BOOST_REQUIRE_MESSAGE(rbitSetProperty(iurl, ourl0, "libx264", "preset", "faster") == 0, "Can't set property preset to codec h264_qsv");
  BOOST_REQUIRE_MESSAGE(rbitSetProperty(iurl, ourl0, "libx264", "look_ahead", "0") == 0, "Can't set property preset to codec h264_qsv");
  BOOST_REQUIRE_MESSAGE(rbitSetProperty(iurl, ourl0, "libx264", "vprofile", "baseline") == 0, "Can't set property vprofile to codec h264_qsv");
  BOOST_REQUIRE_MESSAGE(rbitSetQuality(iurl, ourl0, 480, 360) == 0, "Can't set quality for video streams");
  BOOST_REQUIRE_MESSAGE(rbitSetEncode(iurl, ourl0, "copy", AUDIO) == 0, "Can't init a new audio stream");
  BOOST_REQUIRE_MESSAGE(rbitSetBitFilter(iurl, ourl0, "copy", VIDEO, "h264_mp4toannexb") == 0, "Can't init a new filter");
  //BOOST_REQUIRE_MESSAGE(rbitSetBitrate(iurl, ourl0, VIDEO, 100000) == 0, "Can't change stream's bitrate ");
  //BOOST_REQUIRE_MESSAGE(rbitSetFramerate(iurl, ourl0, VIDEO, 20) == 0, "Can't change stream's framerate");
  BOOST_REQUIRE_MESSAGE(rbitDoApplying(iurl, ourl0) == 0, "Can't apply codec to streams");
  BOOST_REQUIRE_MESSAGE(rbitSetOutput(iurl, ourl1) == 0, "Can't init a new output");
  BOOST_REQUIRE_MESSAGE(rbitSetEncode(iurl, ourl1, "libx264", VIDEO) == 0, "Can't init a new video stream");
  BOOST_REQUIRE_MESSAGE(rbitSetQuality(iurl, ourl1, 320, 240) == 0, "Can't set quality for video streams");
  BOOST_REQUIRE_MESSAGE(rbitSetEncode(iurl, ourl1, "copy", AUDIO) == 0, "Can't init a new audio stream");
  BOOST_REQUIRE_MESSAGE(rbitDoApplying(iurl, ourl1) == 0, "Can't apply codec to streams");
  BOOST_REQUIRE_MESSAGE(rbitDoTranscoding(iurl, false) == 0, "Can't finish transcoding");
}

BOOST_AUTO_TEST_CASE(generate_m3u8_chunk){
  converter::Context context{iurl};
  hls::M3U8 m3u8{context, 60};

  BOOST_REQUIRE_MESSAGE(m3u8.ref("/tmp/abc.ts", 0) == true, "Can't reference a chunk");
  BOOST_REQUIRE_MESSAGE(m3u8.transcode(0) == true, "Can't transcode the first chunk");
  m3u8.unref("/tmp/abc.ts");
}
